package persistence;

import model.Activity;
import java.io.*;
import java.net.*;
import java.util.Collection;

/**
 * Provides methods for loading and saving data
 * to the web (as in an online database).
 */
public class NetActivitySerializer implements ActivitySerializer {
	
	/**
	 * Hostname/IP of the server.
	 */
	private String host;
	/**
	 * Port of the server.
	 */
	private int port;
	/**
	 * Socket instance to connect with.
	 */
	private Socket socket;
	
	/**
	 * Creates a new NetActivitySerializer.
	 * @param host Hostname/IP to use as a server.
	 * @param port Port of the server.
	 */
	public NetActivitySerializer(String host, int port)
	{
		this.host = host;
		this.port = port;
	}
	
	/**
	 * Loads a collection of activities from the server.
	 */
    @SuppressWarnings("unchecked")
	@Override
    public Collection<Activity> loadData() {
    	try {
			boolean success = OpenConnection();
			if (!success) {
				throw new Exception("A connection to the server could not be established.");
			}
			
    		// Create an object reading stream
    		ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
    		// Input object
    		Object activityIn = in.readObject();
    		// Check type
    		if (!(activityIn instanceof Collection<?>)) {
				throw new Exception("Invalid data received. Check program is not outdated.");
			}
    		// Close input stream
    		in.close();
			
			success = CloseConnection();
			if (!success) {
				throw new Exception("Disconnecting from the server failed.");
			}
			
			return (Collection<Activity>)activityIn;
		} catch (Exception e) {
			System.err.println("Loading data failed with error: " + e.getMessage());
			return null;
		}
    }
    
    /**
     * Opens the network socket connection.
     * @return Success. Errors are printed to stderr.
     */
    private boolean OpenConnection()
    {
		try {
			socket = new Socket(host, port);	// opens a connection
			return true;
		} catch (Exception e) {
			System.err.println("An error occured while opening a connection:\n"
					+ "Host: " + host + "\nPort: " + port + "\nError: " + e.getMessage());
			return false;	// return failure
		}
	}
    
    /**
     * Closes the network socket.
     * @return Success. Errors are printed to stderr.
     */
    private boolean CloseConnection()
    {
		try {
			socket.close();	// close connection
			return true;
		} catch (Exception e) {
			System.err.println("An error occured while closing a connection:\n"
					+ "Host: " + host + "\nPort: " + port + "\nError: " + e.getMessage());
			return false;	// return failure
		}
	}
}
