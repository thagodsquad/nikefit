package persistence;

import model.Activity;

import java.util.Collection;

/**
 * A common interface for classes which load activities.
 */
public interface ActivitySerializer {
    /**
     * Loads activity data from a profile
     * @return The activities
     */
    Collection<Activity> loadData() throws Exception;
}
