package tests.analysis;

import analysis.Statistics;
import junit.framework.TestCase;
import model.Activity;
import model.DataPoint;
import model.Location;
import org.junit.Test;
import tests.model.ActivityTest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * The class <code>StatisticsTest</code> contains tests for the class <code>{@link Statistics}</code>.
 * @version $Revision: 1.0 $
 */
public class StatisticsTest extends TestCase {
    @Test
    public void testRound(){
        float result = Statistics.round(5.6f, 0);
        assertEquals(6, result, 0.1f);
    }

    @Test
    public void testRest(){
        Statistics stats = new Statistics();
        stats.analyse(ActivityTest.createTestActivitiesWithTachycardia());

        stats.Reset();

        assertEquals(0.0, stats.getAverageSpeed(), 0.1f);
        assertEquals(0.0, stats.getGreatestSpeed(), 0.1f);

        assertEquals(0.0, stats.getAverageHeartRate(), 0.1f);
        assertEquals(0.0, stats.getHighestHeartRate(), 0.1f);

        assertEquals(0.0, stats.getHighestElevation(), 0.1f);

        assertEquals(0.0, stats.getTotalCalories(), 0.1f);
        assertEquals(0.0, stats.getTotalDistance(), 0.1f);
    }

    @Test
    public void testGetAverageSpeed(){
        ArrayList<Activity> activities = ActivityTest.createTestActivities();

        Statistics stats = new Statistics();
        stats.analyse(activities);

        assertEquals(0.0, stats.getAverageSpeed(), 0.1f);
    }

    @Test
    public void testGetAverageHeartRate(){
        ArrayList<Activity> activities = ActivityTest.createTestActivities();

        Statistics stats = new Statistics();
        stats.analyse(activities);

        assertEquals(0.0, stats.getAverageSpeed(), 0.1f);
    }

    /**
     * Run the float getGreatestSpeed() method test.
     *
     * @throws Exception
     *
     */
    @Test
    public void testGetGreatestSpeedUnmoving() throws Exception
    {
    	ArrayList<DataPoint> dataPoints = new ArrayList<DataPoint>();
    	dataPoints.add(new DataPoint(new Date(),0.0f,new Location(0.0f,0.0f,0.0f),null));
    	DataPoint previousPoint = dataPoints.get(0);
    	for (int i = 0; i < 500; i++)
    	{
    		dataPoints.add(new DataPoint(new Date(),0.0f,new Location(0.0f,0.0f,0.0f),previousPoint));
    		previousPoint = dataPoints.get(i+1);
    	}
    	
    	Collection<Activity> activities = new ArrayList<Activity>();
    	Activity activity = new Activity("something", dataPoints);
    	activities.add(activity);
    	
        Statistics fixture = new Statistics();
        fixture.analyse(activities);

        float result = fixture.getGreatestSpeed();

        assertEquals(0.0f, result, 0.1f);
    }
    
    /**
     * Run the float getGreatestSpeed() method test.
     *
     * @throws Exception
     *
     */
    @Test
    public void testGetGreatestSpeedMoving() throws Exception
    {
    	ArrayList<DataPoint> dataPoints = new ArrayList<DataPoint>();
    	dataPoints.add(new DataPoint(new Date(),0.0f,new Location(0.0f,0.0f,0.0f),null));
    	DataPoint previousPoint = dataPoints.get(0);
    	for (int i = 0; i < 500; i++)
    	{
    		dataPoints.add(new DataPoint(new Date(),0.0f,new Location(1000*i,1000*i,0.0f),previousPoint));
    		previousPoint = dataPoints.get(i+1);
    	}
    	
    	Collection<Activity> activities = new ArrayList<Activity>();
    	Activity activity = new Activity("something", dataPoints);
    	activities.add(activity);
    	
        Statistics fixture = new Statistics();
        fixture.analyse(activities);

        float result = fixture.getGreatestSpeed();

        assertEquals(0.0f, result, 0.1f);
    }

    /**
     * Run the float getHighestElevation() method test.
     *
     * @throws Exception
     *
     */
    @Test
    public void testGetHighestElevation() throws Exception {
    	ArrayList<DataPoint> dataPoints = new ArrayList<DataPoint>();
    	dataPoints.add(new DataPoint(new Date(),0.0f,new Location(0.0f,0.0f,0.0f),null));
    	DataPoint previousPoint = dataPoints.get(0);
    	for (int i = 0; i < 500; i++)
    	{
    		dataPoints.add(new DataPoint(new Date(),0.0f,new Location(1000*i,1000*i,i),previousPoint));
    		previousPoint = dataPoints.get(i+1);
    	}
    	
    	Collection<Activity> activities = new ArrayList<Activity>();
    	Activity activity = new Activity("something", dataPoints);
    	activities.add(activity);
    	
        Statistics fixture = new Statistics();
        fixture.analyse(activities);

        float result = fixture.getHighestElevation();

        assertEquals(499, result, 0.1f);
    }

    /**
     * Run the float getHighestHeartRate() method test.
     *
     * @throws Exception
     *
     */
    @Test
    public void testGetHighestHeartRate() throws Exception {
    	ArrayList<DataPoint> dataPoints = new ArrayList<DataPoint>();
    	dataPoints.add(new DataPoint(new Date(),0.0f,new Location(0.0f,0.0f,0.0f),null));
    	DataPoint previousPoint = dataPoints.get(0);
    	for (int i = 0; i < 500; i++)
    	{
    		dataPoints.add(new DataPoint(new Date(),i,new Location(1000*i,1000*i,0.0f),previousPoint));
    		previousPoint = dataPoints.get(i+1);
    	}
    	
    	Collection<Activity> activities = new ArrayList<Activity>();
    	Activity activity = new Activity("something", dataPoints);
    	activities.add(activity);
    	
        Statistics fixture = new Statistics();
        fixture.analyse(activities);

        float result = fixture.getHighestHeartRate();

        assertEquals(499, result, 0.0f);
    }

    /**
     * Run the float getTotalCalories() method test.
     *
     * @throws Exception
     *
     */
    @Test
    public void testGetTotalCalories() throws Exception {
    	ArrayList<DataPoint> dataPoints = new ArrayList<DataPoint>();
    	dataPoints.add(new DataPoint(new Date(),0.0f,new Location(0.0f,0.0f,0.0f),null));
    	DataPoint previousPoint = dataPoints.get(0);
    	for (int i = 0; i < 500; i++)
    	{
    		dataPoints.add(new DataPoint(new Date(),i,new Location(1000*i,1000*i,0.0f),previousPoint));
    		previousPoint = dataPoints.get(i+1);
    	}
    	
    	Collection<Activity> activities = new ArrayList<Activity>();
    	Activity activity = new Activity("something", dataPoints);
    	activities.add(activity);
    	
        Statistics fixture = new Statistics();
        fixture.analyse(activities);

        float result = fixture.getTotalCalories();

        assertEquals(0.0f, result, 0.0f);
    }

    /**
     * Run the float getTotalDistance() method test.
     *
     * @throws Exception
     *
     */
    @Test
    public void testGetTotalDistance() throws Exception {
    	ArrayList<DataPoint> dataPoints = new ArrayList<DataPoint>();
    	dataPoints.add(new DataPoint(new Date(),0.0f,new Location(0.0f,0.0f,0.0f),null));
    	DataPoint previousPoint = dataPoints.get(0);
    	for (int i = 0; i < 500; i++)
    	{
    		dataPoints.add(new DataPoint(new Date(),i,new Location(1000*i,1000*i,0.0f),previousPoint));
    		previousPoint = dataPoints.get(i+1);
    	}
    	
    	Collection<Activity> activities = new ArrayList<Activity>();
    	Activity activity = new Activity("something", dataPoints);
    	activities.add(activity);
    	
        Statistics fixture = new Statistics();
        fixture.analyse(activities);

        float result = fixture.getTotalDistance();

        assertEquals(4.6496804E9, result, 0.1f*Math.pow(10, 3));	// fp error
    }

    /**
     * Launch the test.
     *
     * @param args the command line arguments
     *
     */
    public static void main(String[] args) {
        new org.junit.runner.JUnitCore().run(StatisticsTest.class);
    }
}