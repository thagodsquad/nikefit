package tests.analysis.calories;

import analysis.calories.CalorieAnalyser;
import junit.framework.TestCase;
import model.DataPoint;
import model.Gender;
import org.junit.Test;
import profiles.UserProfile;
import tests.model.ActivityTest;
import tests.model.DataPointTest;
import tests.profiles.UserProfileTest;

import java.util.ArrayList;
import java.util.Date;

/**
 * The class <code>CalorieAnalyserTest</code> contains tests for the class <code>{@link CalorieAnalyser}</code>.
 *
 * @author Matthew
 * @version $Revision: 1.0 $
 */
public class CalorieAnalyserTest extends TestCase {
	/**
	 * Run the float getCalories(Collection<Activity>,UserProfile) method test.
	 *
	 * @throws Exception
	 *
	 */
	@Test
	public void testGetCalories()
		throws Exception {
        ArrayList<DataPoint> points = DataPointTest.createTestPoints();
        new CalorieAnalyser();

        UserProfile profile = UserProfileTest.createTestProfile();

		float result = CalorieAnalyser.calculateCaloriesAtDataPoint(points.get(0), points.get(1), profile);
		// add additional test code here
		assertEquals(0.0f, result, 0.1f);

        profile = new UserProfile("John Smith", new Date(), 1, 100, Gender.Male, ActivityTest.createTestActivities());
        result = CalorieAnalyser.calculateCaloriesAtDataPoint(points.get(0), points.get(1), profile);

        assertEquals(0.0, result, 0.1f);
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(CalorieAnalyserTest.class);
	}
}