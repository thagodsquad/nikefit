package tests.analysis.health;

import analysis.health.BradycardiaAnalyser;
import analysis.health.HealthWarning;
import junit.framework.TestCase;
import org.junit.Test;
import tests.model.ActivityTest;
import tests.profiles.UserProfileTest;

/**
 * The class <code>BradycardiaAnalyserTest</code> contains tests for the class <code>{@link BradycardiaAnalyser}</code>.
 *
 * @author Matthew
 * @version $Revision: 1.0 $
 */
public class BradycardiaAnalyserTest extends TestCase{

    @Test
    public void testGetWarningUrl() throws Exception {
        HealthWarning w = new BradycardiaAnalyser();
        assertEquals("http://en.wikipedia.org/wiki/Bradycardia", w.getWarningUrl());
    }

    @Test
    public void testGetWarningName() throws Exception {
        HealthWarning w = new BradycardiaAnalyser();
        assertEquals("Bradycardia", w.getWarningName());
    }

    @Test
    public void testGetWarningDescription() throws Exception {
        HealthWarning w = new BradycardiaAnalyser();
        assertEquals("Your resting heart rate is abnormally low (<60bpm), we recommend you see a health professional.", w.getWarningDescription());
    }

	/**
	 * Run the boolean analyse(UserProfile) method test.
	 *
	 * @throws Exception
	 *
	 */
	@Test
	public void testAnalyse() {
        HealthWarning t = new BradycardiaAnalyser();
        assertEquals(false, t.analyse(UserProfileTest.createTestProfile(), ActivityTest.createTestActivitiesWithTachycardia()));
        assertEquals(true, t.analyse(UserProfileTest.createTestProfile(), ActivityTest.createTestActivitiesWithBradycardia()));
    }

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(BradycardiaAnalyserTest.class);
	}
}