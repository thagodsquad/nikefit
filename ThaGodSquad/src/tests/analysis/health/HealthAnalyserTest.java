package tests.analysis.health;

import analysis.health.BradycardiaAnalyser;
import analysis.health.HealthAnalyser;
import analysis.health.HealthWarning;
import analysis.health.TachycardiaAnalyser;
import junit.framework.TestCase;
import model.Activity;
import org.junit.Test;
import profiles.UserProfile;
import tests.model.ActivityTest;
import tests.profiles.UserProfileTest;

import java.util.ArrayList;

/**
 * The class <code>HealthAnalyserTest</code> contains tests for the class <code>{@link HealthAnalyser}</code>.
 *
 * @author Matthew
 * @version $Revision: 1.0 $
 */
public class HealthAnalyserTest extends TestCase{
	/**
	 * Run the Collection<HealthWarning> getWarnings(UserProfile) method test.
	 *
	 * @throws Exception
	 *
	 */
	@Test
	public void testGetWarnings()
		throws Exception {
		UserProfile profile = UserProfileTest.createTestProfile();

        ArrayList<Activity> activities = new ArrayList<>();
        activities.addAll(ActivityTest.createTestActivitiesWithBradycardia());

		ArrayList<HealthWarning> warnings = (ArrayList<HealthWarning>)HealthAnalyser.getWarnings(profile, activities);
        assertEquals(2, warnings.size());
        assertEquals(true, warnings.get(0) instanceof BradycardiaAnalyser);

        activities.clear();
        activities.addAll(ActivityTest.createTestActivitiesWithTachycardia());
        warnings = (ArrayList<HealthWarning>)HealthAnalyser.getWarnings(profile, activities);
        assertEquals(2, warnings.size());
        assertEquals(true, warnings.get(0) instanceof TachycardiaAnalyser);

        activities.addAll(ActivityTest.createTestActivitiesWithBradycardia());
        warnings = (ArrayList<HealthWarning>)HealthAnalyser.getWarnings(profile, activities);
        assertEquals(3, warnings.size());
        assertEquals(true, warnings.get(0) instanceof TachycardiaAnalyser || warnings.get(1) instanceof TachycardiaAnalyser);
        assertEquals(true, warnings.get(0) instanceof BradycardiaAnalyser || warnings.get(1) instanceof BradycardiaAnalyser);

        activities.clear();
        warnings = (ArrayList<HealthWarning>)HealthAnalyser.getWarnings(profile, activities);
        assertEquals(0, warnings.size());
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(HealthAnalyserTest.class);
	}
}