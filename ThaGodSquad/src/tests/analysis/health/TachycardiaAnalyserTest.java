package tests.analysis.health;

import analysis.health.HealthWarning;
import analysis.health.TachycardiaAnalyser;
import junit.framework.TestCase;
import model.Activity;
import org.junit.Test;
import tests.model.ActivityTest;
import tests.profiles.UserProfileTest;

import java.util.ArrayList;

/**
 * The class <code>TachycardiaAnalyserTest</code> contains tests for the class <code>{@link TachycardiaAnalyser}</code>.
 *
 * @author Matthew
 * @version $Revision: 1.0 $
 */
public class TachycardiaAnalyserTest extends TestCase {

    @Test
    public void testGetWarningUrl() throws Exception {
        HealthWarning w = new TachycardiaAnalyser();
        assertEquals("http://en.wikipedia.org/wiki/Tachycardia", w.getWarningUrl());
    }

    @Test
    public void testGetWarningName() throws Exception {
        HealthWarning w = new TachycardiaAnalyser();
        assertEquals("Tachycardia", w.getWarningName());
    }

    @Test
    public void testGetWarningDescription() throws Exception {
        HealthWarning w = new TachycardiaAnalyser();
        assertEquals("Your resting heart rate is abnormally high (>100bpm), we recommend you see a health professional.", w.getWarningDescription());
    }

    /**
	 * Run the boolean analyse(UserProfile) method test.
	 *
	 * @throws Exception
	 *
	 */
	@Test
	public void testAnalyse(){
		HealthWarning t = new TachycardiaAnalyser();
        ArrayList<Activity> a = ActivityTest.createTestActivitiesWithTachycardia();

        assertEquals(true, t.analyse(UserProfileTest.createTestProfile(), a));
        assertEquals(false, t.analyse(UserProfileTest.createTestProfile(), ActivityTest.createTestActivitiesWithBradycardia()));
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(TachycardiaAnalyserTest.class);
	}
}