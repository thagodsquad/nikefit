package tests.analysis.health;

import analysis.health.HealthWarning;
import analysis.health.TooYoungWarning;
import junit.framework.TestCase;
import model.Activity;
import model.Gender;
import org.junit.Test;
import profiles.UserProfile;
import tests.model.ActivityTest;

import java.util.ArrayList;
import java.util.Date;

public class TooYoungWarningTest extends TestCase {

    @Test
    public void testGetWarningUrl() throws Exception {
        HealthWarning w = new TooYoungWarning();
        assertEquals("", w.getWarningUrl());
    }

    @Test
    public void testGetWarningName() throws Exception {
        HealthWarning w = new TooYoungWarning();
        assertEquals("You're too young!", w.getWarningName());
    }

    @Test
    public void testGetWarningDescription() throws Exception {
        HealthWarning w = new TooYoungWarning();
        assertEquals("We can't reliably test for tachycardia at your age :'(", w.getWarningDescription());
    }

    @Test
    public void testAnalyse() throws Exception {
        HealthWarning w = new TooYoungWarning();

        ArrayList<Activity> activities = ActivityTest.createTestActivities();
        UserProfile profile = new UserProfile("John Smith", new Date(), 1.8f, 100, Gender.Male, activities);

        assertEquals(true, w.analyse(profile, activities));

        profile = new UserProfile("John Smith", new Date(0), 1.8f, 100, Gender.Male, activities);
        assertEquals(false, w.analyse(profile, activities));
    }
}