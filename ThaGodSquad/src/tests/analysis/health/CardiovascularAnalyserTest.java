package tests.analysis.health;

import analysis.health.CardiovascularAnalyser;
import analysis.health.HealthWarning;
import junit.framework.TestCase;
import org.junit.Test;
import tests.model.ActivityTest;
import tests.profiles.UserProfileTest;

public class CardiovascularAnalyserTest extends TestCase{
    @Test
    public void testGetWarningUrl() throws Exception {
        HealthWarning w = new CardiovascularAnalyser();
        assertEquals("http://en.wikipedia.org/wiki/Cardiovascular_disease", w.getWarningUrl());
    }

    @Test
    public void testGetWarningName() throws Exception {
        HealthWarning w = new CardiovascularAnalyser();
        assertEquals("Cardiovascular Disease", w.getWarningName());
    }

    @Test
    public void testGetWarningDescription() throws Exception {
        HealthWarning w = new CardiovascularAnalyser();
        assertEquals("You could be at risk of potentially developing symptoms of Cardiovascular Disease, however, this tool doesn't have enough data to safely analyse for it. You can safely ignore this warning. Unless your heart rate is 0. In which case, you might have cardiovascular mortality (no heartbeat). Then see a doctor", w.getWarningDescription());
    }

    @Test
    public void testAnalyse() throws Exception {
        HealthWarning t = new CardiovascularAnalyser();
        assertEquals(true, t.analyse(UserProfileTest.createTestProfile(), ActivityTest.createTestActivitiesWithTachycardia()));
        assertEquals(true, t.analyse(UserProfileTest.createTestProfile(), ActivityTest.createTestActivitiesWithBradycardia()));
    }
}