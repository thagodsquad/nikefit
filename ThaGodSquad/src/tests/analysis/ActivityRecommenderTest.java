package tests.analysis;

import analysis.ActivityRecommender;
import junit.framework.TestCase;
import model.Activity.ActivityType;
import org.junit.Test;
import profiles.UserProfile;
import tests.profiles.UserProfileTest;

import java.util.ArrayList;

/**
 * The class <code>ActivityRecommenderTest</code> contains tests for the class <code>{@link ActivityRecommender}</code>.
 *
 * @author Matthew
 * @version $Revision: 1.0 $
 */
public class ActivityRecommenderTest extends TestCase {
	/**
	 * Run the getRecommendedActivity(UserProfile) method test.
	 *
	 * @throws Exception for any errors
	 */
	@Test
	public void testGetRecommendedActivity() throws Exception
	{
		UserProfile profile = UserProfileTest.createTestProfile();
        new ActivityRecommender();
		ActivityType result = ActivityRecommender.getRecommendedActivity(profile);
		assertNotNull(result);
		assertTrue(result instanceof ActivityType);
		ArrayList<String> activities = new ArrayList<String>();
		for (ActivityType activity : ActivityType.values())
		{
			activities.add(activity.toString());
		}
		assertTrue(activities.contains(result.toString()));
	}

	/**
	 * Launch the test.
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ActivityRecommenderTest.class);
	}
}