package tests.persistence;

import junit.framework.TestCase;
import model.Activity;
import model.DataPoint;
import model.Location;
import org.junit.Test;
import persistence.CSVActivitySerializer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class CSVActivitySerializerTest extends TestCase{

    @Test
    public void testLoad() throws Exception {
        testSampleData("sample_data.csv", 12);
        ArrayList<Activity> as = testSampleData("bradycardia_and_trachycardia.csv", 2);
        testSampleData("fails.csv", 0);
        ArrayList<Activity> ls = testSampleData("wrong_name.csv", 2);

        assertTrue(ls.get(0).getName() == "Unknown Activity");

        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy,HH:mm:ss");
        ArrayList<DataPoint> points = new ArrayList<>();
        for (Activity a : as){
            for (DataPoint p : a.getPoints()){
                points.add(p);
            }
        }

        assertEquals(f.parse("10/04/2005,23:42:28"), points.get(0).getDate());
        assertEquals(true, (new Location(30.2553368f, -97.83891084f, 239.5f)).equals(points.get(0).getLocation()));
        assertEquals(0, points.get(0).getDistance(), 0.1f);
        assertEquals(0, points.get(0).getCalories(), 0.1f);
        assertEquals(0, points.get(0).getHeartRate(), 0.1f);
        assertEquals(0, points.get(0).getSpeed(), 0.1f);

        assertEquals(f.parse("10/04/2005,23:42:28"), points.get(1).getDate());
        assertEquals(true, (new Location(30.2553368f, -97.83891084f, 239.5f)).equals(points.get(1).getLocation()));
        assertEquals(0, points.get(1).getDistance(), 0.1f);
        assertEquals(0, points.get(1).getCalories(), 0.1f);
        assertEquals(1000, points.get(1).getHeartRate(), 0.1f);
        assertEquals(0, points.get(1).getSpeed(), 0.1f);
    }

    private ArrayList<Activity> testSampleData(String name, int expected) throws Exception{
        String path = getClass().getResource("sample data/" + name).getFile();
        path = path.replaceAll("%20", " ");

        CSVActivitySerializer s = new CSVActivitySerializer(path);
        ArrayList<Activity> activities = null;
        try {
            activities = s.loadData();
        }catch (Exception e){
        }

        if (activities != null) {
            for (Activity a : activities) {
                assertNotNull(a.getName());
                assertEquals(true, !a.getName().isEmpty());

                assertNotNull(a.getPoints());

                for (DataPoint p : a.getPoints()) {
                    assertNotNull(p.getLocation());
                    assertNotNull(p.getDate());
                }
            }
        }

        if (activities != null)
            assertEquals(activities.size(), expected);
        else assertEquals(0, expected);

        return activities;
    }
}