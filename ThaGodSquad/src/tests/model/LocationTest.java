package tests.model;

import junit.framework.TestCase;
import model.Location;
import org.junit.Test;

public class LocationTest extends TestCase{

	@Test
	public void testSetup()
	{
		Location a = new Location(30.2553368f, -97.83891084f, 15);
		assertTrue(a.getLatitude() == 30.2553368f);
		assertTrue(a.getLongitude() == -97.83891084f);
		assertTrue(a.getElevation() == 15);
	}
	
	@Test
	public void testEquals()
	{
		Location a = new Location(30.2553368f, -97.83891084f, 15);
		Location b = new Location(30.2553368f, -97.83891084f, 0);
        Location c = new Location(30.25499189f, -97.83913958f, 0);
        
        assertTrue(a.equals(a));
        assertFalse(a.equals(b));
        assertTrue(b.equals(b));
        assertFalse(b.equals(c));
        assertFalse(c.equals(null));
	}
	
    @Test
    public void testDistance(){
        Location a = new Location(30.2553368f, -97.83891084f, 0);
        Location b = new Location(30.25499189f, -97.83913958f, 0);

        float dist = Location.haverSineDistance(a, b);
        System.out.println(dist);
        assertEquals(44.2f, dist, 0.05f);
    }
    
    @Test
    public void testDistance2()
    {
    	Location a = new Location(36.12f,-86.67f, 0);
    	Location b = new Location(33.94f,-118.40f, 0);
    	
    	float distance = Location.haverSineDistance(a, b);
    	System.out.println(distance);
    	assertEquals(distance, 2886000f, 5000f);
    }
    
    @Test
    public void testEarthsRadius()
    {
    	Location loc = new Location(36.12f,-86.67f, 0);
    	double radius = Location.earthRadiusAtLocation(loc);
    	System.out.println("Radius = " + radius + " meters");
    	assertTrue(6384400 >= radius);
    	assertTrue(6352800 <= radius);
    }
    
    @Test
    public void testEarthsRadius2()
    {
    	Location loc = new Location(-84.049622f, 78.593750f, 0);
    	double radius = Location.earthRadiusAtLocation(loc);
    	System.out.println("Radius = " + radius + " meters");
    	assertTrue(6384400 >= radius);
    	assertTrue(6352800 <= radius);
    }
    
    @Test
    public void testEarthsRadius3()
    {
    	Location loc = new Location(-41.492480f, 173.971558f, 0);
    	double radius = Location.earthRadiusAtLocation(loc);
    	System.out.println("Radius = " + radius + " meters");
    	assertTrue(6384400 >= radius);
    	assertTrue(6352800 <= radius);
    }
    
    @Test
    public void testEarthsRadius4()
    {
    	Location loc = new Location(84.198680f, 147.252808f, 0);
    	double radius = Location.earthRadiusAtLocation(loc);
    	System.out.println("Radius = " + radius + " meters");
    	assertTrue(6384400 >= radius);
    	assertTrue(6352800 <= radius);
    }
    
    @Test
    public void testEarthsRadius5()
    {
    	Location loc = new Location(48.472603f, 13.659058f, 0);
    	double radius = Location.earthRadiusAtLocation(loc);
    	System.out.println("Radius = " + radius + " meters");
    	assertTrue(6384400 >= radius);
    	assertTrue(6352800 <= radius);
    }
    
    @Test
    public void testEarthsRadius6()
    {
    	Location loc = new Location(-2.438708f, -100.598755f, 0);
    	double radius = Location.earthRadiusAtLocation(loc);
    	System.out.println("Radius = " + radius + " meters");
    	assertTrue(6384400 >= radius);
    	assertTrue(6352800 <= radius);
    }
}