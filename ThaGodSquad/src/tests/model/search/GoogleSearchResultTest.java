package tests.model.search;

import junit.framework.TestCase;
import model.search.GoogleSearchResult;
import model.search.GoogleSearchResults;
import org.junit.Test;

import java.util.ArrayList;

public class GoogleSearchResultTest extends TestCase{

    @Test
    public void testUrl() throws Exception {
        GoogleSearchResult r = new GoogleSearchResult();

        r.setUrl("Foo");
        assertEquals("Foo", r.getUrl());
    }

    @Test
    public void testTitle() throws Exception {
        GoogleSearchResult r = new GoogleSearchResult();

        r.setTitle("Foo");
        assertEquals("Foo", r.getTitle());
    }

    @Test
    public void testContent() throws Exception {
        GoogleSearchResult r = new GoogleSearchResult();

        r.setContent("Foo");
        assertEquals("Foo", r.getContent());
    }

    @Test
    public void testGettersAndSetters() throws Exception
    {
        GoogleSearchResults results = new GoogleSearchResults();
        results.setResponseData(null);
        assertTrue(results.getResponseData() == null);
        GoogleSearchResults.ResponseData responseData = new GoogleSearchResults.ResponseData();
        ArrayList<GoogleSearchResult> res = new ArrayList<GoogleSearchResult>();
        res.add(new GoogleSearchResult());
        responseData.setResults(res);
        assertTrue(responseData.getResults().equals(res));
    }
}