package tests.model.search;

import junit.framework.TestCase;
import model.search.GoogleSearchHelper;
import model.search.GoogleSearchResult;
import org.junit.Test;

import java.util.ArrayList;

public class GoogleSearchHelperTest extends TestCase {

    @Test
    public void testSearch() throws Exception {
        ArrayList<GoogleSearchResult> results = GoogleSearchHelper.Search("foo", 4);

        assertEquals(4, results.size());
        for (int i = 0; i < results.size(); ++i){
            assertNotNull(results.get(i).getContent());
            assertNotNull(results.get(i).getTitle());
            assertNotNull(results.get(i).getUrl());

            /*System.out.println(results.get(i).getTitle());
            System.out.println(results.get(i).getContent());
            System.out.println(results.get(i).getUrl());
            System.out.println();*/
        }
    }

    @Test(expected = java.lang.IllegalArgumentException.class)
    public void maxResultTest() throws Exception
    {
        GoogleSearchHelper.Search("anything", 6);
    }

    @Test(expected = java.lang.IllegalArgumentException.class)
    public void maxResultTest_2() throws Exception
    {
        GoogleSearchHelper.Search("anything", -4);
    }
}