package tests.model;

import junit.framework.TestCase;
import model.DataPoint;
import model.Location;
import org.junit.Test;
import profiles.ProfileManager;
import tests.profiles.loaders.TestProfileLoader;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

public class DataPointTest extends TestCase{

    @Test
    public void testCreateInstance()
    {
    	DataPoint dataPoint = new DataPoint(new Date(), 0.0f, new Location(0, 0, 0), null);

    	assertEquals(dataPoint.getHeartRate(),0.0f, 0.0f);
        assertEquals(dataPoint.getCalories(), 0.0f, 0.0f);
        assertEquals(dataPoint.getDistance(), 0.0f, 0.0f);
        assertEquals(dataPoint.getSpeed(), 0.0f, 0.0f);
    }

    @Test
    public void testCreateInstance_2()
    {
        // test using profile manager
        ProfileManager.CurrentProfileManager = new ProfileManager(new TestProfileLoader());
        DataPoint dataPoint = new DataPoint(new Date(), 0.0f, new Location(0, 0, 0), null);

        assertEquals(dataPoint.getHeartRate(),0.0f, 0.0f);
        assertEquals(dataPoint.getCalories(), 0.0f, 0.0f);
        assertEquals(dataPoint.getDistance(), 0.0f, 0.0f);
        assertEquals(dataPoint.getSpeed(), 0.0f, 0.0f);
    }
    
    @Test
    public void testSpeed() throws Exception
    {
    	DataPoint dataPoint = new DataPoint(new Date(), 0.0f, new Location(0, 0, 0), null);
    	DataPoint dataPoint2 = new DataPoint(new Date(), 0.0f, new Location(0, 0, 0), dataPoint);
    	Thread.sleep(2000);
    	DataPoint dataPoint3 = new DataPoint(new Date(), 0.0f, new Location(50, 50, 10), dataPoint2);

        assertEquals(dataPoint.getSpeed(), 0, 0.0f);
        assertEquals(dataPoint2.getSpeed(), 0, 0.0f);
        assertEquals(dataPoint3.getSpeed(), 3650613.8, 10.0f);
    }

    /**
     * Creates an array of test points
     * @return the test points
     */
    public static ArrayList<DataPoint> createTestPoints(){
        ArrayList<DataPoint> points = new ArrayList<DataPoint>();

        DataPoint first = new DataPoint(Date.from(Instant.EPOCH), 0, new Location(0, 0, 0), null);
        points.add(first);

        DataPoint second = new DataPoint(Date.from(Instant.EPOCH), 0, new Location(0, 0, 0), first);
        points.add(second);

        return points;
    }
    /**
     * A list of data points indicating tachycardia
     * @return The points
     */
    public static ArrayList<DataPoint> createTachycardia(){
        ArrayList<DataPoint> points = new ArrayList<>();
        points.add(new DataPoint(new Date(), 1000, new Location(0, 0, 0), null));
        return points;
    }

    /**
     * A list of data points indicating bradycardia
     * @return The points
     */
    public static ArrayList<DataPoint> createBradycardia(){
        ArrayList<DataPoint> points = new ArrayList<>();
        points.add(new DataPoint(new Date(), 0, new Location(0, 0, 0), null));
        return points;
    }
}