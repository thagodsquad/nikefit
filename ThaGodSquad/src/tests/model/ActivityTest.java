package tests.model;

import junit.framework.TestCase;
import model.Activity;
import model.DataPoint;
import org.junit.Test;

import java.util.ArrayList;

public class ActivityTest extends TestCase {

    /**
     * Tests the name
     */
    @Test
    public void testName(){
        Activity a = new Activity(null, null);
        a.setName("Foo");
        assertEquals("Foo", a.getName());
        assertEquals(a.toString(), "Foo");
    }

    /**
     * Tests the activity type
     */
    public void testType(){
        Activity a = new Activity(null, null);

        a.setActivityType(Activity.ActivityType.Aerobics);
        assertEquals(Activity.ActivityType.Aerobics, a.getActivityType());
    }

    /**
     * Tests get/set for points
     */
    public void testPoint(){
        Activity a = new Activity(null, null);

        ArrayList<DataPoint> points = DataPointTest.createTestPoints();
        a.setPoints(points);
        assertEquals(points, a.getPoints());
    }

    /**
     * Creates an array of test activities
     * @return The test activities
     */
    public static ArrayList<Activity> createTestActivities(){
        ArrayList<Activity> activities = new ArrayList<Activity>();
        activities.add(new Activity("walking the dog", DataPointTest.createTestPoints()));
        return activities;
    }

    /**
     * Creates an array of test activities with bradycardia
     * @return The test activities
     */
    public static ArrayList<Activity> createTestActivitiesWithBradycardia(){
        ArrayList<Activity> activities = new ArrayList<Activity>();
        activities.add(new Activity("walking the dog", DataPointTest.createBradycardia()));
        return activities;
    }

    /**
     * Creates an array of test activities with tachycardia
     * @return The test activities
     */
    public static ArrayList<Activity> createTestActivitiesWithTachycardia(){
        ArrayList<Activity> activities = new ArrayList<Activity>();
        activities.add(new Activity("walking the dog", DataPointTest.createTachycardia()));
        return activities;
    }
}