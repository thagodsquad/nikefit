package tests.gui;

import junit.framework.TestCase;
import gui.InternetSettings;
import org.junit.*;

/**
 * The class <code>InternetSettingsTest</code> contains tests for the class <code>{@link InternetSettings}</code>.
 *
 * @author Matthew
 * @version $Revision: 1.0 $
 */
public class InternetSettingsTest extends TestCase
{
	/**
	 * Run the InternetSettings(double) constructor test.
	 * @throws Exception on assertion failure
	 */
	@Test
	public void testInternetSettings() throws Exception {
		double pollingDelay = 1.0;
		InternetSettings result = new InternetSettings(pollingDelay);
		assertNotNull(result);
		assertEquals(false, result.getIsOffline());
		assertEquals(false, result.hasChanged());
		assertEquals(0, result.countObservers());
	}

	/**
	 * Run the InternetSettings getInternetSettings() method test.
	 * @throws Exception on assertion failure
	 */
	@Test
	public void testGetInternetSettings() throws Exception
	{
		InternetSettings result = InternetSettings.getInternetSettings();
		assertNotNull(result);
		assertFalse(result.getIsOffline());
	}

	/**
	 * Run the boolean getIsOffline() method test.
	 * @throws Exception on assertion failure
	 */
	@Test
	public void testGetIsOffline() throws Exception
	{
		InternetSettings fixture = new InternetSettings(1.0);
		boolean result = fixture.getIsOffline();
		assertEquals(false, result);
	}

	/**
	 * Run the void start() method test.
	 * @throws Exception on assertion failure
	 */
	@Test
	public void testStart() throws Exception
	{
		assertTrue(InternetSettings.start());
	}

	/**
	 * Run the void stop() method test.
	 * @throws Exception on assertion failure
	 */
	@Test
	public void testStop() throws Exception
	{
		assertTrue(InternetSettings.stop());
		assertFalse(InternetSettings.stop());
	}

	/**
	 * Launch the test.
	 * @param args the command line arguments
	 */
	public static void main(String[] args)
	{
		new org.junit.runner.JUnitCore().run(InternetSettingsTest.class);
	}
}