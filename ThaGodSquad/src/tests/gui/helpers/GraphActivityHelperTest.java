package tests.gui.helpers;

import gui.helpers.GraphActivityHelper;
import gui.helpers.GraphActivityHelper.AxisDataType;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.*;

import tests.profiles.UserProfileTest;
import static org.junit.Assert.*;
import model.Activity;
import model.DataPoint;

/**
 * The class <code>GraphActivityHelperTest</code> contains tests for the class <code>{@link GraphActivityHelper}</code>.
 * Please note that this class requires JavaFX so cannot be fully tested.
 */
public class GraphActivityHelperTest {
	/**
	 * Run the GraphActivityHelper(LineChart<?,?>,AxisDataType,AxisDataType) constructor test.
	 * @throws Exception on assertion failure
	 */
	@Test
	public void testGraphActivityHelper() throws Exception
	{
		GraphActivityHelper.AxisDataType xAxis = GraphActivityHelper.AxisDataType.Speed;
		GraphActivityHelper.AxisDataType yAxis = GraphActivityHelper.AxisDataType.Calories;
		GraphActivityHelper result = new GraphActivityHelper(null, xAxis, yAxis);
		assertNotNull(result);
		result = new GraphActivityHelper(null, null, null);
		assertNotNull(result);
	}

	/**
	 * Run the void addActivity(Activity) method test.
	 * @throws Exception on pass
	 */
	@Test(expected = java.lang.Exception.class)
	public void testAddActivity() throws Exception
	{
		GraphActivityHelper result = new GraphActivityHelper(null, null, null);
		Collection<Activity> activities = UserProfileTest.createTestProfile().getActivities();
		result.addActivity((Activity)activities.toArray()[0]);
	}
	
	/**
	 * Run the void addActivity(Activity) method test.
	 * @throws Exception on pass
	 */
	@Test(expected = java.lang.Exception.class)
	public void testAddActivityNull() throws Exception
	{
		GraphActivityHelper result = new GraphActivityHelper(null, AxisDataType.Distance, AxisDataType.Speed);
		result.addActivity(null);
	}
	
	/**
	 * Run the void addActivity(Activity) method test.
	 * @throws Exception on pass
	 */
	@Test(expected = java.lang.Exception.class)
	public void testAddActivityNone() throws Exception
	{
		GraphActivityHelper fixture = new GraphActivityHelper(null, AxisDataType.Distance, AxisDataType.Speed);
		Activity activity = new Activity("", new ArrayList<DataPoint>());
		fixture.addActivity(activity);
	}

	/**
	 * Run the void clear() method test.
	 * @throws Exception on pass
	 */
	@Test(expected = java.lang.NullPointerException.class)
	public void testClear() throws Exception
	{
		GraphActivityHelper fixture = new GraphActivityHelper(null, AxisDataType.Distance, AxisDataType.Speed);
		fixture.clear();
	}

	/**
	 * Launch the test.
	 * @param args the command line arguments
	 */
	public static void main(String[] args)
	{
		new org.junit.runner.JUnitCore().run(GraphActivityHelperTest.class);
	}
}