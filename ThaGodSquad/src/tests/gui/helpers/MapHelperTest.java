package tests.gui.helpers;

import gui.helpers.MapHelper;
import javafx.scene.web.WebEngine;

import org.junit.*;

import tests.profiles.UserProfileTest;
import static org.junit.Assert.*;
import model.Activity;

/**
 * The class <code>MapHelperTest</code> contains tests for the class <code>{@link MapHelper}</code>.
 * Please note that this class uses JavaFX so cannot be fully tested.
 */
public class MapHelperTest
{
	/**
	 * Run the MapHelper(WebEngine) constructor test.
	 */
	@Test
	public void testMapHelper() throws Exception
	{
		WebEngine engine = null;
		MapHelper result = new MapHelper(engine);
		assertNotNull(result);
	}

	/**
	 * Run the void plotPoints(Collection<Activity>) method test.
	 */
	@Test(expected = java.lang.NullPointerException.class)	// will only get this on pass
	public void testPlotPoints_1() throws Exception
	{
		MapHelper fixture = new MapHelper(null);
		fixture.plotPoints(UserProfileTest.createTestProfile().getActivities());
	}

	/**
	 * Run the void plotPoints(Collection<Activity>) method test.
	 */
	@Test(expected = java.lang.NullPointerException.class)	// will only get this on pass
	public void testPlotPoints_2() throws Exception
	{
		MapHelper fixture = new MapHelper(null);
		Activity activity = (Activity) UserProfileTest.createTestProfile().getActivities().toArray()[0];
		fixture.plotPoints(activity);
	}

	/**
	 * Run the void plotPoints(Activity) method test.
	 */
	@Test(expected = java.lang.IndexOutOfBoundsException.class)	// will only get this on pass
	public void testPlotPointsNone() throws Exception
	{
		MapHelper fixture = new MapHelper(null);
		Activity activity = new Activity("");
		fixture.plotPoints(activity);
	}
	
	/**
	 * Run the void plotPoints(Activity) method test.
	 */
	@Test(expected = java.lang.IndexOutOfBoundsException.class)	// will only get this on pass
	public void testPlotPointsNull() throws Exception
	{
		MapHelper fixture = new MapHelper(null);
		Activity activity = new Activity(null);
		fixture.plotPoints(activity);
	}

	/**
	 * Launch the test.
	 * @param args the command line arguments
	 */
	public static void main(String[] args)
	{
		new org.junit.runner.JUnitCore().run(MapHelperTest.class);
	}
}