package tests.gui.helpers.dialoghelper;

import gui.helpers.dialoghelper.DialogHelper;

import org.junit.*;

import static org.junit.Assert.*;

/**
 * The class <code>DialogHelperTest</code> contains tests for the class <code>{@link DialogHelper}</code>.
 * Please note that this class uses JavaFX so cannot be fully tested.
 */
public class DialogHelperTest
{
	/**
	 * Run the void start(Stage) method test.
	 * @throws Exception on assertion failure
	 */
	@Test
	public void testStart() throws Exception
	{
		try
		{
			DialogHelper fixture = new DialogHelper();
			fixture.start(null);
			assertTrue(false);	// fail, should not have got this far
		}
		catch(Exception e)
		{
			assertTrue(e.getMessage() == "Why are you running this?");
		}
	}

	/**
	 * Launch the test.
	 * @param args the command line arguments
	 */
	public static void main(String[] args)
	{
		new org.junit.runner.JUnitCore().run(DialogHelperTest.class);
	}
}