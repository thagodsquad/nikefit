package tests.profiles.loaders;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import profiles.ProfileManager;
import profiles.UserProfile;
import profiles.loaders.LocalProfileLoader;
import tests.profiles.UserProfileTest;

import java.util.ArrayList;

/**
 * The class <code>LocalProfileLoaderTest</code> contains tests for the class
 * {@link <code>LocalProfileLoader</code>}
 * @author Matthew
 */
public class LocalProfileLoaderTest extends TestCase {
    private ProfileManager manager;

    private UserProfile validProfile;

    public LocalProfileLoaderTest() throws Exception{
        setup();
    }

    @Before
    public void setup() throws Exception{
        this.manager = new ProfileManager(new LocalProfileLoader());
        validProfile = UserProfileTest.createTestProfile();

        manager.saveProfile(validProfile);
    }

    @Test
    public void testDeleteProfile() throws Exception{
        manager.saveProfile(UserProfileTest.createTestProfile("SOME RANDOM pRoFile thAt does exists"));

        manager.deleteProfile("SOME RANDOM pRoFile thAt! doesnt' exists");
        assertEquals(false, manager.deleteProfile("SOME RANDOM pRoFile thAt! doesnt' exists"));

        assertEquals(true, manager.deleteProfile("SOME RANDOM pRoFile thAt does exists"));
        assertEquals(false, manager.deleteProfile("SOME RANDOM pRoFile thAt does exists"));
    }

    @Test
    public void testProfileExists(){
        assertEquals(true, manager.profileExists(validProfile.getName()));

        manager.deleteProfile("SOME RANDOM pRoFile thAt! doesnt' exists");
        assertEquals(false, manager.profileExists("SOME RANDOM pRoFile thAt! doesnt' exists"));
    }

    @Test
    public void testGetProfiles(){
        ArrayList<String> existing = manager.getProfiles();

        boolean has = false;
        for (String p : existing) {
            if (p.equals(validProfile.getName()))
                has = true;
            assertEquals(true, manager.profileExists(p));
        }

        assertEquals(true, has);
    }

    @Test
    public void testLoadProfile(){
        UserProfile loaded = manager.loadProfile(validProfile.getName());

        assertEquals(validProfile.getName(), loaded.getName());
        assertEquals(validProfile.getGender(), loaded.getGender());
        assertEquals(validProfile.getBMI(), loaded.getBMI());
        assertEquals(validProfile.getBirthDate(), loaded.getBirthDate());
        assertEquals(validProfile.getActivities().size(), loaded.getActivities().size());
        assertEquals(validProfile.getAge(), loaded.getAge());
        assertEquals(validProfile.getWeight(), loaded.getWeight());

        manager.deleteProfile("A NON EXISTING PROFILE GOES HERE PLEASE! REALLY! THIS SHOULD NOT EXIST");
        loaded = manager.loadProfile("A NON EXISTING PROFILE GOES HERE PLEASE! REALLY! THIS SHOULD NOT EXIST");
        assertNull(loaded);
    }
}