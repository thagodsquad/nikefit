package tests.profiles.loaders;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.Test;
import junit.framework.TestCase;
import profiles.UserProfile;
import profiles.loaders.DatabaseProfileLoader;
import profiles.loaders.ProfileLoader;
import tests.profiles.UserProfileTest;

/**
 * The class <code>DatabaseProfileLoaderTest</code> contains tests for the
 * class {@link <code>DatabaseProfileLoader</code>}
 */
public class DatabaseProfileLoaderTest extends TestCase {

	/**
	 * Construct new test instance
	 *
	 * @param name the test name
	 */
	public DatabaseProfileLoaderTest(String name)
	{
		super(name);
		loader = new DatabaseProfileLoader(databaseUrl);
	}

	private final static String databaseUrl = "jdbc:sqlite:junitTests.db";
	private static ProfileLoader loader;
	
	/**
	 * Run the UserProfile loadProfile(String) method test
	 */
	@Test
	public void testLoadProfile() throws Exception
	{
		UserProfile profile = UserProfileTest.createTestProfile();
		assertNotNull(profile);
		
		loader.saveProfile(profile);
		UserProfile profileL = loader.loadProfile(profile.getName());
		assertNotNull(profileL);
		assertEquals(profileL.getName(), profile.getName());
	}
	
	/**
	 * Run the UserProfile loadProfile(String) method test
	 */
	@Test
	public void testLoadProfileFail() throws Exception
	{		
		loader.deleteProfile("Bob");
		UserProfile profileL = loader.loadProfile("Bob");
		assertNull(profileL);
	}

	/**
	 * Run the void saveProfile(UserProfile) method test
	 */
	@Test
	public void testSaveProfile() throws Exception
	{
		UserProfile profile = UserProfileTest.createTestProfile();
		assertNotNull(profile);
		
		loader.deleteProfile(profile.getName()); // delete profile if it exists
		
		loader.saveProfile(profile); // on fail it will throw exception
		
		assertTrue(loader.getProfileList().contains(profile.getName()));
	}

	/**
	 * Run the ArrayList<String> getProfileList() method test
	 */
	@Test
	public void testGetProfileList() throws Exception
	{
		UserProfile profile = UserProfileTest.createTestProfile();
		assertNotNull(profile);
		
		ArrayList<String> profiles1 = loader.getProfileList();
		assertNotNull(profiles1);
		
		loader.saveProfile(profile);
		
		ArrayList<String> profiles2 = loader.getProfileList();
		assertNotNull(profiles2);
		
		assertTrue(profiles1.size() < profiles2.size());
	}
	
	/**
	 * Run the boolean deleteProfile(String) method test
	 */
	@Test
	public void testDeleteProfile() throws Exception
	{
		UserProfile profile = UserProfileTest.createTestProfile();
		assertNotNull(profile);

		loader.saveProfile(profile);
		assertFalse(loader.deleteProfile(profile.getName()));
		assertFalse(loader.deleteProfile(profile.getName()));
	}
	
	/**
	 * Run the boolean deleteProfile(String) method test
	 * Tests for unknown profile
	 */
	@Test
	public void testDeleteProfileNone() throws Exception
	{
		loader.deleteProfile("NoSuchProfile");	// make sure there isnt one there first
		assertFalse(loader.deleteProfile("NoSuchProfile"));
		assertFalse(loader.deleteProfile(null));
	}

    /**
     * Tests creating a table by deleting the existing one
     * and saving a profile.
     */
    @Test
    public void testCreateTable() throws Exception
    {
        File file = new File("junitTests.db");
        file.delete();
        file.deleteOnExit();

        UserProfile profile = UserProfileTest.createTestProfile();
        assertNotNull(profile);
        loader.saveProfile(profile); // on fail it will throw exception
        assertTrue(loader.getProfileList().contains(profile.getName()));
    }

    /**
     * Tests creating a table failure.
     */
    @Test
    public void testCreateTableFail() throws Exception
    {
        try
        {
            UserProfile profile = UserProfileTest.createTestProfile();
            assertNotNull(profile);
            DatabaseProfileLoader loader1 = new DatabaseProfileLoader("blah");
            loader1.saveProfile(profile); // on fail it will throw exception
            fail();
        }
        catch (Throwable e)
        {
        }

    }
}