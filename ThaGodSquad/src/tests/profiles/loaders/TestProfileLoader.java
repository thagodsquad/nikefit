package tests.profiles.loaders;

import profiles.UserProfile;
import profiles.loaders.ProfileLoader;
import tests.profiles.UserProfileTest;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Jay Harris on 23/09/2014.
 */
public class TestProfileLoader implements ProfileLoader {
    /**
     * Gets the existing profiles
     * @return The existing profiles
     */
    public static ArrayList<String> existing(){
        ArrayList<String> ps = new ArrayList<>();
        for (String s : existingProfiles.keySet())
            ps.add(s);
        return ps;
    }

    private static HashMap<String, UserProfile> existingProfiles = new HashMap<>();

    public TestProfileLoader(){
        ArrayList<String> intitial = new ArrayList<>();
        intitial.add("Fred");
        intitial.add("Bob");
        intitial.add("Joe");
        intitial.add("Dave");

        for (String s : intitial)
            existingProfiles.put(s, UserProfileTest.createTestProfile(s));
    }

    @Override
    public UserProfile loadProfile(String profileName) {
        return existingProfiles.get(profileName);
    }

    @Override
    public void saveProfile(UserProfile profile) throws Exception {
        if (existingProfiles.containsKey(profile.getName()))
            existingProfiles.remove(profile.getName());
        existingProfiles.put(profile.getName(), profile);
    }

    @Override
    public ArrayList<String> getProfileList() {
        return existing();
    }

    @Override
    public boolean deleteProfile(String profileName) {
        boolean contains = existingProfiles.containsKey(profileName);
        if (contains)
            existingProfiles.remove(profileName);
        return contains;
    }
}
