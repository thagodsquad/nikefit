package tests.profiles.loaders;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Test;

import junit.framework.TestCase;
import profiles.ProfileManager;
import profiles.UserProfile;
import profiles.loaders.NetworkProfileLoader;
import profiles.loaders.ProfileLoader;
import profiles.network.NetworkProfileServer;

/**
 * The class <code>NetworkProfileLoaderTest</code> contains tests for the class
 * {@link <code>NetworkProfileLoader</code>}
 * @author Matthew
 */
public class NetworkProfileLoaderTest extends TestCase {

	private static ProfileLoader loader;
	private static NetworkProfileServer server;
	private static final String url = "127.0.0.1";
	private static final int port = 7104;
	
	/**
	 * Construct new test instance
	 *
	 * @param name the test name
	 */
	public NetworkProfileLoaderTest(String name) {
		super(name);
		server = new NetworkProfileServer(port, new TestProfileLoader());
		Thread thread = new Thread(server);
		thread.start();
		loader = new NetworkProfileLoader(url, port);
	}
	
	/**
	 * Run the UserProfile loadProfile(String) method test
	 */
	@Test
	public void testLoadProfile() throws Exception
	{
		ProfileManager p = new ProfileManager(new TestProfileLoader());
		loader.saveProfile(p.loadProfile("Bob"));
		UserProfile profile = loader.loadProfile("Bob");
		assertNotNull(profile);
		assertEquals(profile.getName(), "Bob");
	}
	
	/**
	 * Run the UserProfile loadProfile(String) method test
	 */
	@Test
	public void testLoadProfileFail() throws Exception
	{
		UserProfile profile = loader.loadProfile("NoSuchProfile");
		assertNull(profile);
		profile = loader.loadProfile(null);
		assertNull(profile);
	}

	/**
	 * Run the void saveProfile(UserProfile) method test
	 */
	@Test
	public void testSaveProfile() throws Exception
	{
		ProfileManager p = new ProfileManager(new TestProfileLoader());
		loader.saveProfile(p.loadProfile("Bob")); // on fail it will throw exception
	}

	/**
	 * Run the ArrayList<String> getProfileList() method test
	 */
	@Test
	public void testGetProfileList() throws Exception
	{	
		loader.deleteProfile("Bob"); // if test profile exists, delete it
		ArrayList<String> profiles1 = loader.getProfileList();
		ProfileManager p = new ProfileManager(new TestProfileLoader());
		loader.saveProfile(p.loadProfile("Bob"));
		ArrayList<String> profiles2 = loader.getProfileList();
		assertNotNull(profiles2);
		assertTrue(profiles1.size() < profiles2.size());
		assertTrue(profiles2.contains("Bob"));
		assertFalse(profiles1.contains("Bob"));
	}
	
	/**
	 * Run the boolean deleteProfile(String) method test
	 */
	@Test
	public void testDeleteProfile() throws Exception
	{
		ProfileManager p = new ProfileManager(new TestProfileLoader());
		loader.saveProfile(p.loadProfile("Bob"));
		assertTrue(loader.deleteProfile("Bob"));
	}
	
	/**
	 * Run the boolean deleteProfile(String) method test
	 * Tests for unknown profile
	 */
	@Test
	public void testDeleteProfileNone() throws Exception
	{
		assertFalse(loader.deleteProfile("NoSuchProfile"));
		assertFalse(loader.deleteProfile(null));
	}
	
	@After
	public void tearDown()
	{
		server.stop();
	}
}