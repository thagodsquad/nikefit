package tests.profiles;

import junit.framework.TestCase;
import model.Activity;
import model.Gender;
import org.junit.Test;
import profiles.UserProfile;
import tests.model.ActivityTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

/**
 * The class <code>UserProfileTest</code> contains tests for the class <code>{@link UserProfile}</code>.
 *
 * @version $Revision: 1.0 $
 */
public class UserProfileTest extends TestCase{
	/**
	 * Run the Collection<Activity> getActivities() method test.
	 *
	 * @throws Exception
	 *
	 */
	@Test
	public void testGetActivities()
		throws Exception {
		UserProfile fixture = UserProfileTest.createTestProfile();

		Collection<Activity> result =  fixture.getActivities();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the Date getBirthDate() method test.
	 *
	 * @throws Exception
	 *
	 */
	@Test
	public void testGetBirthDate()
		throws Exception {
		UserProfile fixture = UserProfileTest.createTestProfile();

		Date result = fixture.getBirthDate();
        Date expected = new SimpleDateFormat("dd/MM/yyyy").parse("20/08/1995");
		// add additional test code here
		assertEquals(expected, result);
	}

	/**
	 * Run the float getHeight() method test.
	 *
	 * @throws Exception
	 *
	 */
	@Test
	public void testGetHeight(){
		UserProfile fixture = UserProfileTest.createTestProfile();
		float result = fixture.getHeight();

		// add additional test code here
		assertEquals(1.8f, result, 0.1f);
	}

	/**
	 * Run the String getName() method test.
	 *
	 * @throws Exception
	 *
	 */
	@Test
	public void testGetName()
		throws Exception {
		UserProfile fixture = UserProfileTest.createTestProfile();

		String result = fixture.getName();

		// add additional test code here
		assertEquals("John Smith", result);
	}

	/**
	 * Run the String getPicturePath() method test.
	 *
	 * @throws Exception
	 *
	 */
	@Test
	public void testGetPicturePath()
		throws Exception {
		UserProfile fixture = UserProfileTest.createTestProfile();

		String result = fixture.getPicturePath();

		// add additional test code here
		assertEquals(null, result);
	}

	/**
	 * Run the float getWeight() method test.
	 *
	 * @throws Exception
	 *
	 */
	@Test
	public void testGetWeight()
		throws Exception {
		UserProfile fixture = UserProfileTest.createTestProfile();

		float result = fixture.getWeight();

		// add additional test code here
		assertEquals(100, result, 0.1f);
	}

	@Test
	public void testAge() throws ParseException
	{
        int iterations = 1000;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

        Random rand = new Random();

        int year = 0;
        int day = 0;
        int month = 0;

        for (int i = 0; i < iterations; ++i) {
            year = rand.nextInt(1014) + 1000;
            month = rand.nextInt(9);
            day = rand.nextInt(15) + 10;

            Date date = f.parse(String.format("%s/%s/%s", day, month, year));
            UserProfile profile = new UserProfile("", date, 0, 0, null, null);
            //System.out.println(profile.getAge());
            assertEquals(((new Date()).getTime() - profile.getBirthDate().getTime()) / (365 * 24 * 60 * 60 * 1000.0), profile.getAge(), 5);
        }
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(UserProfileTest.class);
	}

    /**
     * Creates a test profile
     * @return The test profile
     */
    public static UserProfile createTestProfile(){
        return createTestProfile("John Smith");
    }

    /**
     * Creates a test profile
     * @param name The name of the profile
     * @return The test profile
     */
    public static UserProfile createTestProfile(String name) {
        try {
            UserProfile n = new UserProfile(name, new SimpleDateFormat("dd/MM/yyyy").parse("20/08/1995"), 1.8f, 100, Gender.Female, ActivityTest.createTestActivities());
            return n;
        }catch (Exception e){return null;}
    }
}