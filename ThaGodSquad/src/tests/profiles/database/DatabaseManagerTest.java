package tests.profiles.database;

import org.junit.Test;

import junit.framework.TestCase;
import profiles.database.DatabaseManager;

/**
 * The class <code>DatabaseManagerTest</code> contains tests for the class
 * {@link <code>DatabaseManager</code>}
 * @author Matthew
 */
public class DatabaseManagerTest extends TestCase {

	private static final String database = "jdbc:sqlite:junitTests.db";
	private static DatabaseManager manager;
	
	/**
	 * Construct new test instance
	 *
	 * @param name the test name
	 */
	public DatabaseManagerTest(String name) {
		super(name);
		manager = new DatabaseManager(database);
	}

	/**
	 * Perform post-test clean up
	 *
	 * @throws Exception
	 *
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception
	{
		super.tearDown();
		manager.disconnect();
	}

	/**
	 * Run the boolean connect() and disconnect() method test
	 */
	public void testConnectDisconnect()
	{
        DatabaseManager failManager = new DatabaseManager("blah");
        assertFalse(failManager.connect());
		assertTrue(manager.connect());
		assertTrue(manager.connect()); // rerun connect
		assertTrue(manager.disconnect());
        assertFalse(manager.disconnect());  // rerun disconnect
	}

	/**
	 * Run the void sendStatement(String, Object[]) method test
	 */
	@Test
	public void testSendStatement() throws Exception
	{
		manager.connect();
		manager.sendStatement("SELECT * FROM UserProfiles", null);
		manager.disconnect();
	}

	/**
	 * Run the ResultSet sendStatementWithResponse(String, Object[]) method
	 * test
	 */
	public void testSendStatementWithResponse() throws Exception
	{
		manager.connect();
		assertNotNull(manager.sendStatementWithResponse("SELECT * FROM UserProfiles", null));
		manager.disconnect();
	}
}