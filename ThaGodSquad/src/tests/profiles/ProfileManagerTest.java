package tests.profiles;

import junit.framework.TestCase;
import org.junit.Test;
import profiles.ProfileManager;
import profiles.UserProfile;
import profiles.loaders.LocalProfileLoader;
import profiles.loaders.ProfileLoader;
import tests.profiles.loaders.TestProfileLoader;

import java.util.ArrayList;

/**
 * The class <code>ProfileManagerTest</code> contains tests for the class <code>{@link profiles.ProfileManager}</code>.
 *
 * @author Matthew
 * @version $Revision: 1.0 $
 */
public class ProfileManagerTest extends TestCase {

    /**
     * Construct new test instance
     *
     * @param name the test name
     */
    public ProfileManagerTest(String name) {
        super(name);
        loader = new LocalProfileLoader();
    }

    private static ProfileLoader loader;

    /**
     * Run the UserProfile loadProfile(String) method test
     */
    @Test
    public void testLoadProfile() throws Exception
    {
        ProfileManager p = new ProfileManager(new TestProfileLoader());
        loader.saveProfile(p.loadProfile("Bob"));
        UserProfile profile = loader.loadProfile("Bob");
        assertNotNull(profile);
        assertEquals(profile.getName(), "Bob");
    }

    /**
     * Run the UserProfile loadProfile(String) method test
     */
    @Test
    public void testLoadProfileFail() throws Exception
    {
        UserProfile profile = loader.loadProfile("NoSuchProfile");
        assertNull(profile);
        profile = loader.loadProfile(null);
        assertNull(profile);
    }

    /**
     * Run the void saveProfile(UserProfile) method test
     */
    @Test
    public void testSaveProfile() throws Exception
    {
        ProfileManager p = new ProfileManager(new TestProfileLoader());
        loader.saveProfile(p.loadProfile("Bob")); // on fail it will throw exception
    }

    /**
     * Run the ArrayList<String> getProfileList() method test
     */
    @Test
    public void testGetProfileList() throws Exception
    {
        ArrayList<String> profiles1 = loader.getProfileList();
        ProfileManager p = new ProfileManager(new TestProfileLoader());
        loader.saveProfile(p.loadProfile("Bob"));
        ArrayList<String> profiles2 = loader.getProfileList();
        assertNotNull(profiles2);
        assertTrue(profiles1.size() < profiles2.size());
    }

    /**
     * Run the boolean deleteProfile(String) method test
     */
    @Test
    public void testDeleteProfile() throws Exception
    {
        ProfileManager p = new ProfileManager(new TestProfileLoader());
        loader.saveProfile(p.loadProfile("Bob"));
        assertTrue(loader.deleteProfile("Bob"));
    }

    /**
     * Run the boolean deleteProfile(String) method test
     * Tests for unknown profile
     */
    @Test
    public void testDeleteProfileNone() throws Exception
    {
        assertFalse(loader.deleteProfile("NoSuchProfile"));
        assertFalse(loader.deleteProfile(null));
    }
}