package tests.profiles.network;

import org.junit.Test;

import junit.framework.TestCase;
import profiles.loaders.LocalProfileLoader;
import profiles.network.NetworkProfileServer;

/**
 * The class <code>NetworkProfileServerTest</code> contains tests for the class
 * {@link <code>NetworkProfileServer</code>}
 * @author Matthew
 */
public class NetworkProfileServerTest extends TestCase {

	/**
	 * Construct new test instance
	 *
	 * @param name the test name
	 */
	public NetworkProfileServerTest(String name) {
		super(name);
		server = new NetworkProfileServer(7104, new LocalProfileLoader());
	}

	private static NetworkProfileServer server;
	
	/**
	 * Run the void run() method test
	 * This method is untestable due to its blocking nature,
	 * therefore will always pass.
	 */
	@Test
	public void testRun()
	{
		return;		// as this method is blocking, it it untestable.
	}

	/**
	 * Run server
	 * Tests of sending too and from the server occur
	 * in tests.profiles.loaders.NetworkProfileLoader
	 */
	@Test
	public void testServer() throws Exception
	{
		Thread thread = new Thread(server);
		thread.start();
		Thread.sleep(3000);
		assertTrue(server.serverIsRunning());
		server.stop();
		Thread.sleep(3000);
		assertFalse(server.serverIsRunning());
	}
}