package tests.profiles.network;

import java.util.ArrayList;

import junit.framework.TestCase;
import profiles.network.NetworkDiscoveryServer;
import profiles.network.NetworkDiscoveryServer.AnnouncedService;

/**
 * The class <code>NetworkDiscoveryServerTest</code> contains tests for the
 * class {@link <code>NetworkDiscoveryServer</code>}
 * @author Matthew
 */
public class NetworkDiscoveryServerTest extends TestCase {

	private static NetworkDiscoveryServer server;
	private static NetworkDiscoveryServer client;
	
	/**
	 * Construct new test instance
	 *
	 * @param name the test name
	 */
	public NetworkDiscoveryServerTest(String name)
	{
		super(name);
		server = new NetworkDiscoveryServer(9999, "TEST", 101);
		client = new NetworkDiscoveryServer(9999, "TEST");
	}

	/**
	 * Run the void abort() method test
	 */
	public void testAbort() throws Exception
	{
		Thread.sleep(5000);
		try
		{
			server.abort();
		}
		catch(Throwable e)	// exception expected given how it is being used.
		{
		}
		
		try
		{
			client.abort();
		}
		catch(Throwable e)	// exception expected given how it is being used.
		{
		}
		
		assertFalse(server.isAlive());
		assertFalse(client.isAlive());
	}

	/**
	 * Run the ArrayList<AnnouncedService> getAnnouncedServices() method test
	 */
	public void testGetAnnouncedServices()
	{
		ArrayList<AnnouncedService> services = server.getAnnouncedServices();
		assertNotNull(services);
		assertNotNull(client.getAnnouncedServices());
	}
}