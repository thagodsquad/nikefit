package gui;

import profiles.ProfileManager;
import profiles.loaders.LocalProfileLoader;

import javax.swing.JOptionPane;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.ServerSocket;

/**
 * Main entry point class for the program.
 * Sets up all mandatory static scope variables and 
 * displays the gui.
 */
public class Program
{	
	/**
	 * Main entry point method for the program.
	 * @param args Command line arguments
	 */
    public static void main(String[] args) throws Exception
    {
    	/*
    	 * Disable stderr output of exceptions unless
    	 * the flag -d or --debug is passed to the program.
    	 */
    	boolean enableStdErr = false;
    	for (String str : args)
    	{
    		if (str.contains("-d") || str.contains("--debug"))
    		{
    			enableStdErr = true;	// search for the debug flag
    		}
    	}
    	if (!enableStdErr)
    	{
    		System.setErr(new PrintStream(new OutputStream() {
    		    public void write(int b) {}		// if stderr disabled, redirect error output
    		}));
    	}
    	
    	/*
    	 * Check Java version. fitterizer has a dependency on JavaFX 2 which is only
    	 * available with JRE8 and above. This can be patched down to a high version
    	 * of JRE7 if various JavaFX controls are removed (eg. the DatePicker).
    	 */
        String version = System.getProperty("java.version");
        int lastDpIndex = version.indexOf('.', 2);				// check jre version
        float versionNum = Float.parseFloat(version.substring(0, lastDpIndex));		
        if (versionNum < 1.8f)
        {	// if not Java 8 or above, show swing dialog
            JOptionPane.showMessageDialog(null, "You have the wrong version of java installed. You can install the latest from:\nhttp://www.oracle.com/technetwork/java/javase/downloads/index.html");
            System.err.println("Out of date Java version. Please update to Java 8.");
            return;	// exit
        }

        /*
         * Prevent more than one application instance. this is done because it causes
         * unfixable issues with all networking related items in the application due to
         * the way the JVM bind works.
         */
        ServerSocket applicationInUseSocket;
        try
        {	// abuse API to make only one instance
        	applicationInUseSocket = new ServerSocket(65535, 1, InetAddress.getLocalHost());
        }
        catch(Exception e)
        {	// show swing dialog (javafx is not yet loaded)
        	JOptionPane.showMessageDialog(null, "Only one instance of this application can run at a time.\n"
                			+ "Please close the other instance before trying again.");
        	System.err.println("To prevent application errors only one instance of this program can run at once.\n"
        			+ "Please stop the other instance before launching another.");
        	return;	// exit
        }
        
        // create profile loader and start discovery
    	ProfileManager.CurrentProfileManager = new ProfileManager(new LocalProfileLoader());
    	ProfileManager.CurrentProfileManager.startServerDiscovery();

        //Initialize our internet settings. This stores it in the static 'InternetSettings' property in the class
        InternetSettings.start();
    	
    	FXApplicationStarter.begin(args);	// run main GUI

        //Stop checking internet connection
        InternetSettings.stop();
    	
    	// on shutdown, stop searching for servers
        ProfileManager.CurrentProfileManager.stopServerDiscovery();
        // if a network profile server is running, stop it
        if (ProfileManager.ProfileServer != null && ProfileManager.ProfileServer.serverIsRunning())
        {
        	ProfileManager.ProfileServer.stop();
        }
        
        try
        {	// allow a new application instance
			applicationInUseSocket.close();
		}
        catch (Exception e){} // we just want it closed, doesn't matter if there is an exception
    }
}
