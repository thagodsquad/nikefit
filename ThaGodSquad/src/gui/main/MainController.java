package gui.main;

import analysis.ActivityRecommender;
import analysis.Statistics;
import analysis.health.*;
import gui.FXApplicationStarter;
import gui.InternetSettings;
import gui.helpers.GraphActivityHelper;
import gui.helpers.MapHelper;
import gui.helpers.dialoghelper.DialogHelper;
import gui.profilemanager.ProfileManagerController;
import javafx.application.Platform;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.chart.LineChart;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.*;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.Activity;
import model.DataPoint;
import model.search.GoogleSearchHelper;
import model.search.GoogleSearchResult;
import persistence.ActivitySerializer;
import persistence.CSVActivitySerializer;
import profiles.ProfileManager;
import profiles.UserProfile;

import java.awt.*;
import java.io.File;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

/**
 * The controller for the main gui of the application. This controls how the gui interacts with the user
 */
public class MainController implements Initializable, Observer{
    public static final String FXMLString = "main/MainView.fxml";
    public final String FITTERIZER_TITLE = "fitterizer";
	
    @FXML
    private Menu fileMenu;

    @FXML
    private MenuItem loadMenuItem;

    @FXML
    private MenuItem clearMenuItem, swapProfileMenuItem;

    @FXML
    private TableView fitnessDataTable;

    @FXML
    private ListView activitiesView;

    @FXML
    private TableColumn dateColumn, timeColumn, heartRateColumn, latitudeColumn, longitudeColumn, elevationColumn, speedColumn, distanceColumn, caloriesColumn;

    @FXML
    private LineChart speedGraph, distanceGraph, heartRateGraph, caloriesGraph;
    
    @FXML
    private WebView mapView;

    @FXML
    private Text totalDistanceText, averageSpeedText, averageHeartRateText, totalCaloriesText;

    @FXML
    private Text nameText, heightText, weightText, bmiText, activitySuggestionLabel;

    @FXML
    private TabPane mainTabs;

    @FXML
    private Tab healthInfoTab;

    @FXML
    private ListView healthWarningsList;
    
    @FXML
    private DatePicker datePicker;

    @FXML
    private Accordion healthInfoAccordion;

    private ArrayList<ListView> healthWarningResultBoxes = new ArrayList<ListView>();
    
    private Statistics statistics;

    private ObservableList<HealthWarning> warnings;
    private ObservableList<DataPoint> dataPoints;
    private ObservableList<Activity> allActivities;
    private ObservableList<Activity> activities;
    private UserProfile currentProfile;

    private Collection<GraphActivityHelper> graphHelpers = new ArrayList<GraphActivityHelper>();
    private MapHelper mapHelper;

    private String googleSearchMessage;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    	datePicker.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                filterActivities();
            }
		});
    	
    	loadMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                LoadFitnessData();
            }
        });

        swapProfileMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Stage mainStage = new Stage();

                try {
                    //Start our ProfileManagerController stage
                    FXApplicationStarter.start(ProfileManagerController.FXMLString, mainStage);
                    //Close our current stage
                    ((Stage) fitnessDataTable.getScene().getWindow()).close();
                }
                catch (Exception e){
                    System.err.println("Unable to start ProfileManager, maybe the FXML is corrupt?");
                }
            }
        });
        
        clearMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dataPoints.clear();
                datePicker.setValue(null);
                activitiesView.getSelectionModel().select(-1);
            }
            
        });

        //Initialize shortcuts
        loadMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN));
        clearMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.K, KeyCombination.CONTROL_DOWN));
        swapProfileMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN));
        
        setupColumns(); //Set up the columns of our 'Raw Data Table'
        setupGraphs(); //Set up all of our graphs
        setupStatistics(); //Set up our statistics system
        setupWarnings(); //Set up our health warnings
        setupMaps(); //Set up our maps
        setupHealthInfo(); //Set up our health info (e.g. the accordion, search results)

        //Setup an observable collection for activities
        activities = FXCollections.observableArrayList();
        allActivities = FXCollections.observableArrayList();
        //Set the items in the activities view to the activities
        activitiesView.setItems(activities);

        //Allow selection of multiple items
        activitiesView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        activitiesView.getSelectionModel().getSelectedItems().addListener(new ListChangeListener() {
            @Override
            public void onChanged(Change c) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        while (warnings.size() > 0)
                            warnings.remove(0);

                        dataPoints.clear();

                        for (TitledPane p : healthInfoAccordion.getPanes()) {
                            p.setDisable(true);
                        }
                        healthInfoAccordion.setExpandedPane(null);


                        for (GraphActivityHelper helper : graphHelpers)
                            helper.clear();

                        for (Object a : activitiesView.getSelectionModel().getSelectedItems()) {
                            Activity activity = (Activity) a;

                            for (GraphActivityHelper helper : graphHelpers)
                                helper.addActivity(activity);

                            dataPoints.addAll(activity.getPoints());
                        }

                        Collection<HealthWarning> newWarnings = HealthAnalyser.getWarnings(activitiesView.getSelectionModel().getSelectedItems());
                        for (HealthWarning warning : newWarnings) {
                            warnings.add(warning);
                            for (TitledPane pane : healthInfoAccordion.getPanes())
                                if (warning.getWarningName().equals(pane.getText()))
                                    pane.setDisable(false);
                        }


                        mapHelper.plotPoints(activitiesView.getSelectionModel().getSelectedItems());
                        statistics.analyse(activitiesView.getSelectionModel().getSelectedItems());
                        refreshStats();
                    }
                });
            }
        });

        //Load the current profile
        currentProfile = ProfileManager.CurrentProfileManager.getCurrentProfile();
        setupProfileInfo();

        //Refresh the profile
        refreshView();

        //Set the offline mode according to the 'offline' property in our internet settings
        InternetSettings.getInternetSettings().addObserver(this);
        //Update the internet state
        update(null, true);
    }

    /**
     * Sets the offline mode
     * @param firstRun Indicates if this is the first run or not
     */
    private void setOfflineMode(boolean firstRun){
        boolean offline = InternetSettings.getInternetSettings().getIsOffline();

        for (Tab tab : mainTabs.getTabs())
            if (tab.getText().toLowerCase().contains("map"))
                tab.setDisable(offline);

        for (ListView view : healthWarningResultBoxes)
                tryGetHealthWarningResults(view);

        if (firstRun && !offline) return;

        Stage stage = (Stage)mapView.getScene().getWindow();
        DialogHelper.showDialog(offline ? "You are now in offline mode. Some functionality will be disabled :(" : "You're online! Everything is back to normal!", offline ? "Uh oh!" : "Yay!", stage);
    }

    /**
     * sets up the search
     */
    private void setupHealthInfo(){
        ArrayList<HealthWarning> warnings = new ArrayList<HealthWarning>();
        warnings.add(new TachycardiaAnalyser());
        warnings.add(new BradycardiaAnalyser());
        warnings.add(new CardiovascularAnalyser());
        warnings.add(new TooYoungWarning());

        for (HealthWarning warning : warnings){
            TitledPane pane = createFor(warning, !(warning instanceof TooYoungWarning));
            pane.setDisable(true);
            healthInfoAccordion.getPanes().add(pane);
        }

        mainTabs.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
            @Override
            public void changed(ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) {
                if (newValue == healthInfoTab){
                    if (googleSearchMessage != null){
                        DialogHelper.showDialog(googleSearchMessage, "Uh oh!", (Stage)mainTabs.getScene().getWindow());
                        googleSearchMessage = null;
                    }
                }
            }
        });
    }

    /**
     * Creates a healthinfo pane for a health warning
     * @param warning The health warning
     * @param includeResults Indicates whether the pane should have a list of google search results
     * @return The pane
     */
    private TitledPane createFor(final HealthWarning warning, boolean includeResults){
        GridPane content = new GridPane();
        RowConstraints firstRow = new RowConstraints(Control.USE_COMPUTED_SIZE, Control.USE_COMPUTED_SIZE, Control.USE_COMPUTED_SIZE);
        firstRow.setVgrow(Priority.NEVER);
        content.getRowConstraints().add(firstRow);
        RowConstraints secondRow = new RowConstraints(Control.USE_COMPUTED_SIZE, Control.USE_COMPUTED_SIZE, Control.USE_COMPUTED_SIZE);
        secondRow.setVgrow(Priority.SOMETIMES);
        secondRow.setValignment(VPos.TOP);
        secondRow.setFillHeight(true);

        content.getRowConstraints().add(secondRow);

        content.getColumnConstraints().add(new ColumnConstraints(10,100, Control.USE_COMPUTED_SIZE){{setHgrow(Priority.SOMETIMES);}});

        //Create a label for the description of the warning
        Label description = new Label();
        //Tell the text to wrap
        description.setWrapText(true);
        //Bind the min height to the height of the text
        description.minHeightProperty().bind(description.prefHeightProperty());
        //Set the text to the description
        description.setText(warning.getWarningDescription());

        //Set the min height for the row
        firstRow.setMinHeight(15);
        //Put the description in the first row, first column
        content.add(description, 0, 0);

        //If this warning wants results
        if (includeResults) {
            //Create a list view for our results
            final ListView searchResults = new ListView();
            //Build a cell factory, so we get to display our data properly
            searchResults.setCellFactory(new Callback<ListView, ListCell<GoogleSearchResult>>() {

                @Override
                public ListCell<GoogleSearchResult> call(ListView p) {
                    return new ListCell<GoogleSearchResult>() {
                        //Text for the title
                        private final Text title;
                        //Text for the description
                        private final Text description;

                        //A VBox to hold the content
                        private final VBox content;

                        {
                            //We're only going to display a graphic (the VBox)
                            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            //Yes, wrap text
                            setWrapText(true);

                            //Initialize our VBox
                            content = new VBox();
                            content.setMargin(content, new Insets(5, 2, 5, 2));

                            //Initialize our Title
                            title = new Text();
                            title.setFont(javafx.scene.text.Font.font("Verdana", FontWeight.BOLD, 14));

                            //Initialize our description
                            description = new Text();
                            description.wrappingWidthProperty().bind(searchResults.widthProperty().subtract(5));

                            //Add the description/title to our VBox
                            content.getChildren().add(title);
                            content.getChildren().add(description);

                            //Set the preferred width and our content
                            setPrefWidth(0);
                            setGraphic(content);
                        }

                        @Override
                        protected void updateItem(GoogleSearchResult item, boolean empty) {
                            super.updateItem(item, empty);

                            //If the item is null, or we should empty the box
                            if (item == null || empty) {
                                //set our content to null
                                setGraphic(null);
                            } else {
                                //Otherwise, update the title and description
                                title.setText(item.getTitle());
                                description.setText(item.getContent());
                                setGraphic(content);
                            }
                        }
                    };
                }
            });

            //Set the selection model, so we can handle click events.
            searchResults.getSelectionModel().getSelectedItems().addListener(new ListChangeListener() {
                @Override
                public void onChanged(Change c) {
                    //If there isn't an item selected, return
                    if (searchResults.getSelectionModel().getSelectedIndex() == -1) return;

                    //Get the currently selected result
                    GoogleSearchResult result = ((GoogleSearchResult) searchResults.getSelectionModel().getSelectedItem());

                    //If this result is a warning, return
                    if (result.getTitle().toLowerCase().contains("uh oh!")) return;

                    //If we passed all those tests, search (or go to the web page)
                    search(result.getUrl());
                }
            });

            //Initialize our results list
            final ObservableList<GoogleSearchResult> results = FXCollections.observableArrayList();
            //Tell our list it should have 'results' as it's items
            searchResults.setItems(results);

            //Set the user data to our warning, so we can access it later
            searchResults.setUserData(warning);

            //Add our search results list box to our content panel, on the second row
            content.add(searchResults, 0, 1, 1, 1);

            //Try and get us some results :)
            tryGetHealthWarningResults(searchResults);

            //Add this results box to our list
            healthWarningResultBoxes.add(searchResults);
        }

        //Create a new anchor pane
        AnchorPane pane = new AnchorPane(content);

        //Set the panes position
        AnchorPane.setTopAnchor(content, 0.0);
        AnchorPane.setBottomAnchor(content, 5.0);
        AnchorPane.setLeftAnchor(content, 5.0);
        AnchorPane.setRightAnchor(content, 5.0);

        return new TitledPane(warning.getWarningName(), pane);
    }

    /**
     * Refreshes the warnings displayed in the different panels
     */
    private void tryGetHealthWarningResults(ListView listView){
        final ObservableList<GoogleSearchResult> results = (ObservableList<GoogleSearchResult>)listView.getItems();
        final HealthWarning warning = (HealthWarning)listView.getUserData();

        //If we have results, no need for this
        if (results.size() > 1) return;
        results.clear();

        //If we're offline, apologize
        if (InternetSettings.getInternetSettings().getIsOffline()){
            //Create a placeholder result, saying you've got no internet
            results.add(new GoogleSearchResult(){{
                setContent("We can't get search results, you've got not internet access :(");
                setTitle("Uh oh!");
            }});
        }
        //If we do have internet
        else {
            //Add a place holder result, saying we couldn't find any results :P. This will be cleared if we get results
            results.add(new GoogleSearchResult() {{
                setContent("Looks like we couldn't get any results! This is probably due to a Google Terms of Service violation (just leave things alone for a while and things should resolve themselves).");
                setTitle("Uh oh!");
            }});

            //Search for relevant results on a new thread
            Thread searchThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    //Get the results
                    loadSearchResults(results, warning.getWarningName());
                }
            });
            //Start the thread
            searchThread.start();
        }
    }

    /**
     * Loads search results into an observable collection from a query
     * @param to The observable collection to store the results in
     * @param query The text to search for.
     */
    private void loadSearchResults(final ObservableList<GoogleSearchResult> to, final String query){
        try{
            final ArrayList<GoogleSearchResult> results = GoogleSearchHelper.Search(query, 8);
            //We have to do this on another thread, or we can get Illegal access exceptions (caused by cross thread access)
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    //Clear our results
                    to.clear();
                    //Add all the ones we've found
                    to.addAll(results);
                }
            });
        }catch(Exception e){
            //If there was an exception, tell the user
            if (e.getMessage().equals("Google Terms of service abuse on your internet connection!")) {
                googleSearchMessage = "You've gone and abused Google's terms and condition! You mightn't get all the relevant health information :'(";
            }
            else googleSearchMessage = "We can't seem to search for you :(\nWe're very sorry.";
        }
    }


    /**
     * Sets up the map helper
     */
    private void setupMaps(){
        mapHelper = new MapHelper(mapView.getEngine());
    }

    /**
     * Sets up the profile info
     */
    private void setupProfileInfo(){
        nameText.setText(currentProfile.getName());
        heightText.setText("" + currentProfile.getHeight());
        weightText.setText("" + currentProfile.getWeight());
        bmiText.setText("" + currentProfile.getBMI());
        
        // suggest an activity
        activitySuggestionLabel.setText(ActivityRecommender.getRecommendedActivity(null).toString());
    }

    /**
     * Sets up the statistics
     */
    private void setupStatistics(){
        statistics = new Statistics();
    }

    /**
     * Sets up the health warnings
     */
    private void setupWarnings(){
        warnings = FXCollections.observableList(new ArrayList<HealthWarning>());

        healthWarningsList.setCellFactory(new Callback<ListView, ListCell<HealthWarning>>() {

            @Override
            public ListCell<HealthWarning> call(ListView p) {
                return new ListCell<HealthWarning>() {
                    private final Text text;
                    {
                        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                        setWrapText(true);
                        text = new Text();
                        text.wrappingWidthProperty().bind(healthWarningsList.widthProperty().subtract(15));

                        setPrefWidth(0);
                        setGraphic(text);
                    }

                    @Override
                    protected void updateItem(HealthWarning item, boolean empty) {
                        super.updateItem(item, empty);

                        if (item == null || empty) {
                            setGraphic(null);
                        } else {
                            String t = item.getWarningName();// + "\n" + item.getWarningDescription();
                            text.setText(t);
                            setGraphic(text);
                        }
                    }
                };
            }

        });

        healthWarningsList.getSelectionModel().getSelectedItems().addListener(new ListChangeListener() {
            @Override
            public void onChanged(Change c) {
                Object current = c.getList().get(0);

                if (c.getList().size() != 1 || !(current instanceof HealthWarning)) return;
                HealthWarning warning = (HealthWarning) current;
                //Note: this won't work if the tabs are changed
                mainTabs.getSelectionModel().select(3);

                for (TitledPane pane : healthInfoAccordion.getPanes()){
                    if (pane.getText().equals(warning.getWarningName()))
                        healthInfoAccordion.setExpandedPane(pane);
                }
            }
        });

        healthWarningsList.setItems(warnings);
    }

    /**
     * Refreshes the statistics
     */
    private void refreshStats(){
        totalDistanceText.setText(String.format("%s", Statistics.round(statistics.getTotalDistance(), 1)));
        totalCaloriesText.setText(String.format("%s", Statistics.round(statistics.getTotalCalories(), 1)));
        averageHeartRateText.setText(String.format("%s", Statistics.round(statistics.getAverageHeartRate(), 1)));
        averageSpeedText.setText(String.format("%s", Statistics.round(statistics.getAverageSpeed(), 1)));
    }

    /**
     * Initializes the graphs
     */
    private void setupGraphs(){
        graphHelpers.add(new GraphActivityHelper(speedGraph, GraphActivityHelper.AxisDataType.Time, GraphActivityHelper.AxisDataType.Speed));
        graphHelpers.add(new GraphActivityHelper(distanceGraph, GraphActivityHelper.AxisDataType.Time, GraphActivityHelper.AxisDataType.Distance));
        graphHelpers.add(new GraphActivityHelper(heartRateGraph, GraphActivityHelper.AxisDataType.Time, GraphActivityHelper.AxisDataType.HeartRate));
        graphHelpers.add(new GraphActivityHelper(caloriesGraph, GraphActivityHelper.AxisDataType.Time, GraphActivityHelper.AxisDataType.Calories));
    }

    /**
     * Sets up the columns for the fitness data table
     */
    private void setupColumns() {
        //The format for the dates, you can change this to change how it displays
        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        //The form for the times, you can change this to change how it is displayed
        final SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");

        //Creates a new observable list
        dataPoints = FXCollections.observableArrayList();

        //Makes gets the date and sets the column to it. What can I say? Java is a mess
        dateColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<DataPoint, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<DataPoint, String> param) {
                StringProperty s = new SimpleStringProperty();
                s.setValue(dateFormat.format(param.getValue().getDate()));

                return s;
            }
        });

        //Makes the time column and configures how the time is displayed
        timeColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<DataPoint, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<DataPoint, String> param) {
                StringProperty s = new SimpleStringProperty();
                s.setValue(timeFormat.format(param.getValue().getDate()));

                return s;
            }
        });

        //This ones easier, it's just set to the property
        heartRateColumn.setCellValueFactory(new PropertyValueFactory<DataPoint, Float>("heartRate"));

        //Another of the big ones. They're not too complicated, but Java is still a mess
        latitudeColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<DataPoint, Float>, ObservableValue<Float>>() {
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<DataPoint, Float> param) {
                FloatProperty p = new SimpleFloatProperty();
                p.setValue(param.getValue().getLocation().getLatitude());

                return p;
            }
        });

        //Sets the longitude and configures how it displays
        longitudeColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<DataPoint, Float>, ObservableValue<Float>>() {
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<DataPoint, Float> param) {
                FloatProperty p = new SimpleFloatProperty();
                p.setValue(param.getValue().getLocation().getLongitude());

                return p;
            }
        });

        //Makes the elevation column. Another easy one, thank god
        elevationColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<DataPoint, Float>, ObservableValue<Float>>() {
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<DataPoint, Float> param) {
                FloatProperty p = new SimpleFloatProperty();
                p.setValue(param.getValue().getLocation().getElevation());

                return p;
            }
        });
        //Makes the Speed column. Rounds speed to two decimal places in a somewhat roundabout way (but it works!!!)
        speedColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<DataPoint, Float>, ObservableValue<Float>>() {
            @Override
            public ObservableValue call(TableColumn.CellDataFeatures<DataPoint, Float> param) {
                FloatProperty p = new SimpleFloatProperty();
                double sp = param.getValue().getSpeed();
                sp = sp * 100.0;
                sp = Math.round(sp);
                sp = sp / 100.0;
           
                		
                p.setValue(sp);

                return p;
            }
        });

        distanceColumn.setCellValueFactory(new PropertyValueFactory<DataPoint, Float>("distance"));
        caloriesColumn.setCellValueFactory(new PropertyValueFactory<DataPoint, Float>("calories"));

        //Sets the data the table is displaying. When the list is updated the points will be do
        fitnessDataTable.setItems(dataPoints);
    }

    /**
     * Presents the user with a file picker and adds a .CSV file to a user profile
     */
    private void LoadFitnessData() {
        //Make a new file chooser
        FileChooser fc = new FileChooser();
        fc.setTitle("Open some fitness data");

        //Set the types of files that are valid (only these ones will show up)
        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("Fitness Data Files (*.csv)", "*.csv");
        //Add the filter to the chooser
        fc.getExtensionFilters().add(extensionFilter);

        //Show the chooser
        //TODO where we have 'null' here we should set it to the current window, so you can't give focus back to it. Not sure how to do this in JavaFX
        File chosenFile = fc.showOpenDialog(null);

        if (chosenFile == null) {
            //TODO Tell the user something hasn't gone right (maybe..)
            return;
        }

        ActivitySerializer serializer = new CSVActivitySerializer(chosenFile.getPath());
        try {
            Collection<Activity> activities = serializer.loadData();
            currentProfile.getActivities().addAll(activities);
            refreshView();
            
        }catch (Exception e){
            DialogHelper.showDialog("Unable to load the file! It may be invalid", "Uh oh!", (Stage)mainTabs.getScene().getWindow());
        }

        try {
            ProfileManager.CurrentProfileManager.saveProfile(currentProfile);
        }catch(Exception e){
            System.err.println("Unable to save profile, it might be open in another program");
        }
    }

    /**
     * Refreshes the view data
     */
    private void refreshView(){
    	allActivities.clear();
        activities.clear();
        dataPoints.clear();

        for (Activity activity : currentProfile.getActivities()) {

            this.activities.add(activity);
            this.allActivities.add(activity);

            if (activities.size() > 0)
                activitiesView.getSelectionModel().select(0);

            refreshStats();
        }
    }
    
    /**
     * Searches for some text.
     * @param query The text to search for.
     */
    private void search(String query)
    {
    	try
    	{	// crude search feature. if time permits a better implementation will be written later
    		String address = "https://www.google.com/search?q=";
    		String charset = "UTF-8";
    		URL url = new URL(query.startsWith("http") ? query : address + URLEncoder.encode(query, charset));
			Desktop.getDesktop().browse(url.toURI());
		}
    	catch (Exception e)
    	{
    		System.err.println("An error occured while attempting to search:\n" + e);
    		DialogHelper.showDialog("Google must have changed their search page....\n"
    				+ "Try searching instead manually from their website.", "Search", null);
		}
    }
    
    /**
     * Clears activities, and then adds activities from allActivities that match the date in the datePicker
	     *
	     */
    private void filterActivities() 
    {
    	activities.clear();
    	if (datePicker.getValue() == null){
    		activities.addAll(allActivities);
    		return;
    	}
    	
    	LocalDate filterDate = datePicker.getValue();

    	for (Activity activity : allActivities) {
    		Object[] dataPoints = activity.getPoints().toArray(); // makes each activity into an easy array
    		
    		LocalDate firstDate = ((DataPoint) dataPoints[0]).getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();; // first dataPoint in the array
    		LocalDate lastDate = ((DataPoint) dataPoints[(dataPoints.length) - 1]).getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();; // last dataPoint
    		
    		if (((lastDate.isAfter(filterDate) && firstDate.isBefore(filterDate)) || filterDate.equals(firstDate)) || filterDate.equals(lastDate))
    		// andy did a thing, if filterDate(date we're searching for) is either between the first or last or IS the first/last)
    		{
    			activities.add(activity);
    		}
    	}
    }

    @Override
    public void update(Observable o, final Object arg) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                setOfflineMode(arg instanceof Boolean ? (boolean)arg : false);
            }
        });
    }
}
