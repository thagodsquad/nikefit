package gui;

import gui.profilemanager.ProfileManagerController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Provides methods for helping start JavaFx applications
 */
public class FXApplicationStarter extends Application {
	/**
	 * Path of the FXML file to load
	 */
    private String fxmlPath;

    /**
     * Creates a new FXApplicationStarter
     */
    public FXApplicationStarter()
    {
        this(ProfileManagerController.FXMLString);
    }

    /**
     * Creates a new FXApplicationStarter
     * @param fxmlPath The path to the fxml to open
     */
    public FXApplicationStarter(String fxmlPath){
        this.fxmlPath = fxmlPath;
    }

    /**
     * Shows a new stage and loads the fxml
     * @param fxmlPath The path to the fxml to open
     * @param primaryStage The stage
     * @throws IOException
     */
    public static void start(String fxmlPath, Stage primaryStage) throws IOException{
        start(fxmlPath, primaryStage, -1, -1);
    }

    /**
     * Shows a new stage and loads the fxml
     * @param fxmlPath The path to the fxml to open
     * @param primaryStage The stage
     * @param width The width. Set to -1 for Auto
     * @param height The height. the height. Set to -1 for default
     * @throws IOException
     */
    public static void start(String fxmlPath, Stage primaryStage, int width, int height)throws IOException{
        intitialize(fxmlPath, primaryStage, width, height);
        primaryStage.show();
    }

    /**
     * Initializes a stage and loads the new FXML but doesn't show it
     * @param fxmlPath The path to the fxml to open
     * @param primaryStage The stage
     * @param width The width. Set to -1 for Auto
     * @param  height the height. Set to -1 for default
     * @throws IOException
     */
    public static void intitialize(String fxmlPath, Stage primaryStage, int width, int height) throws IOException{
        //Make the root node. We load this from the fxml file
        Parent root = FXMLLoader.load(FXApplicationStarter.class.getResource(fxmlPath));

        //Set the window icon
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        primaryStage.getIcons().add(new Image(classLoader.getResourceAsStream(("media/logo.png"))));

        //Set the window title. The conditional statements appends 'offline mode' to the stage title if the application
        //is offline
        primaryStage.setTitle("fitterizer" + (InternetSettings.getInternetSettings().getIsOffline() ? " (offline mode)" : ""));

        //Set the scene ;)
        if (width != -1 && height != -1 )
            primaryStage.setScene(new Scene(root, width, height));
        else primaryStage.setScene(new Scene(root));
    }

    /**
     * Used to start the first application instance
     * @param args
     */
    public static void begin(String[] args){
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        start(fxmlPath, primaryStage);
    }
}
