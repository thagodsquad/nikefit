package gui.profileloader;

import gui.helpers.dialoghelper.DialogHelper;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import profiles.ProfileManager;
import profiles.network.NetworkDiscoveryServer.AnnouncedService;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * The controller for the network profile loader
 */
public class ProfileLoaderController implements Initializable
{
	/**
	 * Location of the FXML for this controller.
	 */
    public static final String FXMLString = "profileloader/ProfileLoaderView.fxml";

    @FXML
    private Button continueButton, cancelButton;

    @FXML
    private TextField hostBox, portBox;
    
    @FXML
    private MenuButton knownServers;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
    	continueButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                save();	// when continue button pressed, call save
            }
        });
    	
    	continueButton.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>
        (){
        	@Override
        	public void handle(KeyEvent keyEvent) {
        		if(keyEvent.getCode()==KeyCode.ENTER)
        		{
        			continueButton.fire();	// when the enter key is pressed on the continue button, fire it
        		}
        	}
        });

        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                close();	// when the cancel button is called, close the window
            }
        });
        
        cancelButton.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>
        (){
        	@Override
        	public void handle(KeyEvent keyEvent) {
        		if(keyEvent.getCode()==KeyCode.ENTER)
        		{
        			cancelButton.fire();	// when enter is used on the cancel button, fire that button
        		}
        	}
        });
        
        if (ProfileManager.CurrentProfileManager.getKnownServers().size() == 0)
        {
			knownServers.setText("No Servers Found");	// inform user that there were no servers found
		}
        
        knownServers.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>()
        {
			@Override
			public void handle(MouseEvent arg0)
			{	// check if there are no known servers
                if (knownServers.getItems().size() == 0)
                { 	// if there isn't display an explanation message
                	String message = "Please check that your server is not blocked by your firewall and is actually started."
                			+ "\n\nPlease Note: this feature will not work on enterprise or corporate networks"
                			+ " due to firewall blocking of broadcasts on UDP port 9493 by default.";
                	DialogHelper.showDialog(message, "No Servers Discovered", (Stage)knownServers.getScene().getWindow());
                }
			}
        });
        
        // add all the known servers to the menu
        ObservableList<MenuItem> menuItems = knownServers.getItems();
		menuItems.clear();
		for (final AnnouncedService service : ProfileManager.CurrentProfileManager.getKnownServers())
		{
			MenuItem item = new MenuItem(service.getServiceAddress());
			item.setOnAction(new EventHandler<ActionEvent>() {
	            @Override
	            public void handle(ActionEvent event) {
	                hostBox.setText(service.getServiceAddress());	// when a known server is selected, set text of dialog
	                String port = Integer.toString(service.getServicePort());
	                portBox.setText(port);
	            }
	        });
			menuItems.add(item); // add the server menu item
		}
		
		knownServers.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>(){
        	@Override
        	public void handle(KeyEvent keyEvent) {
        		if(keyEvent.getCode()==KeyCode.ENTER)
        		{
        			knownServers.fire();	// when the enter key is pressed on the button, fire it
        		}
        	}
        });
    }

    /**
     * Closes the stage
     */
    private void close(){
        Stage s = (Stage)cancelButton.getScene().getWindow();
        s.close();
    }
    
    /**
     * Host returned by the last usage of the dialog
     */
    private static String host = "";
    /**
     * Port returned by the last usage of the dialog
     */
    private static int port = -1;
    
    /**
     * Get the last host/IP the user entered in the dialog.
     * @return host/IP
     */
    public static String getLastHost()
    {
    	return host;
    }
    
    /**
     * Get the last port number the user entered in the dialog.
     * @return port or -1 if the user cancelled the dialog.
     */
    public static int getLastPort()
    {
    	return port;
    }
    
    /**
     * Changes the profile loader to use the network location instead
     */
    private void save(){
        try
        {
        	host = hostBox.getText();		// get host
        	if (host == null || host.isEmpty() || host.trim().isEmpty())
        	{
        		throw new Exception("Cannot have a blank host/ip.");	// check for blank host/IP
        	}
        	
        	if (host.contains(" "))
        	{
        		throw new Exception("Cannot have a host/ip with spaces."); // check for invalid host/IP
        	}
        	
        	try
        	{
        		port = Integer.parseInt(portBox.getText());	// get port
            	if (port <= 0)	// check for invalid port
            	{
            		throw new Exception("Cannot have a negitive or 0 for port number.");
            	}
        	}
        	catch (Exception e)
        	{	// log error and throw error
        		System.err.println("Parsing port number failed with error:\n" + e.getMessage());
        		throw new Exception("Invalid port number provided.", e);
        	}
        	
            close();
        }
        catch (Throwable e)
        {
        	host = "";
        	port = -1;	// set error state
        	String message = e.getMessage();
        	DialogHelper.showDialog(message, "Uh oh!", (Stage)continueButton.getScene().getWindow());
        }
    }
}
