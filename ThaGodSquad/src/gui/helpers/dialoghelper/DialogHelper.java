package gui.helpers.dialoghelper;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Provides helper methods for displaying dialogs to the user,
 * such as a confirm dialog and a alert dialog
 */
public class DialogHelper extends Application 
{
    /**
     * Shows a new dialog
     * @param primaryStage The stage that the dialog is displayed on
     * @param owner The owner of this dialog. Allows dialogs to be modal (e.g. you can't click through)
     * @param message The message the dialog displays
     * @param title The title of the messagebox
     * @param yesNo Is a confirmation dialog
     * @throws Exception
     */
    public void startDialog(Stage primaryStage, Stage owner, String message, String title, boolean yesNo) throws Exception {
        //Make the root node. We load this from the fxml file
        Parent root = FXMLLoader.load(getClass().getResource("DialogHelper.fxml"));
        // Set dialog label
        Label messageLabel = (Label)root.lookup("#messageLabel");
        messageLabel.setText(title);
        // Set dialog details
        Label detailsLabel = (Label)root.lookup("#detailsLabel");
        detailsLabel.setText(message);
        // Set dialog image
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Image iconImage = new Image(classLoader.getResourceAsStream(("media/logo.png")));
        ImageView imageView = (ImageView)root.lookup("#dialogImage");
        imageView.setImage(iconImage);
        //Set the icon
        primaryStage.getIcons().add(iconImage);
        // set owner
        primaryStage.initOwner(owner);
        //Set the window title
        primaryStage.setTitle("");
        //Set the current scene to our root node
        primaryStage.setScene(new Scene(root));
        
        if (yesNo)
        {
        	Button cancelButton = (Button)root.lookup("#cancelButton");
        	cancelButton.setText("No");			// set no text
        	
        	Button okButton = (Button)root.lookup("#okButton");
        	okButton.setText("Yes");			// set select text
        }
        else
        {
        	Button cancelButton = (Button)root.lookup("#cancelButton");
        	cancelButton.setVisible(false);		// remove cancel button
        }
        // Disable window resizing
        primaryStage.setResizable(false);
        primaryStage.initModality(Modality.WINDOW_MODAL);
        //Show the window
        primaryStage.showAndWait();
    }
    
    /**
     * Shows a basic dialog
     * @param message The message the dialog displays
     * @param title The title of the messagebox
     * @param owner The owner of the dialog box
     */
    public static void showDialog(final String message, final String title, final Stage owner)
    {
    	Platform.runLater(new Runnable() {
    		@Override
    		public void run(){
		        try
		        {
		        	Stage dialog = new Stage();
			        DialogHelper helper = new DialogHelper();
					helper.startDialog(dialog, owner, message, title, false);
				}
		        catch (Exception e)
		        {
					// The error dialog got an error. Shit.
					System.err.println(title + ": " + message);
				}
			}
    	});
    }
    
    /**
     * Shows a basic dialog with a Yes/No option
     * @param message The message the dialog displays
     * @param title The title of the messagebox
     * @param owner The dialog box owner
     * @returns true for yes, false for no
     */
    public static boolean showConfirmDialog(final String message, final String title, final Stage owner)
    {
    	Platform.runLater(new Runnable() {
    		@Override
    		public void run()
    		{
		        try
		        {
		        	Stage dialog = new Stage();
			        DialogHelper helper = new DialogHelper();
					helper.startDialog(dialog, owner, message, title, true);
				}
		        catch (Exception e)
		        {
					// The error dialog got an error. Shit.
					System.err.println(title + ": " + message);
				}
    		}
		});
        return DialogHelperController.lastWasOk();
    }

	@Override
	public void start(Stage arg0) throws Exception
	{	// do not run from here, however this must be implemented
		throw new Exception("Why are you running this?");
	}
}
