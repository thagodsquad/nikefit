package gui.helpers.dialoghelper;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * A generic controller for dialog boxes. Manages dialog result,
 * where text should be displayed and other user interaction
 */
public class DialogHelperController implements Initializable
{
	private static boolean lastOk;

    /**
     * Indicates whether the last result of the dialog box was 'Ok'
     * e.g. true if 'Okay' or 'Yes' has been pressed.
     * @return
     */
	public static boolean lastWasOk()
	{
		return lastOk;
	}
	
    @FXML
    private Button cancelButton;

    @FXML
    private Label detailsLabel;

    @FXML
    private HBox actionParent;

    @FXML
    private Button okButton;

    @FXML
    private HBox okParent;

    @FXML
    private Label messageLabel;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		okButton.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent arg0) {
				lastOk = true;
				((Stage)okButton.getScene().getWindow()).close();
			}
		});
		
		cancelButton.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent arg0) {
				lastOk = false;
				((Stage)cancelButton.getScene().getWindow()).close();
			}
		});
	}
}
