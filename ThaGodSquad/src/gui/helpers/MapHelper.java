package gui.helpers;

import javafx.scene.web.WebEngine;
import model.Activity;
import model.DataPoint;
import model.Location;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;

/**
 * A class which provides methods of manipulating maps,
 * such as displaying an activity (or activities) on a map
 */
public class MapHelper {
    private WebEngine engine;
    private String baseHtml;

    private String html;

    /**
     * Constructor
     * @param engine The webengine that the map will be rendered on
     */
    public MapHelper(WebEngine engine){
        this.engine = engine;

        //Try and load our base html. Should never fail, as the file is part of our jar
        try{
            loadBaseHtml();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Loads the base html file (which we manipulate)
     * @throws FileNotFoundException
     */
    private void loadBaseHtml() throws FileNotFoundException {
        String content = "";
        Scanner scanner = new Scanner(getClass().getResourceAsStream("googlemapbase.html"));
        String lineSeparator = System.getProperty("line.separator");

        //Read every lin and add it to our content
        try {
            while (scanner.hasNextLine()) {
                //(scanner.nextLine() + lineSeparator);
                content += scanner.nextLine() + lineSeparator;
            }
        } finally {
            scanner.close();
        }

        baseHtml = content;
    }

    /**
     * Plots an activity on the map
     * @param activity The activity to plot
     */
    public void plotPoints(Activity activity){
        Collection<Activity> activities = new ArrayList<>();
        activities.add(activity);
        plotPoints(activities);
    }

    /**
     * Plots a set of points on a map
     * @param activities The activities to plot
     */
    public void plotPoints(Collection<Activity> activities){
        Object[] a = activities.toArray(); //Cast our activities to an array
        if (a.length == 0) return; //We don't have anything to do if we don't have any activities
        Activity first = (Activity)a[0]; //Get the first activity
        Location start = ((ArrayList<DataPoint>)first.getPoints()).get(0).getLocation();
        String pathsString = "["+genPoints(first)+"]";

        //Add all our points to our string
        for(int i = 1 ;i < activities.size(); i++){
        	first = (Activity)a[i];
            pathsString = String.format(pathsString+"%s", ",["+genPoints(first)+"]");
        }
        
        //Generate our html and load it in our engine
        html = String.format(baseHtml, genPoint(start), pathsString);
        engine.loadContent(html);
    }

    /**
     * Generates the html code for a point
     * @param l The location
     * @return The html
     */
    private String genPoint(Location l){
        return String.format("new google.maps.LatLng(%s, %s),\n", l.getLatitude(), l.getLongitude()); //This generates javascript for making a new point
    }

    /**
     * Generates the html for an activity
     * @param activity The activity
     * @return The html
     */
    private String genPoints(Activity activity){
        String allLocations = "";

        for(DataPoint point : activity.getPoints()){
            allLocations += genPoint(point.getLocation()); //Add javascript for each point to our html
        }

        return allLocations;
    }
}
