package gui.helpers;

import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import model.Activity;
import model.DataPoint;

/**
 * A class which helps manage plotting activities on a graph.
 * It must be given a JavaFx chart and told what data to plot
 * on each axis.
 */
public class GraphActivityHelper
{
    public enum AxisDataType
    {
        Distance,
        Speed,
        Calories,
        HeartRate,
        Time
    }

    /**
     * A helper method for deciding if an axis should display cumulative data or not
     * @param type The axis
     * @return Whether it is cumulative
     */
    private static boolean isCulumative(AxisDataType type)
    {
        return type == AxisDataType.Distance || type == AxisDataType.Calories; //Whether the axis is culumative
    }

    private AxisDataType xAxis;
    private AxisDataType yAxis;

    private LineChart<?, ?> chart;

    /**
     * Creates a new graph activity helper
     * @param chart The chart to plot activities on
     * @param xAxis The data for the x axis
     * @param yAxis The data for the y axis
     */
    public GraphActivityHelper(LineChart<?, ?> chart, AxisDataType xAxis, AxisDataType yAxis){
        this.chart = chart;
        this.xAxis = xAxis;
        this.yAxis = yAxis;
    }

    /**
     * Plots another activity on the graph
     * @param activity
     */
    public void addActivity(Activity activity)
    {
        //Make a new series, and give it the activities name
        XYChart.Series series = new XYChart.Series();
        series.setName(activity.getName());

        //A sum for the y axis. Only really used when we have a culumative
        float ySum = 0;
        DataPoint startPoint = null;

        for (DataPoint p : activity.getPoints()) {
            if (startPoint == null) startPoint = p;

            XYChart.Data<Object, Object> point = getData(p, startPoint);
            //If the axis is culumative, add the sum to our value
            if (isCulumative(yAxis)) {
                ySum += (Float) point.getYValue();
                point.setYValue(ySum);
            }
            series.getData().add(point);
        }

        //Add the series to the chart
        chart.getData().add(series);
    }

    private XYChart.Data<Object, Object> getData(DataPoint point, DataPoint startPoint){
        Object x = null;
        Object y = null;

        //Get a value for x and y
        x =  getValue(point, xAxis, startPoint);
        y = getValue(point, yAxis, startPoint);
        //Return a new point
        XYChart.Data<Object, Object> result = new XYChart.Data<Object, Object>(x, y);
        return result;
    }

    private Object getValue(DataPoint point, AxisDataType type, DataPoint startPoint){
        //Mostly, we just return the value of whatever we're looking at
        if (type == AxisDataType.Calories)
            return point.getCalories();

        if (type == AxisDataType.Distance)
            return point.getDistance();

        if (type == AxisDataType.Speed)
            return point.getSpeed();

        if (type == AxisDataType.HeartRate)
            return point.getHeartRate();

        if (type == AxisDataType.Time)
        {
            //Get the ticks between the two points and divide it by 1000 (convert to second)
            long timeInterval = Math.abs(point.getDate().getTime() - startPoint.getDate().getTime());
            timeInterval /= 1000;
            return timeInterval;
        }

        return null;
    }

    /**
     * Clears the points
     */
    public void clear(){
        chart.getData().clear();
    }
}
