package gui.profilemanager;

import gui.FXApplicationStarter;
import gui.helpers.dialoghelper.DialogHelper;
import gui.main.MainController;
import gui.profileeditor.ProfileEditorController;
import gui.profileloader.ProfileLoaderController;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import profiles.ProfileManager;
import profiles.UserProfile;
import profiles.loaders.DatabaseProfileLoader;
import profiles.loaders.LocalProfileLoader;
import profiles.loaders.NetworkProfileLoader;
import profiles.loaders.ProfileLoader;
import profiles.network.NetworkProfileServer;

import java.net.ConnectException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * The controller for the profile manager
 */
public class ProfileManagerController implements Initializable {
    public static final String FXMLString = "profilemanager/ProfileManagerView.fxml";

    private Stage mainViewStage;

    @FXML
    private ListView<String> profilesList;

    @FXML
    private Button createButton, deleteButton, selectButton;
    
    @FXML
    private MenuItem networkButton, exportButton, importButton, disconnectButton;

    @FXML
    private RadioMenuItem localServerToggleButton;
    
    @FXML
    private Label fitterizerLocation;
    
    private ObservableList<String> profiles;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        selectButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                int selectedIndex = profilesList.getSelectionModel().getSelectedIndex();
                if (selectedIndex == -1) return;

                String selected = profiles.get(selectedIndex);
                if (!ProfileManager.CurrentProfileManager.profileExists(selected)) return;

                UserProfile p = ProfileManager.CurrentProfileManager.loadProfile(selected);
                ProfileManager.CurrentProfileManager.setCurrentProfile(p);

                Stage mainStage = new Stage();

                try {
                   //Initialize our stage
                    FXApplicationStarter.start(MainController.FXMLString, mainStage);
                    //Close our current stage
                    ((Stage)selectButton.getScene().getWindow()).close();
                }
                catch (Exception e){
                    e.printStackTrace();
                    System.err.println("Unable to start MainView, maybe the FXML is corrupt?");
                }
            }
        });
        
        selectButton.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>
        (){
        	@Override
        	public void handle(KeyEvent keyEvent) {
        		if(keyEvent.getCode()==KeyCode.ENTER)
        		{
        			selectButton.fire();
        		}
        	}
        });

        createButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                //ProfileEditorView profileEditor = new ProfileEditorView();

                Stage secondary = new Stage();
                secondary.initOwner(createButton.getScene().getWindow());
                secondary.initModality(Modality.WINDOW_MODAL);
                secondary.setOnHidden(new EventHandler<WindowEvent>() {
                    @Override
                    public void handle(WindowEvent event) {
                        refreshProfiles();
                    }
                });

                try {
                    //If you use the current stage it opens in the current window...
                    FXApplicationStarter.start(ProfileEditorController.FXMLString, secondary);
                }
                catch(Exception e){

                }
            }
        });
        
        createButton.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>
        (){
        	@Override
        	public void handle(KeyEvent keyEvent) {
        		if(keyEvent.getCode()==KeyCode.ENTER)
        		{
        			createButton.fire();
        		}
        	}
        });

        deleteButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                int selected = profilesList.getSelectionModel().getSelectedIndex();
                if (selected == -1) return;
                ProfileManager.CurrentProfileManager.deleteProfile(profiles.get(selected));
                profiles.remove(selected);
            }
        });
        
        deleteButton.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>
        (){
        	@Override
        	public void handle(KeyEvent keyEvent) {
        		if(keyEvent.getCode()==KeyCode.ENTER)
        		{
        			deleteButton.fire();
        		}
        	}
        });
        
        // imports the currently selected profile to the local HD
        importButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	
            	// check that a profile is actually selected
            	int selectedIndex = profilesList.getSelectionModel().getSelectedIndex();
                if (selectedIndex == -1)
                {
                	DialogHelper.showDialog("Please select a profile to import first.", "Import", null);
                	return;
                }
                
                // get profile name and check if it exists
                String selected = profiles.get(selectedIndex);
                if (!ProfileManager.CurrentProfileManager.profileExists(selected))
                {
                	DialogHelper.showDialog("Please select an existing profile to import first.", "Import", null);
                	return;
                }
                
                // load the profile
                final UserProfile p = ProfileManager.CurrentProfileManager.loadProfile(selected);
                
                try
                {
                	// Create Secondary Profile manager
                    ProfileManager localManager = new ProfileManager(new LocalProfileLoader());
                    if (localManager.profileExists(p.getName()))
                    {
                    	// check if you want to overwrite existing profile data
                    	boolean r = DialogHelper.showConfirmDialog("A profile called " + p.getName() + 
                    			" already exists on your local HD. Do you want to overwrite it?", "Import", null);
                    	if (!r)
                    	{
                    		return;	// no selected to overwrite question
                    	}
                    }
                    
                    // save to local profile
                    localManager.saveProfile(p);
                    DialogHelper.showDialog("Your profile has been successfully imported.", "Import", null);
                }
                catch (Exception e)
                {
                	DialogHelper.showDialog("An error occured while importing:\n" + e.getMessage(), "Import", null);
                }
            }
        });
        
        // exports the currently selected profile to a network server
        exportButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	
            	// check that a profile is actually selected
            	int selectedIndex = profilesList.getSelectionModel().getSelectedIndex();
                if (selectedIndex == -1)
                {
                	DialogHelper.showDialog("Please select a profile to export first.", "Export", null);
                	return;
                }
                
                // get profile name and check if it exists
                String selected = profiles.get(selectedIndex);
                if (!ProfileManager.CurrentProfileManager.profileExists(selected))
                {
                	DialogHelper.showDialog("Please select an existing profile to export first.", "Export", null);
                	return;
                }
                
                // load the profile
                final UserProfile p = ProfileManager.CurrentProfileManager.loadProfile(selected);
                
                // Get host/port
                Stage secondary = new Stage();
                secondary.initOwner(deleteButton.getScene().getWindow());
                secondary.initModality(Modality.WINDOW_MODAL);
                secondary.setOnHidden(new EventHandler<WindowEvent>() {
                    @Override
                    public void handle(WindowEvent event)
                    {
                    	if (ProfileLoaderController.getLastPort() == -1)
                    	{
                    		return;	// invalid port, abort
                    	}
                    	try
                        {
                        	// Create Secondary Profile manager
                            ProfileManager networkManager = 
                            		new ProfileManager(new NetworkProfileLoader(ProfileLoaderController.getLastHost(),
                            						ProfileLoaderController.getLastPort()));
                            if (networkManager.profileExists(p.getName()))
                            {
                            	// check if you want to overwrite existing profile data
                            	boolean r = DialogHelper.showConfirmDialog("A profile called " + p.getName() + 
                            			" already exists on the server. Do you want to overwrite it?", "Export", null);
                            	if (!r)
                            	{
                            		return;
                            	}
                            }
                            
                            // save to remote profile
                            networkManager.saveProfile(p);
                            DialogHelper.showDialog("Your profile has been successfully exported.", "Export", null);
                        }
                        catch (Exception e)
                        {
                        	DialogHelper.showDialog("An error occured while exporting:\n" + e.getMessage(), "Export", null);
                        }
                    }
                });

                try {
                	FXApplicationStarter.start(ProfileLoaderController.FXMLString, secondary);	// display the dialog
                }
                catch(Exception e){
                	DialogHelper.showDialog("The required window could not be shown. Please contact us for support.", "Network Server", null);
                }
            }
        });
        
        // Opens a network server instead of the local server
        networkButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	// setup the dialog to open network
                Stage secondary = new Stage();
                secondary.initOwner(deleteButton.getScene().getWindow());
                secondary.initModality(Modality.WINDOW_MODAL);
                secondary.setOnHidden(new EventHandler<WindowEvent>() {
                    @Override
                    public void handle(WindowEvent event) {
                    	try
                    	{
                    		if (ProfileLoaderController.getLastPort() == -1)
                    		{
                    			return;
                    		}
                        	// set the new profile loader
                        	ProfileLoader loader = new NetworkProfileLoader(ProfileLoaderController.getLastHost(), ProfileLoaderController.getLastPort());
                        	loader.getProfileList();	// get the profile list to test if connection was successful
                            disconnectButton.setDisable(false);	// enable the disconnect button
                            importButton.setDisable(false);	// enable importing
                            ProfileManager.CurrentProfileManager.setProfileLoader(loader);
                            refreshProfiles();	// on close, refresh the profiles view to show correct list
                    	}
                    	catch (Exception e)
                    	{
                    		String errorMessage = "";
                    		if (e instanceof ConnectException)
                    		{	// no error message actually received
                    			errorMessage = "Cannot connect to server.";
                    		}
                    		DialogHelper.showDialog("Loading network profiles failed with error."
                    				+ (errorMessage.equals("") ? "" : ("\n" + errorMessage))
                    				+ "\nPlease check you input the correct network settings.",
                    				"Network Profiles", null);
                    		ProfileManager.CurrentProfileManager.setProfileLoader(new LocalProfileLoader());
                            refreshProfiles();	// on close, refresh the profiles view to show correct list
                            disconnectButton.setDisable(true);	// enable the disconnect button
                            importButton.setDisable(true);	// enable importing
                    	}
                    }
                });

                try {
                	FXApplicationStarter.start(ProfileLoaderController.FXMLString, secondary);	// display the dialog
                }
                catch(Exception e){
                	DialogHelper.showDialog("The required window could not be shown. Please contact us for support.", "Network Server", null);
                }
            }
        });

        if (ProfileManager.CurrentProfileManager.getProfileLoader() instanceof NetworkProfileLoader)
        {
        	disconnectButton.setDisable(false);	// if we are connected to a network server, enable disconnect button
        	importButton.setDisable(false);	// enable importing
        }
        else
        {
        	disconnectButton.setDisable(true); // disable disconnect
        	importButton.setDisable(true);	// disable importing
        }
        
        // Disconnects from a network server
        disconnectButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	ProfileLoader loader = new LocalProfileLoader();
            	ProfileManager.CurrentProfileManager.setProfileLoader(loader);
                refreshProfiles();					// on close, refresh the profiles view to show correct list
                disconnectButton.setDisable(true);	// set disabled as we are no longer connected
                importButton.setDisable(true);		// disable importing
            }
        });
        
        if (ProfileManager.ProfileServer != null && ProfileManager.ProfileServer.serverIsRunning())
        {
        	localServerToggleButton.setSelected(true); // the server is running so make sure users know
        }
        
        localServerToggleButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	if (ProfileManager.ProfileServer != null && ProfileManager.ProfileServer.serverIsRunning())
            	{
            		ProfileManager.ProfileServer.stop();		// stop server
            		DialogHelper.showDialog("Server Stopped.", "Server", null);
            		return;
            	}

            	try
                {
            		ProfileLoader loader = new LocalProfileLoader();
            		// if you want a specific port, run as a stand alone server
            		ProfileManager.ProfileServer = new NetworkProfileServer(7104, loader);
            		
                	Thread thread = new Thread(ProfileManager.ProfileServer);
        			thread.start();		// start the server in a new thread
                    
                    // show confirmation dialog
                    DialogHelper.showDialog("Network profile server successfully started.", "Server", (Stage)selectButton.getScene().getWindow());
                }
                catch (Exception e)
                {
                	DialogHelper.showDialog("An error occured while starting the server:\n"
                			+ e.getMessage()
                			+ "\nPlease contact us for assistance.", "Server", null);
                }
            }
        });
        
        profiles = FXCollections.observableArrayList();
        profilesList.setItems(profiles);

        profilesList.getSelectionModel().getSelectedItems().addListener(new ListChangeListener<Object>() {
            @Override
            public void onChanged(Change<?> c) {
                boolean validSelection = profilesList.getSelectionModel().getSelectedIndex() != -1;
                deleteButton.setDisable(!validSelection);
                selectButton.setDisable(!validSelection);
            }
        });
        
        profilesList.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getButton().equals(MouseButton.PRIMARY)){
                    if (mouseEvent.getClickCount() == 2){
                        selectButton.fire();
                    }
                }
            }
        });
        
        profilesList.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>
        (){
        	@Override
        	public void handle(KeyEvent keyEvent) {
        		if(keyEvent.getCode()==KeyCode.ENTER)
        		{
        			selectButton.fire();
        		}
        	}
        });

        refreshProfiles();
    }

    /**
     * Refreshes the list of profiles (say, if one has been added or removed)
     */
    private void refreshProfiles(){
        ArrayList<String> profileNames = ProfileManager.CurrentProfileManager.getProfiles();
        profiles.clear();

        for (String s : profileNames)
            profiles.add(s);
        
        if (profileNames.size() > 0) {
        	profilesList.getSelectionModel().select(0);
        }

        boolean validSelection = profilesList.getSelectionModel().getSelectedIndex() != -1;
        deleteButton.setDisable(!validSelection);
        selectButton.setDisable(!validSelection);
        
        // change indicator to show if we are on network server
        if (ProfileManager.CurrentProfileManager.getProfileLoader() instanceof NetworkProfileLoader)
        {
        	String host = ((NetworkProfileLoader)ProfileManager.CurrentProfileManager.getProfileLoader()).getHost();
        	host = "fitterizer on " + host;
        	fitterizerLocation.setText(host);
        }
        else
        {
        	fitterizerLocation.setText("fitterizer");
        }
    }
}
