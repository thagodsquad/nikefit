package gui;

import java.net.URL;
import java.net.URLConnection;
import java.util.Observable;

/**
 * Contains settings relevant to the GUI classes, such as whether or not
 * you have an Internet connection at any particular moment.
 */
public class InternetSettings extends Observable
{
	/**
	 * The current instance of the InternetSettings class
	 */
    private static InternetSettings internetSettings;

    /**
     * Gets the current instance of the InternetSettings class
     * @return current InternetSettings class
     */
    public static InternetSettings getInternetSettings()
    {
        return internetSettings;
    }

    /**
     * Current connection state.
     */
    private boolean isOffline;
    
    /**
     * Used to assist with thread shutdown.
     */
    private static boolean threadLoop;
    
    /**
     * Delay in seconds between each connection test.
     */
    private double pollingDelay;

    /**
     * Background thread to do checks on.
     */
    private Thread refreshThread;

    /**
     * Creates a new 'Internet Settings'
     * Note: This will set the static 'InternetSettings' property
     * @param pollingDelay The delay between checking if the internet is up/down, in seconds
     */
    public InternetSettings(double pollingDelay)
    {
        this.pollingDelay = pollingDelay;
        internetSettings = this;
        initialize();
    }

    /**
     * Initialise the Internet settings
     */
    private void initialize()
    {
    	// create and start the background testing thread
    	threadLoop = true;
        refreshThread = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {	// use loop variable to stop runaway memory leaks via stack frames
                	while (threadLoop)	
                	{
                		refresh();
    	                Thread.sleep((int)(pollingDelay*1000));
                	}
                }
                catch (InterruptedException e){}
            }
        });
        refreshThread.start();
    }

    /**
     * Stops the InternetSettings from checking for updates. If this isn't called the
     * program won't correctly stop
     * @return success
     */
    public static boolean stop()
    {	// check if the thread is actually running
        if (getInternetSettings() != null && getInternetSettings().refreshThread != null)
        {
        	threadLoop = false;
            getInternetSettings().refreshThread.interrupt();
            getInternetSettings().refreshThread = null;
            return true;
        }
        else
        {	// failure due to non existent thread
        	return false;
        }
    }

    /**
     * Starts the Internet settings checking for a connection
     * @return success
     */
    public static boolean start()
    {
    	try
    	{
    		new InternetSettings(5);	// instantiate new settings checker
    		return true;		// return success
    	}
        catch (Exception e)
    	{
        	System.err.println("Starting InternetSettings thread failed due to error:\n" + e.getMessage());
        	return false;		// return failure
    	}
    }

    /**
     * Indicates whether the application should operate in 'Offline Mode'
     * @return Offline mode? true --> Yes false --> No
     */
    public boolean getIsOffline()
    {
        return isOffline;
    }

    /**
     * Updates our current known state of connectivity.
     */
    private void refresh()
    {
        boolean newValue = false;
        /* Try and connect to google.
         * There does not appear to be a proper Java API for doing this
         * and as this is cross platform we cannot use the windows only
         * InternetGetConnectedState() from wininet.h */
        try
        {
            URLConnection connection = new URL("http://www.google.com/").openConnection();
            connection.setConnectTimeout(1000);
            connection.setUseCaches(false);

            @SuppressWarnings("unused")
			Object test = connection.getContent(); // Will fail with no data
        }
        catch (Exception e)
        {
            //If there's some sort of exception, we're offline
            newValue = true;
        }

        //If our new value is different, notify our observers of the new state
        if (isOffline == newValue) return;

        isOffline = newValue;
        setChanged();
        this.notifyObservers();
    }
}
