package gui.profileeditor;

import gui.helpers.dialoghelper.DialogHelper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import model.Activity;
import model.Gender;
import profiles.ProfileManager;

import java.net.URL;
import java.time.LocalDate;
import java.util.*;

/**
 * The controller for the profile editor. Manages user interaction and responses
 * to user interaction with the profile editor
 */
public class ProfileEditorController implements Initializable {
    public static final String FXMLString = "profileeditor/ProfileEditorView.fxml";

    @FXML
    private Button saveButton, cancelButton;

    @FXML
    private TextField nameBox, heightBox, weightBox;

    @FXML
    private DatePicker datePicker;
    
    @FXML
    private ChoiceBox<Gender> genderDropdown;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    	
    	// populate the gender drop-down
    	List<Gender> list = new ArrayList<Gender>(Arrays.asList(Gender.values()));
        ObservableList<Gender> obList = FXCollections.observableList(list);
    	genderDropdown.getItems().clear();
    	genderDropdown.setItems(obList);
    	
    	
        saveButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                save();
            }
        });
        
        saveButton.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>
        (){
        	@Override
        	public void handle(KeyEvent keyEvent) {
        		if(keyEvent.getCode()==KeyCode.ENTER)
        		{
        			saveButton.fire();
        		}
        	}
        });

        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                close();
            }
        });
        
        cancelButton.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>
        (){
        	@Override
        	public void handle(KeyEvent keyEvent) {
        		if(keyEvent.getCode()==KeyCode.ENTER)
        		{
        			cancelButton.fire();
        		}
        	}
        });
    }

    /**
     * Closes the stage
     */
    private void close(){
        Stage s = (Stage)cancelButton.getScene().getWindow();
        try {
            //FXApplicationStarter.start(ProfileManagerController.FXMLString, new Stage());
            s.close();
        }catch (Exception e){
            e.printStackTrace();
            System.err.println("This is really, really bad....");
        }
    }

    /**
     * Saves the user profile
     */
    private void save(){
        try
        {        	
        	LocalDate ld = datePicker.getValue();
        	Calendar c =  Calendar.getInstance();
        	c.set(ld.getYear(), ld.getMonthValue()-1, ld.getDayOfMonth());
        	Date birthDate = c.getTime();
        	
            if (!birthDate.before(new Date()))
            {	// birth date in the future selected
            	throw new Exception("Birth dates cannot be in the future.");
            }

            String name = nameBox.getText();
            if (name.equals("") || name == null)
            {
            	throw new Exception("Name cannot be blank.");	// invalid name
            }
            
            float height = Float.parseFloat(heightBox.getText());
            if (height <= 0.0f)
            {
            	throw new Exception("Unreasonable height provided.");	// invalid height
            }
            
            float weight = Float.parseFloat(weightBox.getText());
            if (weight <= 0.0f)
            {
            	throw new Exception("Unreasonable weight provided.");	// invalid weight
            }
            
            Gender gender = (Gender)genderDropdown.getSelectionModel().getSelectedItem();
            if (gender == null)
            {
            	throw new Exception("No gender provided.");	// invalid gender
            }

            ProfileManager.CurrentProfileManager.createProfile(name, birthDate, height, weight, gender, new ArrayList<Activity>());
            close();
        }
        catch (Exception e)
        {
        	String message = "Unable to save profile. Check all values are valid and that the profile does not already exist.";
        	if (e.getMessage() != null && e.getMessage() != "empty String")
        	{
        		message += "\nFurther Information:\n" + e.getMessage();		// from error
        	}
            		
            DialogHelper.showDialog(message, "Uh oh!", (Stage)saveButton.getScene().getWindow());
        }
    }
}
