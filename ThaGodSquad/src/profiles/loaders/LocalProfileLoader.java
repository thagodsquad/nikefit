package profiles.loaders;

import profiles.UserProfile;

import java.io.*;
import java.util.ArrayList;

/**
 * Manages loading profile from the local HD.
 * 
 * This is used in conjunction with the NetworkProfileServer
 * or the ProfileManager to provide the required methods for
 * loading or saving profiles. This allows the front end to
 * be independent of the backend loaders.
 */
public class LocalProfileLoader implements ProfileLoader {
	
	private String workingDirectory = System.getProperty("user.dir");
	
	/**
	 * Instantiates a new LocalProfileLoader.
	 * Blank constructor for default usage.
	 */
	public LocalProfileLoader(){}
	
	/**
	 * Instantiates a new LocalProfileLoader.
	 * @param directory Directory to use profiles in.
	 */
	public LocalProfileLoader(String directory)
	{
		this.workingDirectory = directory;
	}
	
    /**
     * Gets the storage directory of profiles.
     * @return The location the profiles are stored on the HD.
     */
    private String getDirectory()
    {
    	// return the current working directory
		return workingDirectory;
    }

    /**
     * Loads a user profile from the disk.
     * @param profileName The name of the profile to load
     * @return The loaded profile.
     */
    @Override
    public UserProfile loadProfile(String profileName)
    {
    	// load the profile using the default directory
    	return loadProfile(profileName, getDirectory());
    }

    /**
     * Loads a user profile from the disk.
     * @param profileName The name of the profile to load
     * @param directory The directory to load the profile from.
     * @return The loaded profile.
     */
    public UserProfile loadProfile(String profileName, String directory){
    	try
    	{
    		// Open the profile file
    		String profileFileLocation = directory + File.separator + profileName + ".profile";
    		// Create an object reading stream
    		ObjectInputStream in = new ObjectInputStream(new FileInputStream(profileFileLocation));
    		// Input and case to correct type
    		UserProfile userIn = (UserProfile)in.readObject();
    		// Close input stream
    		in.close();
    		return userIn;
    	}
    	catch (Exception e)
    	{
    		// What the hell happened?
    		System.err.println("An error occured while loading the profile:\n" + e.getMessage());
    		return null;
    	}
    }

    /**
     * Saves a user profile out to a file in the default directory.
     * @param profile Profile to save.
     * @throws Exception When a profile fails to save.
     */
    @Override
    public void saveProfile(UserProfile profile) throws Exception
    {
    	// saves the profile using the default directory
    	saveProfile(profile, getDirectory());
    }

    /**
     * Saves a user profile out to a file.
     * @param profile Profile to save.
     * @param directory Directory to save the profile in.
     */
    public void saveProfile(UserProfile profile, String directory) throws Exception
    {
    	try
    	{
    		// Open the profile file
    		String profileFileLocation = directory + File.separator + profile.getName() + ".profile";
    		// Open object stream to file
    		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(profileFileLocation));
    		// Write the object out to the file
    		out.writeObject(profile);
    		// close the stream
    		out.close();
    	}
    	catch (Exception e)
    	{
    		// What the hell happened?
    		System.err.println("An error occured while saving the profile:\n" + e.getMessage());
    		throw new Exception("Profile not loaded.", e);
    	}
    }

    /**
     * Gets a list of profiles that exist.
     * @return List of profiles.
     */
    @Override
    public ArrayList<String> getProfileList()
    {
    	return getProfileList(getDirectory());
    }

    /**
     * Returns a list of profiles that exist in a directory.
     * @param directory Directory to search
     * @return list of profiles
     */
    public static ArrayList<String> getProfileList(String directory)
    {
    	ArrayList<String> profileList = new ArrayList<String>();
    	File dir = new File(directory); // create handle to directory
    	for (File f : dir.listFiles())
    	{
    		String name = f.getName();
    		if (name.endsWith(".profile")) // check if it ends with the correct ext
    		{
    			// if it does add
    			profileList.add(name.substring(0, name.indexOf(".profile")));
    		}
    	}
    	return profileList;
    }

    /**
     * Deletes the specified profile
     * @param profileName The The name of the profile
     * @return Whether the operation was successful
     */
    @Override
    public boolean deleteProfile(String profileName)
    {
    	return deleteProfile(profileName, getDirectory());
    }

    /**
     * Deletes the specified profile from the directory
     * @param profileName The name of The name of the profile
     * @param directory The Directory to search
     * @return Whether the operation was successful
     */
    public boolean deleteProfile(String profileName, String directory)
    {
    	try
    	{
    		File profileFile = new File(directory + File.separator + profileName + ".profile");

    		if(profileFile.delete())
    		{
    			return true;
    		}
    		else
    		{
    			throw new Exception("File delete failed");
    		}
    	}
    	catch (Exception e)
    	{
    		System.err.println("Deleting profile failed with error:\n" + e.getMessage());
    		return false;
    	}
    }
}
