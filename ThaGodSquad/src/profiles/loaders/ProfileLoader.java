package profiles.loaders;
import profiles.UserProfile;

import java.util.ArrayList;

/**
 * Interface for loading profiles from different locations.
 * All classes that inherit from this provide an API for
 * saving and loading profiles.
 * 
 * This is used in conjunction with the NetworkProfileServer
 * or the ProfileManager to provide the required methods for
 * loading or saving profiles. This allows the front end to
 * be independent of the backend loaders.
 */
public interface ProfileLoader {
    /**
     * Loads a user profile from the disk.
     * @param profileName The name of the profile to load
     * @return The loaded profile.
     */
    public UserProfile loadProfile(String profileName);

    /**
     * Saves a user profile out to a file in the default directory.
     * @param profile Profile to save.
     * @throws Exception When a profile fails to save.
     */
    public void saveProfile(UserProfile profile) throws Exception;

    /**
     * Returns a list of profiles that exist 
     * @return list of profiles
     */
    public ArrayList<String> getProfileList();

    /**
     * Deletes the specified profile
     * @param profileName The The name of the profile
     * @return Whether the operation was successful
     */
    public boolean deleteProfile(String profileName);
}
