package profiles.loaders;
import profiles.UserProfile;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ProtocolException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * Handles sending to and receiving from a server.
 * Uses a telnet, smtp like protocol combined with
 * serialization. As a result objects to be sent
 * must be serialisable.
 * 
 * Various parts of this class are derived from
 * Beej's Guide to Network Programming
 * (http://beej.us/guide/bgnet/) and a
 * http://www.dreamincode.net/ post (object sending),
 *
 */
public class NetworkProfileLoader implements ProfileLoader
{
	/**
	 * Stream to use to receive data from the server.
	 */
	private ObjectInputStream inputStream;
	
	/**
	 * Stream to use to send data to the server.
	 */
	private ObjectOutputStream outputStream;
	
	/**
	 * Handle to ServerListener that listens for incoming objects from the server.
	 */
	private ServerListener serverListener;
	
	/**
	 * TCP socket used to transmit/receive data.
	 */
	private Socket socket;
	
	/**
	 * Host/IP to connect to.
	 */
	private String server;
	
	/**
	 * Port to use to connect.
	 */
	private int port;
	
	/**
	 * Amount of time to wait for data before
	 * aborting/checking the queue for data (race condition).
	 */
	private static final long timeout = 5000;
	
	/**
	 * Network protocol version. All connections must use 
	 * this protocol version or lower to be supported.
	 */
	private static final int protoVer = 1;
	
	/**
	 * Instantiates a new NetworkProfileLoader
	 * @param server Host/IP to connect to.
	 * @param port Port to connect to.
	 */
	public NetworkProfileLoader(String server, int port)
	{
		this.server = server;
		this.port = port;
	}
	
	/**
	 * Connects to the server, and verifies the protocol version.
	 * @return Success
	 */
	private boolean connect()
	{
		try
		{
			// open a tcp socket connection to the server
			socket = new Socket(server, port);
		}
		catch(Exception ec)
		{
			System.err.println("An error occured connectiong to server:" + ec);
			return false;	// early failure. socket is probably in use by another application
		}
		
		System.out.println("Connection Established\n"
				+ "\tHost: " + socket.getInetAddress()
				+ "\n\tPort: " + socket.getPort());
	
		try
		{
			// get r/w streams
			inputStream  = new ObjectInputStream(socket.getInputStream());
			outputStream = new ObjectOutputStream(socket.getOutputStream());
		}
		catch (IOException e)
		{	// we cannot get a stream. this breaks the entire point of the server
			// so we may as well abort.
			System.err.println("An exception occured while getting IO streams: " + e);
			disconnect();
			return false;
		}

		serverListener = new ServerListener(this);
		serverListener.start();	// start an async listener for incoming server messages

		try
		{
			send("helo");	// server helo (stolen from smtp)
			String recv = (String)receive();
			if (!recv.startsWith("ack seng202"))
			{	// check correct protocol ack
				throw new Exception("Unknown server. Please connect to one for this application only.");
			}
			
			String[] spl = recv.split(" ");
			int ver;
			try			// get protocol version
			{
				ver = Integer.parseInt(spl[2]);
				if (ver > protoVer)
				{	// if a future protocol, abort
					throw new ProtocolException("Client cannot read future protocol version. Please update program.");
				}
			}
			catch (NumberFormatException e)
			{	// something else is going on...
				throw new Exception("Unknown server. Please connect to one for this application only.");
			}
			return true;
		}
		catch (Exception e)
		{
			System.err.println(e);
			disconnect();	// make sure we disconnect from the server,
			return false;	// before aborting with a false return
		}
	}
	
	/**
	 * Sends an object to the server.
	 * @param obj Object to send.
	 * @throws Exception If an exception occurred while sending the object.
	 */
	private void send(Object obj) throws Exception
	{
		outputStream.writeObject(obj);
	}
	
	/**
	 * Waits to receive the next message from the server. Will wait for
	 * a maximum of timeout milliseconds. If timeout occurs, it will
	 * return the top item of the received queue (this is just in case a
	 * race condition has occurred).
	 * @return Next object received.
	 * @throws Exception If no data is received/queued or thread was interrupted
	 * while waiting to receive data.
	 */
	private Object receive() throws Exception
	{
		synchronized (this)
		{	// wait max of timeout for data received notification
	    	this.wait(timeout);
		}
    	// get the top item of the queue
		Object obj = serverListener.dequeue();
		if (obj == null)
		{	// check item isnt null
			throw new Exception("No data received.");
		}
		return obj;
	}

	/**
	 * Disconnects from the server and closes streams.
	 */
	private void disconnect()
	{
		try
		{
			send("exit");	// tell server to disconnect
			if(inputStream != null) inputStream.close();	// close IO streams
			if(outputStream != null) outputStream.close();
			if(socket != null) socket.close();	// close socket
		}
		catch(Exception e)
		{
			// at this point the networking is broken so there is nothing
			// that can be done about it. better to ignore and deal with
			// the exceptions later
		}
	}

	/**
	 * Listens for incoming objects on the socket.
	 * This is done on another thread so as to be unblocking
	 * except when receive() is called on the main thread.
	 * @author Matthew Knox
	 */
	private class ServerListener extends Thread
	{
		/**
		 * NetworkProfileLoader client that is managing the connection.
		 */
		private NetworkProfileLoader client;
		
		/**
		 * Queue to store received items before processing.
		 */
		private Queue<Object> itemsReceived = new LinkedList<Object>();
		
		/**
		 * Returns the top item of the received items queue. 
		 * @return the object on the top of the queue, or null if there is none.
		 */
		public Object dequeue()
		{
			return itemsReceived.poll();
		}
		
		/**
		 * Instantiates a new server listener.
		 * @param client NetworkProfileLoader client that is managing the connection. 
		 */
		public ServerListener(NetworkProfileLoader client)
		{
			this.client = client;
		}
		
		/**
		 * Main listening loop. Awaits new incoming objects, adds them to the received
		 * object queue and notifies the NetworkProfileLoader that an object has been
		 * received from the server.
		 * Never call this directly. Instead use the start() method of this object so
		 * it will be run in another thread.
		 */
		public void run()
		{
			while(true)
			{
				try
				{
					Object recvObj = inputStream.readObject();	// get the object
					itemsReceived.add(recvObj);	// add to the received items queue
					synchronized (client)
					{
						client.notify();	// notify the NetworkProfileLoader that an
					}						// object has been received
				}
				catch(IOException e)
				{
					System.out.println("Server Closed Connection.");
					break;					// IO exception *should* only occur on a server disconnect
				}
				catch(ClassNotFoundException e)
				{	// what has been received is not an object (probably an atomic or wrong protocol)
					System.err.println("An unknown object has been received which the program cannot handle."
							+ e + "\nThe client must disconnect.");
					disconnect();	// this cannot be handled nicely, so just disconnect and break.
					break;			// a reconnect may solve the problem (which a disconnect will cause)
				}
			}
		}
	}

	@Override
	public UserProfile loadProfile(String profileName) {
		if (socket == null || !socket.isConnected() || socket.isClosed())
		{
			if (!connect())		// if we arn't connected, attempt to connect
			{
				return null;
			}
		}
		
		try
		{
			send("load " + profileName);	// send protocol command
			Object recv = receive();		// get the object
			disconnect();
			
			if (!(recv instanceof UserProfile)) // check object is a UserProfile
			{
				return null;
			}
			
			UserProfile profile = (UserProfile)recv;	// cast and return
			return profile;
		}
		catch (Exception e)
		{
			System.err.println("Critical error occured while getting profile list:\n" + e);
			return null; // on error, return null as profile cannot be retrieved
		}
	}

	@Override
	public void saveProfile(UserProfile profile) throws Exception 
	{
		if (socket == null || !socket.isConnected() || socket.isClosed())
		{
			if (!connect())		// if we arn't connected, attempt to connect
			{
				throw new Exception("Connecting to server failed. See log for details.");
			}
		}
		
		send("save");				// send protocol command
		send(profile);				// send profile object
		Object recv = receive();	// receive object
		disconnect();
		
		if (recv instanceof Exception)	// check that the received object is an exception
		{
			if (recv != null)
			{
				throw (Exception)recv;	// if it isn't a null exception, throw it
			}
		}
	}

	@Override
	public ArrayList<String> getProfileList() 
	{
		if (socket == null || !socket.isConnected() || socket.isClosed())
		{
			if (!connect())		// if we arn't connected, attempt to connect
			{
				return null;
			}
		}
		
		try
		{
			send("list");				// send protocol command
			Object recv = receive();	// get object
			disconnect();				// disconnect
			if (!(recv instanceof ArrayList<?>))	// check if it is an arraylist
			{
				return null;
			}
			
			// required because it is impossible to remove warning due to transmission
			// over a network (it thinks it is an object, has no idea otherwise cause
			// it is a generic)
			@SuppressWarnings("unchecked")
			ArrayList<String> profileList = (ArrayList<String>)recv;
			return profileList;
		}
		catch (Exception e)
		{
			System.err.println("Critical error occured while getting profile list:\n" + e);
			return null;	// return null on error, because a list cannot be retrieved
		}
	}

	@Override
	public boolean deleteProfile(String profileName) 
	{
		if (socket == null || !socket.isConnected() || socket.isClosed())
		{
			if (!connect())		// if we arn't connected, attempt to connect
			{
				return false;
			}
		}
		
		try
		{
			send("delete " + profileName);	// send protocol command
			Object obj = receive();			// get object
			disconnect();					// disconnect
			if (!(obj instanceof Boolean))	// check that we have received a bool
			{
				return false;	// something weird is going on...
			}
			return (boolean)obj;
		}
		catch (Exception e)
		{
			System.err.println("Critical error occured while deleting profile:\n" + e);
			return false;	// if an error occurred, deleting failed so send false
		}
	}
	
	/**
	 * Gets the IP/Host currently connected to.
	 * @return IP/Host connected to.
	 */
	public String getHost()
	{
		return server;
	}
	
	/**
	 * Main class for testing.
	 */
	public static void main(String[] args)
	{
		int portNumber = 7104;				// port
		String serverAddress = "127.0.0.1";	// host
		
		NetworkProfileLoader client = new NetworkProfileLoader(serverAddress, portNumber);
		if(!client.connect())
		{
			return;		// open connection. if fail, abort
		}
		
		Scanner scan = new Scanner(System.in);	// open scanner to stdin
		try
		{
			while(true) {
				System.out.print("> ");
				String msg = scan.nextLine();	// get next command from stdin
				
				client.send(msg);	// send command
				
				if (!msg.equalsIgnoreCase("exit"))	// if we arn't disconnecting
				{
					msg = (String)client.receive();
					System.out.println("# "+msg);	// wait for and print the reply
				}
			}
		}
		catch (Exception e)
		{	// if the exception hasn't occurred cause we disconnected
			if (!e.getMessage().contains("No data received"))
			{	// print the error
				System.err.println("An error occured: " + e);
			}
		}
		scan.close();			// close stdin scanner
		client.disconnect();	// disconnect from server
	}
}

