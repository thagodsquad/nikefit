package profiles.loaders;

import java.util.ArrayList;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.*;
import profiles.UserProfile;
import profiles.database.DatabaseManager;

/**
 * Provides an API for saving and loading a UserProfile from
 * a database. This API is largely database independent,
 * which allows the database type to be swapped out at will.
 * 
 * However in this classes current form any replacement
 * database must support the unofficial SQL extension "blob"
 * (most do).
 * 
 * This is used in conjunction with the NetworkProfileServer
 * or the ProfileManager to provide the required methods for
 * loading or saving profiles. This allows the front end to
 * be independent of the backend loaders.
 */
public class DatabaseProfileLoader implements ProfileLoader
{
	/**
	 * The manager of the database connection.
	 */
	private DatabaseManager databaseManager;
	
	/**
	 * Creates a new database profile loader.
	 * @param url The jdbc url of the database.
	 */
	public DatabaseProfileLoader(String url)
	{
		databaseManager = new DatabaseManager(url);	// create a database manager.
	}
	
	/**
	 * Creates the database for storing user profiles.
	 */
	private void createDatabase()
	{
		try
		{
			if (!databaseManager.connect(4))	// try connecting to database
			{
				throw new Exception("Could not connect to database.");
			}
			
	        try
	        {
	        	// prepare and send sql statements
	        	String sqlStatement = "create table UserProfiles (Name varchar(255),ProfileObject blob);";
	        	databaseManager.sendStatement(sqlStatement);
	        }
	        catch (SQLException e)
	        {
	        	System.err.println("Could not create sql table.");	// something went wrong creating table
	        	throw new Exception("Could not create sql table.");	// throw more useful error message
	        }
	        
	        if (!databaseManager.disconnect(4))	// try disconnecting
			{									// nothing user can do if fails so just print debug message
	        	System.err.println("Could not disconnect from to database.");
			}
		}
		catch (Exception e)	// return fail and print error
		{
			System.err.println("Something went wrong...:\n" + e);
		}
	}
	
	@Override
	public UserProfile loadProfile(String profileName)
	{
		if (!databaseManager.connect(0))	// connection failed.
		{
			System.err.println("Could not connect to database.");
			return null;
		}

		UserProfile profile = null;
        try
        {
        	// create and send SQL
        	String sql="select ProfileObject from UserProfiles where Name='" + profileName + "'";
        	ResultSet resultSet = databaseManager.sendStatementWithResponse(sql);
        	
        	resultSet.next();	// move to the first row of the returned table
        	
        	byte[] bytes = resultSet.getBytes("ProfileObject");
        	ByteArrayInputStream byteInputStream = new ByteArrayInputStream(bytes);
        	ObjectInputStream ins = new ObjectInputStream(byteInputStream);	// read the object
            profile =(UserProfile)ins.readObject();
            ins.close();
        }
        catch (Exception e)
        {
        	if (e instanceof java.sql.SQLException && e.getMessage().contains("no such table"))
        	{	// database doesnt exist, create it
        		System.err.println("Database does not yet exist, creating...");
        		createDatabase();
        	}
        	else
        	{	// no need to error out because will return error by default
        		System.err.println("Something went wrong reading database:\n" + e); // we failed to load. print error
        	}
        }
        
        if (!databaseManager.disconnect(0))	// try disconnecting
		{									// nothing user can do if fails so just print debug message
        	System.err.println("Could not disconnect from to database.");
		}

        return profile;
	}

	@Override
	public void saveProfile(UserProfile profile) throws Exception
	{
		try
		{
			if (!databaseManager.connect(1))	// try connecting to the database
			{
				throw new Exception("Could not connect to database.");
			}

			ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayStream);
			objectOutputStream.writeObject(profile);
			objectOutputStream.flush();					// create object for saving in database
			objectOutputStream.close();
			byteArrayStream.close();

			deleteProfile(profile.getName());	// try delete profile if it exists
			
			String sqlStatement = "insert into UserProfiles (Name, ProfileObject) values(?, ?)";
			Object[] obj = {profile.getName(), byteArrayStream.toByteArray()};	// prepare and send sql
			databaseManager.sendStatement(sqlStatement, obj);
			
			if (!databaseManager.disconnect(1))	// try disconnecting.
			{									// nothing user can do on failure so just print debug message
				System.err.println("Could not disconnect from to database.");
			}
		}
		catch (java.sql.SQLException e)
		{
			if (e instanceof java.sql.SQLException && e.getMessage().contains("no such table"))
        	{	// database doesnt exist, create it
        		System.err.println("Database does not yet exist, creating...");
        		createDatabase();		// create db
        		saveProfile(profile);	// retry saving
        	}
			else
			{
				throw e;	// otherwise is a different kind of exception
			}
		}
	}

	@Override
	public ArrayList<String> getProfileList()
	{
		if (!databaseManager.connect(2))	// try connecting to database
		{
			System.err.println("Could not connect to database.");
			return null;
		}

		ArrayList<String> profileList = new ArrayList<String>();
        try
        {
        	String sql = "select Name from UserProfiles";	// prepare and send sql
        	ResultSet resultSet = databaseManager.sendStatementWithResponse(sql);
        	
            while (resultSet.next())	// for each returned row in DB
            {
            	String str = resultSet.getString("Name");
                profileList.add(str);	// get profile name and add to array
            }
        }
        catch (Exception e)
        {
        	if (e instanceof java.sql.SQLException && e.getMessage().contains("no such table"))
        	{	// database doesnt exist, create it
        		System.err.println("Database does not yet exist, creating...");
        		createDatabase();
        	}
        	else
        	{	// will return no profiles by default, so just print error
        		System.err.println("Something went wrong reading database:\n" + e);
        	}
        }
        
        if (!databaseManager.disconnect(2))	// disconnect from database
		{									// ignore as nothing the user can do about it
        	System.err.println("Could not disconnect from to database.");
		}

        return profileList;
	}

	@Override
	public boolean deleteProfile(String profileName)
	{
		try
		{
			if (!databaseManager.connect(3))	// try connecting to database
			{
				throw new Exception("Could not connect to database.");
			}
	        
			boolean success = true;
	        try
	        {
	        	// prepare and send sql statements
	        	String sqlStatement = "delete from UserProfiles where Name='" + profileName + "'";
	        	databaseManager.sendStatement(sqlStatement);
	        	
	        	databaseManager.sendStatement(sqlStatement);
	        	ArrayList<String> profiles = getProfileList();
	        	if (!profiles.equals(null))							// check if the database contains
	        	{													// the added profile
		        	success = profiles.contains(profileName);
	        	}
	        }
	        catch (SQLException e)
	        {
	        	System.err.println("Could not delete profile.");	// something went wrong deleting
	        	throw new Exception("Could not delete profile.");	// throw more useful error message
	        }
	        
	        if (!databaseManager.disconnect(3))	// try disconnecting
			{
	        	System.err.println("Could not disconnect from to database.");
			}
	        return success;	// return success value
		}
		catch (Exception e)	// return fail and print error
		{
			if (e instanceof java.sql.SQLException && e.getMessage().contains("no such table"))
        	{	// database doesn't exist, create it
				System.err.println("Cannot delete from a non existant database. Creating...");
        		createDatabase();
        	}
			else
			{
				System.err.println("Something went wrong...:\n" + e);
			}
			return false;	// return false to indicate error
		}
	}
}
