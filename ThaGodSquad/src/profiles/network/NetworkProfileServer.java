package profiles.network;
import profiles.UserProfile;
import profiles.loaders.DatabaseProfileLoader;
import profiles.loaders.LocalProfileLoader;
import profiles.loaders.ProfileLoader;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Class to server network profiles to network client(s).
 * 
 * Handles all network interactions and is the only server that
 * the program ever has to connect to. The server is loader independent
 * so allows any backend (eg could use a database to store profiles or
 * could use the local HD to store profiles).
 */
public class NetworkProfileServer implements Runnable
{
	/**
	 * Protocol version this server implements.
	 */
	private static final int protoVer = 1;
	/**
	 * Format to print date-time strings as.
	 */
	private static final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");		
	/**
	 * ArrayList to store all the connected clients.
	 */
	private ArrayList<ConnectedClientThread> clients = new ArrayList<ConnectedClientThread>();
	/**
	 * Port to listen on.
	 */
	private int port;
	/**
	 * Control variable used to assist with server termination.
	 */
	private boolean serverIsRuning;
	/**
	 * Loader used to manipulate the actual User Profiles.
	 */
	private ProfileLoader loader;
	/**
	 * Server used to discover available servers.
	 */
	private NetworkDiscoveryServer server;
	
	/**
	 * Instantiates a new profile server.
	 * @param port Port to listen on.
	 * @param loader Loader to use to manipulate objects.
	 */
	public NetworkProfileServer(int port, ProfileLoader loader)
	{
		this.port = port;
		this.loader = loader;
	}
	
	/**
	 * Checks if the server is running in the background.
	 * @return true or false.
	 */
	public boolean serverIsRunning()
	{
		return serverIsRuning;
	}
	
	@Override
	public void run()
	{
		start();	// calls start in a separate thread (see Runnable interface)		
	}
	
	/**
	 * Starts the server listening for incoming clients.
	 * This method is blocking so start class in new thread to avoid.
	 */
	public void start()
	{
		server = new NetworkDiscoveryServer(9493, "seng202", port);
		server.start();		// start auto-discovery server
		
		serverIsRuning = true;
		try 
		{	// init server listening
			ServerSocket serverSocket = new ServerSocket(port);
			
			System.out.println("Server started at "
					+ dateFormat.format(new Date()) + ".\n"
					+ "\tIP/Hostname:\t" + InetAddress.getLocalHost().getHostAddress()
					+ "\n\tPort:\t\t" + serverSocket.getLocalPort());
			System.out.println("Waiting for clients...");
			
			while(serverIsRuning)
			{		
				Socket socket = serverSocket.accept();	// get socket of incoming client
				if(!serverIsRuning)
				{
					break;		// if server has terminated, break
				}
				ConnectedClientThread clientThread = new ConnectedClientThread(socket);
				clients.add(clientThread);		// create a new client thread and start
				clientThread.start();
			}
			
			System.out.println("Shutting down server...");
			try
			{
				serverSocket.close();	// stop listening for incoming clients
				for(int i = 0; i < clients.size(); ++i)
				{
					ConnectedClientThread client = clients.get(i);
					client.close();	// disconnect each connected client
				}
			}
			catch(Exception e)
			{	// something went wrong, given we are shutting down anyway, just print error
				// showing an error to user would be counter productive
				System.err.println("A critical error occured while closing the server and connected clients: " + e);
			}
			System.out.println("Server Terminated.");
		}
		catch (IOException e)
		{	// something went drastically wrong. no way this can be handled nicely.
			// so just print error and terminate
			System.err.println("A critical error occured while running the server.\n"
					+ e
					+ "\nThe server will now abort.");
		}
	}		

	/**
	 * Stops the server.
	 */
	public void stop()
	{
		serverIsRuning = false;	// set control variable.
		try
		{
			server.abort(); // shutdown auto discovery server
			// connect to server (which immediately checks control var and terminates)
			Socket sock = new Socket("127.0.0.1", port);
			sock.close();
		}
		catch(Exception e)
		{	// nothing that can be done to handle this exception
		}	// we are terminating anyway, so ignore
	}

	/**
	 * Removes a client from the clients array after disconnect.
	 * @param cct Client to disconnect.
	 */
	private synchronized void remove(ConnectedClientThread cct)
	{
		clients.remove(cct);
	}
	
	/**
	 * Special thread class for handling each client. 
	 * @author Matthew Knox
	 */
	private class ConnectedClientThread extends Thread
	{
		/**
		 * Socket used to communicate with the client.
		 */
		private Socket socket;
		/**
		 * Input stream to get data from the client.
		 */
		private ObjectInputStream inputStream;
		/**
		 * Output stream to send data to the client.
		 */
		private ObjectOutputStream outputStream;

		/**
		 * Creates a new class to handle a connected client.
		 * @param socket Socket used to communicate with client.
		 */
		public ConnectedClientThread(Socket socket)
		{
			this.socket = socket;
			try
			{	// get IO streams
				outputStream = new ObjectOutputStream(socket.getOutputStream());
				inputStream  = new ObjectInputStream(socket.getInputStream());
				System.out.println("Client " + socket.getInetAddress() + " connected.\n");
			}
			catch (IOException e)
			{	/* 	this error will automatically solve itself gracefully
			 		if ignored. this is because when the code uses one of
					the streams, it will throw an exception and disconnect
					from the client. */
				System.err.println("Error creating IO streams: " + e);
				return;
			}
		}

		/**
		 * Client send/receive loop. Handles all incoming and outgoing
		 * data to/from the client to the server.
		 * 
		 * Designed to be called by Thread.start(). If called
		 * otherwise, behaviour is undefined.
		 */
		public void run()
		{
			boolean runClient = true;
			while(runClient)
			{
				String command, args = "";
				try
				{
					command = (String)inputStream.readObject();	// receive protocol command
					
					if(command.contains(" "))	// if there is a space, command has args
					{
						args = command.substring(command.indexOf(' ')+1);	// get args
						command = command.substring(0, command.indexOf(' '));
					}
				}
				catch (EOFException e)
				{
					command = "<none>"; // unknown command, will disconnect client (safely)
				}
				catch (IOException | ClassNotFoundException e)
				{
					System.err.println("Cannot read command from client due to error: " + e);
					System.err.println("This error is irrecoverable. Disconnecting client...");
					runClient = false;	// error reading commands. will disconnect client (immediately)
					break;				
				}

				System.out.println("Command Received at " + dateFormat.format(new Date())
						+ "\n\tSource:\t\t" + socket.getInetAddress()
						+ "\n\tCommand:\t" + command);

				switch(command)		// handle command
				{
					case "helo":	// client helo (from smtp, not to be mistaken with hello)
					{
						String response = "ack seng202 " + protoVer;	// send ack and protocol version
						System.out.println("\tResponse:\t" + response + "\n");
						boolean success = write(response); // send response
						if (!success)
						{	// error occurred sending. disconnect.
							System.err.println("Sending response failed. Disconnecting client...");
							runClient = false;
						}
						break;
					}
					case "load":	// sends a user profile
					{
						System.out.println("\tResponse:\t<user_profile(\"" + args + "\")>\n");
						UserProfile userProfile = loader.loadProfile(args);	// get the user profile
						boolean success = write(userProfile);	// send
						if (!success)
						{	// error occurred sending. disconnect.
							System.err.println("Sending response failed. Disconnecting client...");
							runClient = false;
						}
						break;
					}
					case "save":	// saves a user profile
					{
						System.out.println("\tResponse:\t<none>\n");
						Object obj;
						
						try
						{
							obj = inputStream.readObject();	// gets the user profile to save
						}
						catch (IOException | ClassNotFoundException e)
						{
							System.err.println("Cannot read UserProfile from client due to error: " + e);
							System.err.println("This error is irrecoverable. Disconnecting client...");
							runClient = false;	// error occurred receiving. disconnect.
							break;				
						}
						
						if (!(obj instanceof UserProfile))	// check type
						{
							System.err.println("Invalid data received.");
							break;
						}
						
						boolean success;
						Object ex = "<none>";
						try
						{	// try saving
							loader.saveProfile((UserProfile)obj);
						}
						catch(Exception e)
						{	// if there is an exception catch
							ex = (Object) e;
						}
						success = write(ex);	// send exception to to client
						if (!success)
						{
							System.err.println("Sending response failed. Disconnecting client...");
							runClient = false;	// something went wrong... disconnect.
						}
						break;
					}
					case "list":	// sends a list of profiles to the client
					{
						System.out.println("\tResponse:\t<profile_list>\n");
						ArrayList<String> profileList = loader.getProfileList();	// get the profile list
						boolean success = write(profileList);	// send to the client
						if (!success)
						{	// error occurred sending. disconnect.
							System.err.println("Sending response failed. Disconnecting client...");
							runClient = false;
						}
						break;
					}
					case "delete":	// deletes a user profile
					{
						boolean result = loader.deleteProfile(args);	// try delete profile
						System.out.println("\tResponse:\t" + result + "\n");
						boolean success = write(result);	// send success value
						if (!success)
						{	// error occurred sending. disconnect.
							System.err.println("Sending response failed. Disconnecting client...");
							runClient = false;
						}
						break;
					}
					case "echo":	// manual testing command. replies with the args of what was sent
					{
						System.out.println("\tResponse:\t" + args + "\n");
						boolean success = write(args);	// send args back to client
						if (!success)
						{	// error occurred sending. disconnect.
							System.err.println("Sending response failed. Disconnecting client...");
							runClient = false;
						}
						break;
					}
					case "exit":	// disconnect command
					{
						System.out.println("\tResponse:\t<none>\n");
						System.out.println("Client " + socket.getInetAddress() + " disconnected.\n");
						runClient = false;	// disconnect as requested...
						break;
					}
					default:	// unknown command received
					{
						System.out.println("\tResponse:\t<unknown>\n");
						boolean success = write("Unknown Command \"" + command + "\"");	// reply with unknown cmd
						if (!success)
						{	// error occurred sending. disconnect.
							System.err.println("Sending response failed. Disconnecting client...");
							runClient = false;
						}
						break;
					}
				}
			}

			remove(this);	// remove this client from the clients array
			close();		// close the connection to this client
		}
		
		/**
		 * Closes the connection to this client.
		 */
		private void close()
		{
			try
			{
				if(outputStream != null) outputStream.close();
				if(inputStream != null) inputStream.close();	// close IO streams
				if(socket != null) socket.close();	// close socket
			}	// not much we can do on exception, besides which we are disconnecting so ignore
			catch (Exception e){}
		}

		/**
		 * Sends an object to the client.
		 * @param msg Object/message to send.
		 * @return Success
		 */
		private boolean write(Object msg)
		{
			if(!socket.isConnected())
			{
				close();	// if we arn't connected, don't bother cause it will fail
				return false;
			}
			
			try
			{	// write object to stream
				outputStream.writeObject(msg);
				return true;
			}
			catch(IOException e)
			{	// we can't send to the client. probably disconnected or irrecoverable error
				System.err.println("Error sending message to " + socket.getInetAddress());
				System.err.println(e.toString());
				return false;
			}
		}
	}
	
	/**
	 * Stand alone console server.
	 * @args Command line arguments. This only has one argument: --db/-d [sqlServer] [user] [password].
	 * @throws Exception on unhandled error
	 */
	public static void main(String[] args) throws Exception
	{
		ProfileLoader loader = null;
		
		for (int i = 0; i < args.length; i++)	// parse command line arguments
		{
			if (args[i].equals("--db") || args[i].equals("-d"))	// check if we want a database instead
			{
				loader = new DatabaseProfileLoader(args[i+1]);	// setup database
				i += 1;
			}
			else
			{
				System.err.println("Unknown argument \"" + args[i] + "\".");
			}
		}
		
		if (loader == null)
		{
			loader = new LocalProfileLoader();	// create a local loader object
		}
		
		int port = 7104;	// port to listen on
		NetworkProfileServer server = new NetworkProfileServer(port, loader);	// create server
		
		try
		{
			Thread thread = new Thread(server);
			thread.start();		// start the server in a new thread
			System.out.println("Press any key to shutdown server.\n"+
					"NOTE: Make sure clients are disconnected to ensure data integrety."
					+ "\n---");
			System.in.read();	// wait for input (to stop server)
			server.stop();		// stop server
		}
		catch (Exception e)
		{	// print the critical error (so we don't crash with a stack trace)
			System.err.println("A critical error occured while running the server:\n"
					+ e.getMessage() + "\n---\nThe server will now terminate.");
		}
	}
}


