package profiles.network;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * Discovers and informs clients about servers
 * of the same type on the network.
 * 
 * Please note: DOES NOT WORK on enterprise networks where
 * UDP broadcasts are blocked by the routing firewalls.
 * However this class is known to work on LAN's.
 */
public class NetworkDiscoveryServer extends Thread
{
	/**
	 * Protocol version to expect.
	 */
	private static final double protoVer = 1.0;
	/**
	 * Post to listen and send announce packets on.
	 */
	private int listenPort;
	/**
	 * Port that the service is running on (only if announcing).
	 */
	private int servicePort;
	/**
	 * Service to search for/announce.
	 */
	private String serviceName;
	/**
	 * Servers located on the network.
	 */
	private ArrayList<AnnouncedService> announcedServices;
	/**
	 * Socket to use to send data.
	 */
	private DatagramSocket socket;
	/**
	 * By setting this to false the server loop will stop and shutdown the server.
	 */
	private boolean runServer = true;
	
	/**
	 * Gets all the known servers on the network
	 * for the given service type.
	 * @return Known services.
	 */
	public ArrayList<AnnouncedService> getAnnouncedServices()
	{
		return announcedServices;
	}
	
	/**
	 * Instantiates a new NetworkDiscoveryServer that will announce a service.
	 * @param announcePort Port to announce service on. If this is -1, will listen instead.
	 * @param serviceName Service to announce.
	 * @param servicePort Port that the service we are announcing is on.
	 */
	public NetworkDiscoveryServer(int announcePort, String serviceName, int servicePort)
	{
		announcedServices = new ArrayList<AnnouncedService>();
		this.listenPort = announcePort;
		this.servicePort = servicePort;
		this.serviceName = serviceName;
	}
	
	/**
	 * Instantiates a new NetworkDiscoveryServer that will listen for services.
	 * @param listenPort Port to listen on for services.
	 * @param serviceName Service to search for.
	 */
	public NetworkDiscoveryServer(int listenPort, String serviceName)
	{
		this(listenPort, serviceName, -1);
	}
	
	@Override
	public void run()
	{
		try
		{
			runServer = true;
			socket = new DatagramSocket(null);		// create socket
			socket.setReuseAddress(true);			// allow socket reuse
			socket.bind(new InetSocketAddress(InetAddress.getByName("0.0.0.0"), listenPort));
			socket.setBroadcast(true);				// bind and allow broadcasting

			byte[] sendData;
			DatagramPacket sendPacket;
			
			if (servicePort != -1)		// if we are announcing
			{
				sendData = buildAnnouncePacket().getBytes();
				sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.getByName(getBroadcastAddress()), listenPort);
				socket.send(sendPacket);	// announce presence to network
			}
			else						// if we are listening
			{
				sendData = "request announce".getBytes();
				sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.getByName(getBroadcastAddress()), listenPort);
				socket.send(sendPacket);	// ask servers to announce presence
			}
			
			while (runServer)	// until shutdown
			{			
				byte[] recvBuf = new byte[15000];
				DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
				socket.receive(packet);		// receive packet
				String packetData = new String(packet.getData()).trim();
				
				if (packetData.startsWith("request announce") && servicePort != -1)
				{	// if we are announcing and we receive an announce request
					sendData = buildAnnouncePacket().getBytes();
					sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.getByName(getBroadcastAddress()), listenPort);
					socket.send(sendPacket);	// send an announce packet
				}
				else if (packetData.startsWith("announce") || packetData.startsWith("unannounce"))
				{	// otherwise we must be listening, if they are announce or unannounce, parse them
					parsePacket(packetData);
				}
			}
		}
		catch (IOException ex)
		{	// socket closed sometimes happens on shutdown if it attempts to listen before termination
			if (!ex.getMessage().equals("socket closed"))
			{	// not much can be done at this point
				System.err.println("Something went drastically wrong: " + ex);
				// No message to user because it would be counter productive to do so
			}
	    }
	}
	
	/**
	 * Shuts down the server.
	 * @throws Exception If an error occurs during this process.
	 */
	public void abort() throws Exception
	{
		runServer = false;
		if (servicePort != -1)
		{
			byte[] sendData = buildUnannouncePacket().getBytes();
			DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.getByName(getBroadcastAddress()), listenPort);
			socket.send(sendPacket);	// send an unannounce packet
		}
		socket.close();			// close the socket
	}
	
	/**
	 * Parses and deals with announce and unannounce packets.
	 * @param data The packet data.
	 */
	private void parsePacket(String data)
	{
		try
		{
			String[] spl = data.split("\r\n");	// split on each line

			String temp = spl[0].substring(spl[0].indexOf(' ')+1);
			String[] spl2 = temp.split("/");	// get service name and protocol version
			
			String host = spl[1].substring(spl[1].indexOf(':')+2); // get host name
			int port = Integer.parseInt(spl[2].substring(spl[2].indexOf(':')+2)); // get port
			
			if (Double.parseDouble(spl2[1]) > protoVer) // check protocol version
			{
				throw new Exception("Future protocol versions unsupported.");
			}
			
			if (!spl2[0].equals(serviceName)) // check service name
			{
				throw new Exception("Wrong service for this receiver.");
			}
			
			// create an announced service for the received data
			AnnouncedService service = new AnnouncedService(host, port);
			if (data.startsWith("announce") && !announcedServices.contains(service))
			{	// add the service to known services
				boolean result = announcedServices.add(service);
				if (!result)	// something went wrong
				{
					throw new Exception("Cannot add service.");
				}
			}
			if (data.startsWith("unannounce"))
			{	// remove the service
				boolean result = announcedServices.remove(service);
				if (!result)	// we didn't know the service existed!
				{
					System.err.println("Attempted to remove service that we did not know existed.");
				}
			}
		}
		catch (Exception e)
		{	// to handle an invalid packet, its best just to ignore it in this case
			System.err.println("Error parsing and dealing with packet: " + e);
		}
	}
	
	/**
	 * Builds a packet that will unannounce the current service from the network.
	 * @return packet data
	 */
	private String buildUnannouncePacket()
	{
		String packet = buildAnnouncePacket();
		packet = "un" + packet;	// same as announce packet with "un" at the front
		return packet;
	}
	
	/**
	 * Builds a packet that will announce the current service to the network.
	 * @return packet data.
	 */
	private String buildAnnouncePacket()
	{
		String host = getLocalhostIp();
		String packet = "announce " + serviceName + "/" + protoVer + "\r\n"
				+ "Host: " + host + "\r\n"			// build packet
				+ "Port: " + servicePort + "\r\n";
		return packet;
	}
	
	/**
	 * Gets the IP of the server.
	 * @return IP Address
	 */
	private String getLocalhostIp()
	{	// This code is based off a stackoverflow.com post, hence will appear to be very similar
	    try
	    {
	    	String ipAddress = "";
	        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
	        while (interfaces.hasMoreElements())
	        {
	            NetworkInterface iface = interfaces.nextElement();
	            if (iface.isLoopback() || !iface.isUp())	// make sure interface is not loopback
                {
	            	continue;
                }

	            Enumeration<InetAddress> addresses = iface.getInetAddresses();
	            while(addresses.hasMoreElements())	// go through available addresses
	            {
	                InetAddress addr = addresses.nextElement();
	                ipAddress = addr.getHostAddress();
	                if (!ipAddress.equals("") && !ipAddress.equals(null))
	                {
	                	return ipAddress;	// get primary IP
	                }
	            }
	        }
	        return ipAddress;
	    }
	    catch (Exception e)
	    {	// absolutely nothing can be done about this error
	    	// at best we can let the user input data
	    	return "";
	    }
	}
	
	/**
	 * Gets the local LAN broadcast subnet. Using 255.255.255.255 only as
	 * a fall back because some LAN's drop packets sent to this address.
	 * @return Broadcast subnet address
	 */
	private String getBroadcastAddress()
	{
		String ip = getLocalhostIp();
		if (ip.equals("127.0.0.1") || ip.equals("") || ip == null)
		{
			ip = "255.255.255.255";
		}
		ip = ip.replaceFirst("[.][0-9]+$", ".255");
		return ip;
	}
	
	/**
	 * Class to store the data of a known service.
	 * @author Matthew Knox
	 */
	public class AnnouncedService
	{
		/**
		 * Host/IP to store.
		 */
		private String ipHost;
		/**
		 * Port of the service.
		 */
		private int port;
		
		/**
		 * 
		 * @param ipHost
		 * @param port
		 */
		public AnnouncedService(String ipHost, int port)
		{
			this.ipHost = ipHost;
			this.port = port;
		}
		
		/**
		 * Gets the host/IP address of the service.
		 * @return host/IP
		 */
		public String getServiceAddress()
		{
			return ipHost;
		}
		
		/**
		 * Gets the port number of the service.
		 * @return port
		 */
		public int getServicePort()
		{
			return port;
		}
		
		@Override
		public boolean equals(Object obj)
		{
			if (!(obj instanceof AnnouncedService))
			{
				return false;	// if wrong type, fail
			}
			
			AnnouncedService ser = (AnnouncedService)obj;	// cast
			
			if (ser.getServicePort() != getServicePort()) return false;	// check values
			if (!ser.getServiceAddress().equals(getServiceAddress())) return false;
			return true;
		}
	}
	
	/**
	 * Main entry point for testing this class.
	 * @param args Ignored, don't use.
	 * @throws Exception If a critical, unhandled error occurs.
	 */
	public static void main(String[] args) throws Exception
	{
		NetworkDiscoveryServer manager = new NetworkDiscoveryServer(9082, "SomeService", 7104);
		manager.start();
		Thread.sleep(5000);		// delay to prevent race condition
		System.out.println("-");
		NetworkDiscoveryServer manager2 = new NetworkDiscoveryServer(9082, "SomeService");
		manager2.start();
		System.in.read();
		System.out.println("####");
		ArrayList<AnnouncedService> services = manager2.getAnnouncedServices();
		for (AnnouncedService service : services)	// should print at least 1
		{
			System.out.println(service.getServiceAddress() + " : " + service.getServicePort());
		}
		System.out.println("####");
		manager.abort();
		Thread.sleep(5000);
		services = manager2.getAnnouncedServices();
		for (AnnouncedService service : services)	// should print 1 less than last time
		{
			System.out.println(service.getServiceAddress() + " : " + service.getServicePort());
		}
		System.out.println("####");
		manager2.abort();
	}
}
