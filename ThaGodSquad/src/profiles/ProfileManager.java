package profiles;

import model.Activity;
import model.Gender;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import profiles.loaders.ProfileLoader;
import profiles.network.NetworkDiscoveryServer;
import profiles.network.NetworkProfileServer;

/**
 * Provides methods for managing profiles
 */
public final class ProfileManager {
	
	/**
	 * Static profile server, used if enabled in ProfileManagerView
	 */
	public static NetworkProfileServer ProfileServer;
	
	/**
	 * Static profile manager, used to keep a persistent manager where ever it is used from.
	 */
	public static ProfileManager CurrentProfileManager;

    /**
     * Indicates whether or not there is a current profile manage
     * @return The profile manager
     */
    public static boolean CurrentProfileManagerExists(){ return CurrentProfileManager != null; }

	/**
	 * Currently in use user profile.
	 */
    private UserProfile currentProfile;
    
    /**
     * Currently in use profile loader.
     */
    private ProfileLoader profileLoader;

    /**
	 * Automatic network discovery server.
	 */
	private NetworkDiscoveryServer discoveryServer;
    
    /**
     * Instantiates a new profile manager.
     * @param profileLoader Loader to use to read and write profiles
     */
    public ProfileManager(ProfileLoader profileLoader)
    {
    	setProfileLoader(profileLoader);
    }
    
    /**
	 * Gets a list of known servers for this application.
	 * @return list of server details
	 */
	public ArrayList<NetworkDiscoveryServer.AnnouncedService> getKnownServers()
	{
		return discoveryServer.getAnnouncedServices();
	}
	
	/**
	 * Starts server automatic discovery.
	 */
	public void startServerDiscovery()
	{
		discoveryServer = new NetworkDiscoveryServer(9493, "seng202");
    	discoveryServer.start();		// start auto-discovery server
	}
	
	/**
	 * Stops server automatic discovery.
	 */
	public void stopServerDiscovery()
	{
		try
		{
			discoveryServer.abort();
		}
		catch (Exception e)
		{
			// nothing we can do, besides which we want to shutdown
		}
	}

    /**
     * Gets the current in use profile loader.
     * @return current profile loader
     */
    public ProfileLoader getProfileLoader()
    {
    	return profileLoader;
    }
    
    /**
     * Sets the currently in use profile loader.
     * @param profileLoader Profile loader to use.
     */
    public void setProfileLoader(ProfileLoader profileLoader)
    {
    	this.profileLoader = profileLoader;
    }
    
    /**
     * Creates a profile and associates the activities with it
     * @param profileName The name of the profile
     * @return The created profile
     * @exception If a problem occurs during profile creation
     */
    public UserProfile createProfile(String profileName, Date birthDate, float height, float weight, Gender gender, Collection<Activity> activities) throws Exception
    {
        // create a user profile
    	UserProfile userProfile = new UserProfile(profileName, birthDate, height, weight, gender, activities);
        // save the profile to the HD
    	saveProfile(userProfile);
        return userProfile;
    }

    /**
     * Loads a user profile from the disk.
     * @param profileName The name of the profile to load
     * @return The loaded profile. Will be null if the profile does not exist
     */
    public UserProfile loadProfile(String profileName)
    {
    	// load the profile using the default directory
    	return profileLoader.loadProfile(profileName);
    }

    /**
     * Saves a user profile out to a file in the default directory.
     * @param profile Profile to save.
     * @throws Exception When a profile fails to save.
     */
    public void saveProfile(UserProfile profile) throws Exception
    {
    	// saves the profile using the default directory
    	profileLoader.saveProfile(profile);
    }

    /**
     * Checks to see if a profile exists
     * @param profileName The name of the profile
     * @return Whether the profile exists
     */
    public boolean profileExists(String profileName){
    	ArrayList<String> profiles = getProfiles();
    	return profiles.contains(profileName);
    }

    /**
     * Returns a list of profiles that exist in the default directory.
     * @return
     */
    public ArrayList<String> getProfiles()
    {
    	return profileLoader.getProfileList();
    }

    /**
     * Deletes the specified profile
     * @param profileName The The name of the profile
     * @return Whether the operation was successful
     */
    public boolean deleteProfile(String profileName)
    {
    	return profileLoader.deleteProfile(profileName);
    }

    /**
     * Gets the current user profile
     * @return The current profile. Null if it has not been set
     */
    public UserProfile getCurrentProfile(){
        return currentProfile;
    }

    /**
     * Sets the current user profile
     * @param profile The new user profile
     */
    public void setCurrentProfile(UserProfile profile){
        currentProfile = profile;
    }
}
