package profiles.database;

import java.util.Stack;
import java.sql.*;

/**
 * Class to manage database connections to a SQL database.
 * This is so that the DatabaseProfileLoader is independent
 * of the type of database being used (so the database type
 * can be more easily swapped out) and so that persistent
 * connections can be used where multiple requests are made.
 * 
 * Also provides helper functions to make creating JDBC
 * SQL queries easier.
 */
public class DatabaseManager
{
	/**
	 * Driver to use to manage connections to the database.
	 */
	private static final String JdbcDriver = "org.sqlite.JDBC";
	/**
	 * Connection to the database.
	 */
	private Connection databaseConnection;
	/**
	 * Current connection state.
	 */
	private boolean connected = false;
	/**
	 * Connection ownership stack. Will only connect/disconnect when empty.
	 * Useful for allowing chaining of methods with their own connect/disconnect
	 * calls.
	 */
	private Stack<Integer> disconnectFor = new Stack<Integer>();
	/**
	 * URL of the database.
	 */
	private String url;
	
	/**
	 * Creates a new DatabaseManager.
	 * @param url Url of the SQL Database server.
	 */
	public DatabaseManager(String url)
	{
		this.url = url;
	}
	
	/**
	 * Connect to the database if another connection owner did not already.
	 * @param index Connection owner.
	 * @return Success.
	 */
	public boolean connect(int index)
	{
		disconnectFor.push(index);
		return connect();
	}
	
	/**
	 * Connect to the database.
	 * @return Success
	 */
	public boolean connect()
	{
		try
		{
			if (connected)
			{
				return true;	// we are already connected
			}
			
			@SuppressWarnings("unused")	// may be unused but is required
			Object driver = Class.forName(JdbcDriver); // load the driver
			databaseConnection = DriverManager.getConnection(url); // get a connection to the db using the driver
			connected = true;
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();	// something went wrong. 
			System.err.println("Error occured while connecting to database:\n" + e);
			connected = false;
			return false;			// return failure
		}
	}
	
	/**
	 * Disconnect from the database if another index does not own the connection.
	 * @param index Connect owner.
	 * @returnSuccess
	 */
	public boolean disconnect(int index)
	{
		if (disconnectFor.peek() == index)
		{
			disconnectFor.pop();	// remove the top item from the connection stack
		}
		
		if (disconnectFor.isEmpty())
		{
			return disconnect();	// disconnect if this is the connection owner
		}
		return true;				// return success
	}
	
	/**
	 * Disconnect from the database.
	 * @return Success.
	 */
	public boolean disconnect()
	{
		connected = false;
		try
		{
			databaseConnection.close();	// try disconnect
			return true;				// return success
		}
		catch (Exception e)
		{
			System.err.println("Error occured while disconnecting to database:\n" + e.getMessage());
			return false;				// return failure
		}
	}
	
	/**
	 * Generates a PreparedStatement from an SQL string and a set of objects.
	 * @param statement SQL statement.
	 * @param objects Objects to send. If null, no objects will be sent.
	 * @return A prepared statement.
	 * @throws SQLException On database error.
	 */
	private PreparedStatement generateStatement(String statement, Object[] objects) throws SQLException
	{
		PreparedStatement preparedStatement = databaseConnection.prepareStatement(statement);	// create statement
		if (objects != null)
		{
			for (int i = 0; i < objects.length; i++)
			{
				preparedStatement.setObject(i+1, objects[i]);	// add object
			}
		}
        return preparedStatement;
	}
	
	/**
	 * Sends a statement.
	 * @param statement Statement to send.
	 * @throws SQLException On database error.
	 */
	public void sendStatement(String statement) throws SQLException
	{
		sendStatement(statement, null);
	}
	
	/**
	 * Sends a statement.
	 * @param statement Statement to send.
	 * @param objects Objects to send. If null, no objects are sent.
	 * @throws SQLException On database error.
	 */
	public void sendStatement(String statement, Object[] objects) throws SQLException
	{
		PreparedStatement preparedStatement = generateStatement(statement, objects);
		preparedStatement.execute();
	}
	
	/**
	 * Sends a statement and waits for a response.
	 * @param statement Statement to send.
	 * @return The objects returned from the database.
	 * @throws SQLException On database error.
	 */
	public ResultSet sendStatementWithResponse(String statement) throws SQLException
	{
		return sendStatementWithResponse(statement, null);
	}
	
	/**
	 * Sends a statement and waits for a response.
	 * @param statement Statement to send.
	 * @param objects Objects to send. If null, no objects are sent.
	 * @return The objects returned from the database.
	 * @throws SQLException On database error.
	 */
	public ResultSet sendStatementWithResponse(String statement, Object[] objects) throws SQLException
	{
		PreparedStatement preparedStatement = generateStatement(statement, objects);
        return preparedStatement.executeQuery();
	}
}
