package model;

/**
 * Enum to store the gender of the user.
 */
public enum Gender
{
	/**
	 * They are male...
	 */
	Male,
	/**
	 * They are female...
	 */
	Female
}
