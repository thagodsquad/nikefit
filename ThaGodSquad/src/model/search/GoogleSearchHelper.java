package model.search;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Provides helper methods for searching google for content.
 */
public class GoogleSearchHelper
{
    /**
     * Gets a list of search results
     * @param query The thing to search for
     * @param maxResults The max number of results. Note: Must be a multiple of 4
     * @return The results
     * @throws Exception Bad stuff happened
     */
    public static ArrayList<GoogleSearchResult> Search(String query, int maxResults) throws Exception
    {
        if (maxResults % 4 != 0 || maxResults < 0)	// check that we have an appropriate number of results
        {
        	throw new IllegalArgumentException("'maxResults' must be a multiple of 4 (Jay's lazy)");
        }

        ArrayList<GoogleSearchResult> searchResults = new ArrayList<>();

        for (int i = 0; i < maxResults; i = i + 4)	// for each result
        {
            final String address = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&key=AIzaSyD5L8pyGe_Drj_PZvXEyk1X6Teu4_9PTKw&start="+i+"&q=";
            final String charset = "UTF-8";

            URL url = new URL(address + URLEncoder.encode(query, charset));		// create URL
            
            Reader reader = new InputStreamReader(url.openStream(), charset);	// open the URL for reading

            // setup GSON to build search results
            Gson g = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
            GoogleSearchResults results = g.fromJson(reader, GoogleSearchResults.class);
            GoogleSearchResults.ResponseData d = results.getResponseData();
            
            if (d == null)	// if there is no response, then we have a terms of service abuse
        	{
            	throw new Exception("Google Terms of service abuse on your internet connection!");
        	}
            
            // convert to an array of search results
            ArrayList<GoogleSearchResult> rs = (ArrayList<GoogleSearchResult>) d.getResults();
            for(int m = 0; m <= 3; m++)
            {
                GoogleSearchResult result = rs.get(m);
                result.setContent(removeTags(result.getContent()));		// set content and titles
                result.setTitle(removeTags(result.getTitle()));
                searchResults.add(result);
            }
        }

        return searchResults;
    }

    /**
     * Removes html tags from a string
     * For example <p>Foo</p> goes to Foo
     * @param input The string with html tags
     * @return The string without html tags
     */
    private static String removeTags(String input)
    {
        String result = new String();
        boolean inTag = false;
        
        for (char c : input.toCharArray())	// for each char in the string
        {
            if (c == '<')
            {
                inTag = true;
            }
            else if (c == '>')	// set intag status
            {
                inTag = false;
                continue;
            }

            if (inTag)
            {
            	continue;		// while intag dont add to string
            }

            result += c;		// add char to string
        }

        return result;			// return string
    }
}
