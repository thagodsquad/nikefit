package model.search;

/**
 * Stores an individual google search result.
 */
public class GoogleSearchResult
{
	/**
	 * Url of the search result.
	 */
    private String url;
    
    /**
     * Title of the search result.
     */
    private String title;

    /**
     * Content of the search result.
     */
    private String content;

    /**
     * Gets the URL of the search result.
     * @return search result URL
     */
    public String getUrl()
    {
        return url;
    }

    /**
     * Gets the title of the search result.
     * @return search result title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Sets the URL of the search result
     * @param url URL of the search result
     */
    public void setUrl(String url)
    {
        this.url = url;
    }

    /**
     * Sets the title of the search result.
     * @param title Title of the search result.
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * Gets the content of the search result.
     * @return content of the search result.
     */
    public String getContent()
    {
        return content;
    }

    /**
     * Sets the content of the search result without any newlines.
     * @param content Content of the search result.
     */
    public void setContent(String content)
	{
        this.content = content.replace('\n', ' ').replace('\r', ' ');
    }
}
