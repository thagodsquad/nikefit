package model.search;

import java.util.List;

/**
 * Handles the collection of results returned by a google search.
 */
public class GoogleSearchResults
{
	/**
	 * The data received from google.
	 */
    private ResponseData responseData;

    /**
     * Gets the raw response data received from google.
     * @return Raw response data.
     */
    public ResponseData getResponseData()
    {
        return responseData;
    }

    /**
     * Sets the raw response data received from google.
     * @param responseData The data received.
     */
    public void setResponseData(ResponseData responseData)
    {
        this.responseData = responseData;
    }

    /**
     * Class for storing the raw response data from google.
     */
    public static class ResponseData
    {
    	/**
    	 * List of google search results
    	 */
        private List<GoogleSearchResult> results;
        
        /**
         * Gets the list of results
         * @return
         */
        public List<GoogleSearchResult> getResults()
        {
            return results;
        }

        /**
         * Sets the search results
         * @param results
         */
        public void setResults(List<GoogleSearchResult> results)
        {
            this.results = results;
        }
    }
}