package model;

/**
 * Class for holding a latitude, longitude and elevation coordinate,
 * as well as calculating the distance between this location and another.
 */
public class Location implements java.io.Serializable {
    /**
	 * Unique serialisation id.
	 */
	private static final long serialVersionUID = -4344739848693878246L;
	
	/**
	 * Radius of earth at the equator.
	 */
	private static final float EARTHS_EQUITORIAL_RADIUS = 63781370f;
	
	/**
	 * Radius of earth at the poles.
	 */
	private static final float EARTHS_POLAR_RADIUS = 63567523f;

	/**
	 * Latitude of this location.
	 */
    private float latitude;
    
    /**
     * Longitude of this location.
     */
    private float longitude;

    /**
     * Elevation of this location.
     */
    private float elevation;

    /**
     * A constructor
     * @param latitude The latitude
     * @param longitude The longitude
     */
    public Location(float latitude, float longitude, float elevation){
        this.latitude = latitude;
        this.longitude = longitude;		// initialise variables
        this.elevation = elevation;
    }

    /**
     * Gets the latitude of this location
     * @return The latitude
     */
    public float getLatitude(){
        return latitude;
    }

    /**
     * Gets the longitude of this location
     * @return The longitude
     */
    public float getLongitude(){
        return longitude;
    }

    /**
     * Gets the elevation of this location
     * @return The elevation
     */
    public float getElevation() {return elevation;}

    /**
     * Calculates the earth's radius as a specific latitude (given by a location)
     * @param loc Location to calculate radius to center of earth for
     * @return Distance in meters
     */
    public static double earthRadiusAtLocation(Location loc)
    {
		double lat = loc.getLatitude();	// get latitude
		
		// run formulae to get radius
		double c = Math.pow(Math.pow(EARTHS_EQUITORIAL_RADIUS, 2) * Math.cos(lat), 2)
		+ Math.pow(Math.pow(EARTHS_POLAR_RADIUS, 2) * Math.sin(lat), 2);
		double e = Math.pow(EARTHS_EQUITORIAL_RADIUS * Math.cos(lat), 2)
		+ Math.pow(EARTHS_POLAR_RADIUS * Math.sin(lat), 2);

		// sqrt, account for error in conversion and add the elevation
		return Math.sqrt(c/e) / 10 + loc.getElevation();
    }
    
    /**
     * Gets the distance between two points using the haversine formula
     * You can see the wikipedia article here (if you're brave)
     * http://en.wikipedia.org/wiki/Haversine_formula
     * @param a The first point
     * @param b The second point
     * @return The distance between them
     */
    public static float haverSineDistance(Location a, Location b){
    	// get the radian angles for each location
    	double lat1 = Math.toRadians(a.getLatitude());
    	double long1 = Math.toRadians(a.getLongitude());
    	double lat2 = Math.toRadians(b.getLatitude());
    	double long2 = Math.toRadians(b.getLongitude());
    	
    	// Calculate the average earth radius between locations
    	double earthRadius = Location.earthRadiusAtLocation(a) + Location.earthRadiusAtLocation(b);
    	earthRadius /= 2;
    	
    	// Calculate haversine distance
    	double distance = Math.pow(Math.sin((lat2 - lat1)/2.0), 2);
    	distance += Math.pow(Math.sin((long2 - long1)/2.0), 2) * Math.cos(lat1) * Math.cos(lat2);
    	distance = Math.asin(Math.sqrt(distance));
    	distance *= 2.0 * earthRadius;
    	
    	return (float)distance;
    }

    @Override
    public boolean equals(Object obj){
        if (!(obj instanceof Location)) return false;
        Location other = (Location)obj;			// custom comparison so that the DataPoint class works properly
        return other.elevation == elevation && other.latitude == latitude && other.longitude == longitude;
    }
}