package analysis;

import model.Activity;
import model.DataPoint;

import java.util.Collection;

/**
 * A class for containing general statistics
 */
public class Statistics {
    private float totalDistance;

    private float totalCalories;

    private float highestHeartRate;
    private float averageHeartRate;

    private float highestElevation;

    private float greatestSpeed;
    private float averageSpeed;

    /**
     * Analyses a set of activities and pulls relevant data from them
     * @param activities
     */
    public void analyse(Collection<Activity> activities){
        float pointCount = 0;
        Reset();
        for (Activity a : activities)
            for (DataPoint p : a.getPoints()){
                totalDistance += p.getDistance();
                totalCalories += p.getCalories();

                if (p.getSpeed() >= greatestSpeed)
                    greatestSpeed = p.getSpeed();

                if (p.getHeartRate() > highestHeartRate)
                    highestHeartRate = p.getHeartRate();
                
                if (p.getLocation().getElevation() > highestElevation)
                {
                	highestElevation = p.getLocation().getElevation();
                }

                averageSpeed += p.getSpeed();
                averageHeartRate += p.getHeartRate();
                pointCount++;
            }

        if (pointCount == 0) return;

        averageSpeed /= pointCount;
        averageHeartRate /= pointCount;
    }

    /**
     * Rounds a value to a specified number of decimal places
     * @param value The value to round
     * @param dp The number of decimal places
     * @return The rounded value
     */
    public static float round(float value, int dp) {
        return Math.round(value * Math.pow(10, dp)) / (float)Math.pow(10, dp);
    }

    /**
     * Resets the statistics
     */
    public void Reset(){
        totalDistance = 0;

        totalCalories = 0;

        greatestSpeed = 0;
        averageSpeed = 0;

        averageHeartRate = 0;
        highestHeartRate = 0;

        highestElevation = 0;
    }

    /**
     * The total distance travelled
     * @return The total distance
     */
    public float getTotalDistance(){
        return totalDistance;
    }

    /**
     * The total calories burnt
     * @return The total calories
     */
    public float getTotalCalories(){
        return totalCalories;
    }

    /**
     * The highest heart rate
     * @return The highest heart rate
     */
    public float getHighestHeartRate(){
        return highestHeartRate;
    }

    /**
     * The average heart rate
     * @return The average heart rate
     */
    public float getAverageHeartRate() {return averageHeartRate; }

    /**
     * Gets the highest elevation reached
     * @return The highest elevation
     */
    public float getHighestElevation(){
        return highestElevation;
    }

    /**
     * The greatest speed acquired
     * @return The greatest speed
     */
    public float getGreatestSpeed(){
        return greatestSpeed;
    }

    /**
     * The average speed acquired
     * @return The average speed
     */
    public float getAverageSpeed() {return averageSpeed; }
}
