package analysis.health;

import model.Activity;
import model.DataPoint;
import profiles.UserProfile;

import java.util.Collection;

/**
 * Checks to see if a user has Tachycardia, a description of the disease and a link to a relevant article on the
 * web.
 */
public class TachycardiaAnalyser implements HealthWarning {
    @Override
    public String getWarningUrl() {
        return "http://en.wikipedia.org/wiki/Tachycardia";
    }

    @Override
    public String getWarningName() {
        return "Tachycardia";
    }

    @Override
    public String getWarningDescription() {
        return "Your resting heart rate is abnormally high (>100bpm), we recommend you see a health professional.";
    }

    @Override
    public boolean analyse(UserProfile profile, Collection<Activity> activities) {
    	// check if current speed is less than ~ 1m/s ("resting") then check if heartrate is above 100bpm
    	// only checks for tachycardia range in either the 12-15yo or the 15+ range
    	
    	// will check if the previous 5 points are also resting, in case someone just went real hard and stopped
    	for (Activity activity : activities) {
    		Object[] dataPoints = activity.getPoints().toArray();
    		for (int i=0; i < dataPoints.length; i++) { // incrementing through the datapoints the proper way!
    			boolean resting = true; // if true, the person has been resting for the last 5 points, and hasn't stopped briefly after going hard

    			for (int j=0; j < 5; j++) { // gonna have a look at the prvious ~5 points
    				if (i-j >= 0) { // otherwise we go into negative indices (bad news)
	    				if ((((DataPoint)dataPoints[i-j]).getSpeed() > 1.0)) {
	    					resting = false;
	    				}
    				}
    			}

    			DataPoint point = (DataPoint)dataPoints[i];
    			if (resting) {
	    			if ((profile.getAge() >= 12) && (profile.getAge() < 15)) { // 12-15yo
	    				if ((point.getSpeed() <= 1.0) && (point.getHeartRate() >= 119.0)) {
	    					return true;
	    				}
	    			}
	    			if (profile.getAge() >= 15) { //15yo+
	    				if ((point.getSpeed() <= 1.0) && (point.getHeartRate() >= 100.0)) {
	    					return true;
	    				}
	    			}
	    		}
    		}
    	}	
    	return false;
    
    }
}
