package analysis.health;

import model.Activity;
import profiles.ProfileManager;
import profiles.UserProfile;
import tests.profiles.UserProfileTest;

import java.util.ArrayList;
import java.util.Collection;

/**
 * A class which provides methods for getting health warnings based on user data
 */
public class HealthAnalyser {
    /**
     * Checks to see if a user has any of several predefined health warnings
     * @param profile The user to analyse
     * @return A list of health warnings
     */
    public static Collection<HealthWarning> getWarnings(UserProfile profile){
        return getWarnings(profile, profile.getActivities());
    }

    /**
     * Checks to see if any of the activities in the list indicate that the current user has any
     * of a set of predefined diseases
     * @param activities The activities to check
     * @return The warnings
     */
    public static Collection<HealthWarning> getWarnings(Collection<Activity> activities){
        return getWarnings(ProfileManager.CurrentProfileManagerExists() ? ProfileManager.CurrentProfileManager.getCurrentProfile() : UserProfileTest.createTestProfile(), activities);
    }

    /**
     * Checks if any of the activities in the list indicate that the specified user has any
     * of a set of predefined diseases
     * @param profile The user to analyse
     * @param activities The activities to check
     * @return The warnings
     */
    public static Collection<HealthWarning> getWarnings(UserProfile profile, Collection<Activity> activities){
        ArrayList<HealthWarning> possible = new ArrayList<>();
        possible.add(new TooYoungWarning());
        possible.add(new BradycardiaAnalyser());
        possible.add(new TachycardiaAnalyser());
        possible.add(new CardiovascularAnalyser());

        ArrayList<HealthWarning> warnings = new ArrayList<>();

        for (HealthWarning warning : possible)
            if (warning.analyse(profile, activities))
                warnings.add(warning);

        return warnings;
    }
}
