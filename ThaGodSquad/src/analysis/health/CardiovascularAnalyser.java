package analysis.health;

import model.Activity;
import profiles.UserProfile;

import java.util.Collection;

/**
 * Checks to see if a user has Bradycardia, a description of the disease and a url to an article on the internet
 * with relevant information
 */
public class CardiovascularAnalyser implements HealthWarning{
	
	/**
     * Getter for the Cardiovascular warning URL
     * @return Warning URL string
     */
    @Override
    public String getWarningUrl() {
        return "http://en.wikipedia.org/wiki/Cardiovascular_disease";
    }

    /**
     * Getter for the health warning
     * @return Warning name string
     */
    @Override
    public String getWarningName() {
        return "Cardiovascular Disease";
    }

    /**
     * Getter for the Cardiovascular warning description
     * @return Warning description string
     */
    @Override
    public String getWarningDescription() {
        return "You could be at risk of potentially developing symptoms of Cardiovascular Disease, however, this tool doesn't have enough data to safely analyse for it. You can safely ignore this warning. Unless your heart rate is 0. In which case, you might have cardiovascular mortality (no heartbeat). Then see a doctor";
    }

    /**
     * Analysis for cardiovascular disease
     * @param profile, the user profile in use
     * @param activities, the collection of activities
     * @return Boolean (will only return true)
     */
    @Override
    public boolean analyse(UserProfile profile, Collection<Activity> activities) {
    	return activities.size() > 0;
    	
    }
}
