package analysis.health;

import model.Activity;
import profiles.UserProfile;

import java.util.Collection;

/**
 * A common interface for health related  providing access to the name of the risk, a description of it and a link
 * to a relevant article on the web.
 */
public interface HealthWarning {
    /**
     * A url to the wikipedia page
     * @return the url
     */
    String getWarningUrl();

    /**
     * The name of health issue
     * @return The name
     */
    String getWarningName();

    /**
     * A description of the health issue
     * @return The description
     */
    String getWarningDescription();

    /**
     * Indicates whether the user is at risk from this disease
     * @param profile The user
     * @param activities The activities to check
     * @return Whether or not the user has this disease
     */
    boolean analyse(UserProfile profile, Collection<Activity> activities);
}
