package analysis.health;

import model.Activity;
import model.DataPoint;
import profiles.UserProfile;

import java.util.Collection;

/**
 * Checks to see if a user has Bradycardia, and provides access to relevant information and a description
 * along with a url to a wikipedia article.
 */
public class BradycardiaAnalyser implements HealthWarning{
	
	/**
     * Getter for the Bradycardia warning URL
     * @return Warning URL string
     */
    @Override
    public String getWarningUrl() {
        return "http://en.wikipedia.org/wiki/Bradycardia";
    }

    /**
     * Getter for the health warning
     * @return Warning name string
     */
    @Override
    public String getWarningName() {
        return "Bradycardia";
    }

    /**
     * Getter for the Bradycardia warning description
     * @return Warning description string
     */
    @Override
    public String getWarningDescription() {
        return "Your resting heart rate is abnormally low (<60bpm), we recommend you see a health professional.";
    }
    
    /**
     * Checks whether or not if said person is "resting"
     * @param profile, the user profile in use
     * @param activities, the collection of activities
     * @return Boolean value
     */
    @Override
    public boolean analyse(UserProfile profile, Collection<Activity> activities) {
    	// check if current speed is less than ~ 1m/s ("resting") then check if heartrate is below 60bpm
    	for (Activity activity : activities) {
    		for (DataPoint point : activity.getPoints()) {
    			if ((point.getSpeed() <= 1.0) && (point.getHeartRate() < 60)) {
    				return true;
    			}
    		}
    	}
        return false;
    }
}
