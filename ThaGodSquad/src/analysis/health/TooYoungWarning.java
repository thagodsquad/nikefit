package analysis.health;

import model.Activity;
import profiles.UserProfile;

import java.util.Collection;

/**
 * A warning message stating a user is too young for us to diagnose with tachycardia, due to the way that things
 * quickly become more complicated with young users, and thus, harder to predice.
 */
public class TooYoungWarning implements HealthWarning {
    @Override
    public String getWarningUrl() {
        return "";
    }

    @Override
    public String getWarningName() {
        return "You're too young!";
    }

    @Override
    public String getWarningDescription() {
        return "We can't reliably test for tachycardia at your age :'(";
    }

    @Override
    public boolean analyse(UserProfile profile, Collection<Activity> activities) {
        return profile.getAge() < 12;
    }
}
