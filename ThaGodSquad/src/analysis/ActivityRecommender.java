package analysis;

import java.util.Random;

import model.Activity;
import model.Activity.ActivityType;
import profiles.UserProfile;

/**
 * Recommends an activity based on user profile data
 */
public class ActivityRecommender {
	
	
    /**
     * Recommends a type of activity for the user.
     * This class is a stub for a better future implementation.
     * @param profile The profile of the user the activity is being recommended for
     * @return The activity the user should partake in
     */
    public static Activity.ActivityType getRecommendedActivity(UserProfile profile){
    	return ActivityType.forCode((new Random()).nextInt(ActivityType.Unknown.getIntValue()));
    }
}
