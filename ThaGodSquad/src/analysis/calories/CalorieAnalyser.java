package analysis.calories;

import model.DataPoint;
import model.Gender;
import profiles.UserProfile;

/**
 * An interface different calorie analysers use to provide a consistent interface
 */
public final class CalorieAnalyser {
    /**
     * Calculates the calories burnt between two points
     * @param point The point to calculate calories for.
     * @param previousPoint The previous point
     * @return The calories burnt (cal)
     */
    public static float calculateCaloriesAtDataPoint(DataPoint point, DataPoint previousPoint, UserProfile profile)
    {
        //For testing
        if (profile == null) return 0;

    	if (previousPoint == null) return 0f;
        // Source for calculation: http://fitnowtraining.com/2012/01/formula-for-calories-burned/
        float caloriesBurned = 0;
        double timeInterval = (previousPoint.getDate().getTime() - point.getDate().getTime()) / 60000.0; // In minutes
        
        // If male
        if (profile.getGender() == Gender.Male) {
        	caloriesBurned = (float) (((profile.getAge() * 0.2017) - (profile.getWeight() * 0.09036) + (point.getHeartRate() * 0.6309) - 55.0969) * timeInterval / 4.184);
        }
        // If female
        else if (profile.getGender() == Gender.Female) {
        	caloriesBurned = (float) (((profile.getAge() * 0.074) - (profile.getWeight() * 0.05741) + (point.getHeartRate() * 0.4472) - 20.4022) * timeInterval / 4.184);
        }
        // Else unspecified...
        else {
        	// Average?
        	// Male?
        	// Female?
        	// Random?
        }
        return Math.abs(caloriesBurned);
    }
}
